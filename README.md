# README #

Code and artefacts in support of
> **A Framework for the Detection of Conflicts between Clinical Pathways using Constraint Solvers**.  Weber, Backman, Litchfield, Filho, Lee.  *Submitted to MODELS 2018*.

## What is this repository for? ##

Python, Z3 SMT, and Alloy code, and BPMN+V models in support of the paper submission.

## Code

**Version 0.1**

**Note:** This is rough code developed for research purposes.  It has not particularly been optimised 
  nor developed to exhibit good Python style or efficiency, or to work beyond the bounds of the experiments
  explicitly carried out for this paper.  Some parts of it are only partially developed.

### Z3

* ```bpmnv_seq15.py```, ```bpmnv_xor15.py```, ```bpmnv_ior15.py```, ```bpmnv_pll15.py```: individual test programs exhibiting the main aspects of the BPMN+V mapping to Z3.
* ```bpmnv_model.py```: top-level code to read BPMN+V, transform to Z3 and run conflict detection.
* ```bpmnv_result.py```: utility module
* ```Z3BPMNV.py```: implement the details of constraints into Z3-SMT.
* ```BPMNV.py```: classes for implementing BPMN+V models.
* ```BPMNVparse.py```: grammars for parsing guard and data modification annotations.
* ```BPMNVrandom.py```: wrapper to generate multiple random BPMN+V models and detect conflicts.

### Alloy

* ```BPMNV.als```: meta-model.
* ```BPMNV_seq.als```, ```BPMNV_xor2.als```, ```BPMNV_pll2.als```, ```BPMNV_ior2.als```: indvidual models.
* ```BPMNV.thm```: Alloy theme for visualisation.

### BPMN+V Models

* ```BPMNV_TEXT/```: Basic models in a naive text format.
* ```BPMNV/```: Basic models suitable for Camunda or [bpmn.io](http://demo.bpmn.io/),
  *plus*
  OA.bpmn, COPD.bpmn, HT.bpmn *fragments* (Osteoarthritis, COPD, hyptertension) for case studies.  More detailed models are work in progress.


## How do I get set up? ##

```bpmnv_pll15.py``` etc. take no parameters.

Framework to read in BPMN+V model(s) and analyse for consistencies or conflicts:

>
```./bpmnv_model.py -f BPMNV/pll2.bpmn```

>
```./bpmnv_model.py -f1 BPMNV/pll2.bpmn -f2 BPMNV/xor2.bpmn```

```
usage: bpmnv_model.py [-h] [-f MODELFILE] [-f1 MODELFILE1] [-f2 MODELFILE2]
                      [-l T_LIMIT] [-d DOTFILE] [-n] [-x]

Z3 BPMN+V

optional arguments:
  -h, --help            show this help message and exit
  -f MODELFILE, --file MODELFILE
                        File specifying BPMN+V model.
  -f1 MODELFILE1, --file1 MODELFILE1
                        File specifying BPMN+V model (1st of pair to compose).
  -f2 MODELFILE2, --file2 MODELFILE2
                        File specifying BPMN+V model (2nd of pair to compose).
  -l T_LIMIT, --limit T_LIMIT
                        Number of time steps in model (TEMP).
  -d DOTFILE, --dotfile DOTFILE
                        Output to dot file(s)
  -n, --nosuccess       Ask Z3 for failures only
  -x, --debug
```

### Contribution guidelines ###

* Not applicable.

### Who do I talk to? ###

* Phil Weber, School of Computer Science, University of Birmingham.  Moving to Aston University, May 2018.
* Mark Lee, School of Computer Science, University of Birmingham.
* Ian Litchfield, Institute of Applied Health Research, University of Birmingham.

**Updated 2018-05-09**