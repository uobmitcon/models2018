#!/usr/bin/env python3
"""Module for manipulating BPMN+V (data-enriched subset of BPMN).

This is a subset of the module implemented for BPMN+V analysis via CPN.
Removed all code related to translation to CPN.
Removed all code related to removing IORs -- may be needed due to problems with Z3.

- import to internal object representation.
- basic output.
- accept and visualise changes, e.g. highlight dead markings.

Problems:
- repeated code to create new obj_map entries; should be rolled into the "new".

Limitations:
o   Pre-process XML to remove namespaces; bodge to allow for old version of
    lxml.

Possibilities:

History:
    2017-11-16: First version for Z3 created from original for CPN.

Phil Weber 2017-11-16
"""

import random
import re
import time
from math import sqrt
from copy import copy, deepcopy
import BPMNVparse

from io import BytesIO
from lxml import etree

DEBUG = False
DIVERGING = "Diverging"
CONVERGING = "Converging"
T_TO_P = "TtoP"
P_TO_T = "PtoT"
ETA = "_eta"

START_REC = "(s)"  # fields for text BPMNV import
END_REC = "(e)"
ACT_REC = "[a]"
XORDIV_REC = "-x<"
XORCONV_REC = ">x-"
PLLDIV_REC = "-+<"
PLLCONV_REC = ">+-"
IORDIV_REC = "-o<"
IORCONV_REC = ">o-"

# Set some defaults for dot
DG_RANKDIR = "LR"  # Arcs left --> right by default
DG_RANKSEP = 0.3  # Min " between ranks
DG_FONTNAME = "Arial"
DG_FONTSIZE = 8  # Points
DG_REMINCROSS = "true"  # dot internal
DG_MARGIN = "0.0.0.0"

# Edges
E_ARROWSIZE = 0.5
E_FONTNAME = "Arial"
E_FONTSIZE = 8

# Nodes
N_HEIGHT = 0.5
N_WIDTH = 0.5
N_FONTNAME = "Arial"
N_FONTSIZE = 8

CONTROL_CHARS = ''.join(map(chr, list(range(0, 32)) + list(range(127, 160))))
CONTROL_CHAR_RE = re.compile('[%s]' % re.escape(CONTROL_CHARS))


class Node(object):
    """Node class to be overridden by Start, End, Activity, XOR, AND, IOR nodes."""
    pre = None  # pre-arcs
    post = None  # post-arcs
    name = None
    oid = None

    def add_pre(self, arc):
        """Add to list of predecessor arcs."""
        if self.pre is None:
            self.pre = []
            self.pre = []
        self.pre.append(arc)

    def add_post(self, arc):
        """Add to list of successor arcs."""
        if self.post is None:
            self.post = []
        self.post.append(arc)

    def rm_pre(self, arc):
        """Remove from list of predecessor arcs."""
        if arc in self.pre:
            self.pre.remove(arc)
            if not self.pre:
                self.pre = None
        else:
            raise Exception("Trying to remove non-existent arc from", self.name,
                            self.oid)

    def rm_post(self, arc):
        """Remove from list of successor arcs."""
        if arc in self.post:
            self.post.remove(arc)
            if not self.post:
                self.post = None
        else:
            raise Exception("Trying to remove non-existent arc from", self.name,
                            self.oid)


class Start(Node):
    """Start node."""

    def __init__(self, sid, name):
        """Create new Start node with supplied ID."""
        self.oid = sid
        self.name = "Start" if name is None or name.strip() == '' else name

    def print(self):
        """Pretty print."""
        name = "''" if self.name in [None, ""] else self.name.replace("\n", " ")
        print("Start: %s (%s)" % (name, self.oid))


class End(Node):
    """End node."""

    def __init__(self, eid, name):
        """Create new End node with supplied ID."""
        self.oid = eid
        self.name = "End" if name is None or name.strip() == '' else name

    def print(self):
        """Pretty print."""
        name = "''" if self.name in [None, ""] else self.name.replace("\n", " ")
        print("End: %s (%s)" % (name, self.oid))


class Activity(Node):
    """Activity node."""

    def __init__(self, aid, name):
        """Define new Activity node with the supplied attributes."""
        self.oid = aid
        self.name = "Activity" if name in [None, ""] else name
        self.guard = None  # Add later
        self.data = None  # Add later

    def print(self):
        """Pretty print."""
        name = "''" if self.name is None or self.name.strip() == '' \
            else self.name.replace("\n", " ")
        if self.guard is None and self.data is None:
            ann = ''
        else:
            ann = '['
            if self.guard is not None:
                ann += "guard: " + self.guard
                if self.data is not None:
                    ann += ', '
            if self.data is not None:
                ann += "data: " + self.data
            ann += ']'

        print("Activity: %s (%s) %s" % (name, self.oid, ann))


class XOR(Node):
    """Exclusive gateways."""

    def __init__(self, xid, name, direction, default):
        """Define new XOR Gateway with the supplied attributes."""
        self.oid = xid
        self.name = None if name is None or name.strip() == '' else name
        self.direction = direction
        self.default = default

    def print(self):
        """Pretty print."""
        if self.name is None:
            name = ''
        else:
            # noinspection PyUnresolvedReferences
            name = self.name.replace("\n", " ")
        print("XOR %s: %s (%s)" % (self.direction, name, self.oid))


class IOR(Node):
    """Inlusive gateways."""

    def __init__(self, iid, name, direction, pair=None):
        """Define new IOR Gateway with the suupplied attributes."""
        self.oid = iid

        # Default name to iid so we can use it in pairing div and conv nodes
        self.name = iid if name is None or name.strip() == '' else name
        self.direction = direction
        self.pair = pair

    def print(self):
        """Pretty print."""
        if self.name is None:
            name = ''
        else:
            # noinspection PyUnresolvedReferences
            name = self.name.replace("\n", " ")
        print("IOR %s: %s (%s)" % (self.direction, name, self.oid))


class AND(Node):
    """Parallel gateways."""

    def __init__(self, aid, name, direction):
        """Define new AND Gateway with the supplied attributes."""
        self.oid = aid
        self.name = None if name is None or name.strip() == '' else name
        self.direction = direction

    def print(self):
        """Pretty print."""
        if self.name is None:
            name = ''
        else:
            # noinspection PyUnresolvedReferences
            name = self.name.replace("\n", " ")
        print("AND %s: %s (%s)" % (self.direction, name, self.oid))


class SequenceFlow(object):
    """Sequence Flows (arcs)."""

    # pylint: disable=too-few-public-methods
    def __init__(self, fid, name, fr_node, to_node):
        """Define new Sequence Flow with the supplied attributes.

        name carries arc inscription (cf CPN/Tools).
        fr and to are P or T objects, not explicitly defined as place/trans.
        annotation (ann) is mathematical expression; may be empty.
        """
        self.oid = fid

        if isinstance(name, str):
            name = rm_ctrl_chars(name)
        self.name = name  # unused
        self.fr_node = fr_node
        self.to_node = to_node

    def print(self):
        """Pretty print."""
        name = '' if self.name in [None, ""] else self.name.replace("\n", " ")
        fr_node = "''" if self.fr_node.name in [None, ""] else self.fr_node.name.replace("\n", " ")
        to_node = "''" if self.to_node.name in [None, ""] else self.to_node.name.replace("\n", " ")
        print("Sequence Flow: (%s -> %s) %s" % (fr_node, to_node, name))


class BPMNV(object):
    """Top-level BPMN+V object."""

    # pylint: disable=too-many-instance-attributes
    validVarTypes = ["BOOL", "INT", "REAL", "STRING"]

    def __init__(self, fn=None):
        """Set up empty BPMN+V Model structure, optionally populating from file."""

        self.vardefs = {}  # Variable definitions {var: {type, value}} # val unused
        self.s = None  # start event
        self.e = None  # end event
        self.Act = []  # Activities
        self.XOR = []  # XOR gateways
        self.IOR = []  # IOR gateways
        self.AND = []  # Parallel gateways
        self.F = []  # Sequence Flows

        self.obj_map = {}  # Map object names to object refs
        self.guards = {}  # Save annotations which are guards before add to Activity
        self.data = {}  # Save annotations which are data mods b/f add to Activity
        self.cover_token_set = []  # List of tokens (data valuations) to explore the net

        self.tree = None  # XML import
        self.conflictacts = {}  # Activities with between-model conflicts, for overlays
        self.constracts = {}  # Activities with model-internal (data-)constraints

        if fn:
            self.tree = self.parse_bpmn(fn)

    def add_start(self, oid, name):
        """add_start: wrapper to ensure obj_map updated."""
        s = Start(oid, name)
        self.obj_map[oid] = s
        return(s)

    def add_end(self, oid, name):
        """add_end: wrapper to ensure obj_map updated."""
        e = End(oid, name)
        self.obj_map[oid] = e
        return(e)

    def add_act(self, oid, name):
        """add_act: wrapper to ensure obj_map updated."""
        act = Activity(oid, name)
        self.obj_map[oid] = act
        return(act)

    def add_xor(self, oid, name, direction, default):
        """add_xor: wrapper to ensure obj_map updated."""
        if not name:
            name = oid
        xor = XOR(oid, name, direction, default)
        self.obj_map[oid] = xor
        return(xor)

    def add_ior(self, oid, name, direction, pair=None):
        """add_ior: wrapper to ensure obj_map updated.

        NB: Set pairs as text strings matching name, for now.
            Need to be changed to object IDs after all nodes created.
        """
        if not name:
            name = oid  # ensure set, for pairing div and conv
        if not pair:
            if direction == DIVERGING:
                pair = name + '_'
            else:
                pair = name.rstrip('_')
                if pair == name:
                    raise Exception("Cannot match converging pair", name, "not ending with '_'!")
        ior = IOR(oid, name, direction, pair)
        self.obj_map[oid] = ior
        return(ior)

    def add_pll(self, oid, name, direction):
        """add_pll: wrapper to ensure obj_map updated."""
        if not name:
            name = oid
        pll = AND(oid, name, direction)
        self.obj_map[oid] = pll
        return(pll)

    def add_flow(self, fid, name, fr_node, to_node):
        """add_flow: wrapper to ensure obj_map updated."""
        flow = SequenceFlow(fid, name, fr_node, to_node)
        self.obj_map[fid] = flow
        return(flow)

    def gen_rnd(self, modelsize, p_structs=None):
        """Generate model by starting with a baseline and randomly expanding nodes."""

        # pylint: disable=too-many-locals
        # pylint: disable=too-many-statements

        def stroid():
            """Generate string oids for convenience."""
            y_id = 0
            while True:
                yield "rnd" + str(y_id)
                y_id += 1

        def expand_seq(act_a):
            """Expand Activity a to sequence a-b with original start and end points."""
            fout = act_a.post[0]
            act_a.rm_post(fout)
            new_aid = next(oid)
            act_b = self.add_act(new_aid, new_aid)
            self.Act.append(act_b)
            f = self.add_flow(next(oid), None, act_a, act_b)
            self.F.append(f)
            act_a.add_post(f)
            act_b.add_pre(f)
            fout.fr_node = act_b  # This should be in addPost?
            act_b.add_post(fout)

        def expand_xor(act_a):
            """Expand Activity a to XOR o-a|b-o with original start and end points."""
            fin = act_a.pre[0]  # We know there is only one
            fout = act_a.post[0]
            act_a.rm_pre(fin)
            act_a.rm_post(fout)
            aid_b = next(oid)
            act_b = self.add_act(aid_b, aid_b)
            self.Act.append(act_b)

            xid = next(oid)
            xi = self.add_xor(xid, xid, DIVERGING, None)
            self.XOR.append(xi)

            xid = next(oid)
            xo = self.add_xor(xid, xid, CONVERGING, None)
            self.XOR.append(xo)

            f = self.add_flow(next(oid), None, xi, act_a)  # Connect the XOR structure
            self.F.append(f)
            act_a.add_pre(f)

            f = self.add_flow(next(oid), None, xi, act_b)
            self.F.append(f)
            act_b.add_pre(f)

            f = self.add_flow(next(oid), None, act_a, xo)
            self.F.append(f)
            act_a.add_post(f)

            f = self.add_flow(next(oid), None, act_b, xo)
            self.F.append(f)
            act_b.add_post(f)

            fin.to_node = xi
            fout.fr_node = xo

        def expand_ior(act_a):
            """Expand Activity a to IOR o-a|b-o with original start and end points."""
            fin = act_a.pre[0]  # We know there is only one
            fout = act_a.post[0]
            act_a.rm_pre(fin)
            act_a.rm_post(fout)
            aid_b = next(oid)
            act_b = self.add_act(aid_b, aid_b)
            self.Act.append(act_b)

            iid = next(oid)
            iior = self.add_ior(iid, iid, DIVERGING)
            self.IOR.append(iior)

            iid = next(oid)
            io = self.add_ior(iid, iid, CONVERGING)
            self.IOR.append(io)

            f = self.add_flow(next(oid), None, iior, act_a)  # Connect the IOR structure
            self.F.append(f)
            act_a.add_pre(f)

            f = self.add_flow(next(oid), None, iior, act_b)
            self.F.append(f)
            act_b.add_pre(f)

            f = self.add_flow(next(oid), None, act_a, io)
            self.F.append(f)
            act_a.add_post(f)

            f = self.add_flow(next(oid), None, act_b, io)
            self.F.append(f)
            act_b.add_post(f)

            fin.to_node = iior
            fout.fr_node = io

        def expand_and(act_a):
            """Expand Activity a to AND o-a||b-o with original start and end points."""
            fin = act_a.pre[0]  # We know there is only one
            fout = act_a.post[0]
            act_a.rm_pre(fin)
            act_a.rm_post(fout)
            aid_b = next(oid)
            act_b = self.add_act(aid_b, aid_b)
            self.Act.append(act_b)

            aid_b = next(oid)
            ai = self.add_pll(aid_b, aid_b, DIVERGING)
            self.AND.append(ai)

            aid_b = next(oid)
            ao = self.add_pll(aid_b, aid_b, CONVERGING)
            self.AND.append(ao)

            f = self.add_flow(next(oid), None, ai, act_a)  # Connect the AND structure
            self.F.append(f)
            act_a.add_pre(f)

            f = self.add_flow(next(oid), None, ai, act_b)
            self.F.append(f)
            act_b.add_pre(f)

            f = self.add_flow(next(oid), None, act_a, ao)
            self.F.append(f)
            act_a.add_post(f)

            f = self.add_flow(next(oid), None, act_b, ao)
            self.F.append(f)
            act_b.add_post(f)

            fin.to_node = ai
            fout.fr_node = ao

        # Counters
        (seq, xor, ior, pll) = (0, 0, 0, 0)

        # Roulette probabilities for expanding nodes
        # (IOR issues with creating guards that cannot be well detected.)
        if p_structs:  # previously confirmed summation to 1.0
            p_seq, p_xor, p_ior, p_and = p_structs
        else:
            p_seq = 0.6  # 0.5
            p_xor = 0.1
            p_ior = 0.0  # 0.15
            p_and = 0.3  # 0.15
        p = [p_seq, p_xor, p_ior, p_and]
        [p_seq, p_xor, p_ior, p_and] = [p[i] + sum(p[0:i]) for i in range(len(p))]

        # Initialise base model o-A-o
        oid = stroid()
        name = ""
        self.s = self.add_start(next(oid), name)
        self.e = self.add_end(next(oid), name)

        aid = next(oid)
        a = self.add_act(aid, aid)
        self.Act.append(a)

        f1 = self.add_flow(next(oid), None, self.s, a)
        f2 = self.add_flow(next(oid), None, a, self.e)
        a.add_pre(f1)
        a.add_post(f2)
        self.F.append(f1)
        self.F.append(f2)

        # Expand random Activities.
        random.seed(time.time())

        for _ in range(1, modelsize):
            ii = random.randint(0, len(self.Act) - 1)  # which Activity to expand
            a = self.Act[ii]
            p = random.random()  # dice roll
            if p < p_seq:
                expand_seq(a)
                seq += 1
            elif p < p_xor:
                expand_xor(a)
                xor += 1
            elif p < p_ior:
                expand_ior(a)
                ior += 1
            elif p < p_and:
                expand_and(a)
                pll += 1

        return seq, xor, ior, pll

    # pylint: disable=too-many-arguments
    def print(self, ptype=None, tree=None, js_tplt="TPLT_JS_VIEW",
              html_tplt="TPLT_HT_VIEW", fn="viewbpmn"):
        """Pretty output of BPMN+V.

        Output original BPMN (perhaps modified), as .bpmn, in internal representation format,
        or GraphViz dot.

        :param ptype: "bpmn", "dot", or none
        :param tree: lxml.etree object (default self.etree)
        :param html_tplt: Javascript template file
        :param js_tplt: Javascript template file
        :param fn: template to produce fn.{html,js} files
        :return: n/a
        """
        fn_html = fn + ".html"
        fn_js = fn + ".js"
        fn_bpmn = fn + ".bpmn"
        if ptype == "bpmn":
            # Output HTML / JS combination using the templates.
            if tree is None:
                tree = self.tree

            # String replace default namespace code as can't see how to set it directly.
            bioc_regexp = re.compile("ns[0-9]+([=:])")

            # Output the .html file pointing to the bespoke .js
            with open(fn_bpmn, 'w') as bpmn:
                with open(html_tplt, 'r') as tplt:
                    with open(fn_html, 'w') as html:
                        for line in tplt:
                            html.write(line.replace("[JSFILE]", fn + ".js"))

                # Output into the .js wrapper for bpmn-js (bpmn.io)
                sbpmn = etree.tostring(tree, pretty_print=True)

                with open(js_tplt, 'r') as tplt:
                    with open(fn_js, 'w') as js:
                        line = tplt.readline()

                        # Copy first part from .js template up to [OVERLAYS] marker
                        while line is not None and line.strip() != "[OVERLAYS]":
                            js.write(line)
                            line = tplt.readline()
                        line = tplt.readline()

                        # Insert overlays to annotate activities with internal constraints or
                        # between-model conflicts.
                        for key in self.constracts:
                            act = self.constracts[key]
                            anntext = "Inconsistent"
                            acts = set(act["conflictingacts"])
                            if acts:
                                anntext += " with <b>this</b> " + ", ".join(acts) \
                                         + " when initial " + ", ".join(act["initvars"])\
                                         + ": now " + ", ".join(act["conflictvars"])
                            else:
                                anntext += " when initial " + ", ".join(act["initvars"])\
                                         + " (now " + ", ".join(act["conflictvars"]) + ')'

                            js.write("overlays.add('" + key + "', 'constr', {\n")
                            js.write("  position: {\n")
                            js.write("    top: 0,\n")
                            js.write("    left: -85\n")
                            js.write("  },\n")
                            js.write("  html: '<div class=\"diagram-constr\">"
                                     + anntext + "</div>'\n")
                            js.write("});\n")

                        for key in self.conflictacts:
                            act = self.conflictacts[key]
                            anntext = "Conflict with <b>other</b> "\
                                      + ", ".join(set(act["conflictingacts"])) \
                                      + " when initial " + ", ".join(act["initvars"]) + ": now " \
                                      + ", ".join(act["conflictvars"])

                            js.write("overlays.add('" + key + "', 'conflict', {\n")
                            js.write("  position: {\n")
                            js.write("    bottom: 15,\n")
                            js.write("    right: 15\n")
                            js.write("  },\n")
                            js.write("  html: '<div class=\"diagram-conflict\">"
                                     + anntext + "</div>'\n")
                            js.write("});\n")

                        # Copy second part from .js template up to [INSERTXML] marker
                        while line is not None and line.strip() != "[INSERTXML]":
                            js.write(line)
                            line = tplt.readline()

                        # Insert BPMN XML into variable.
                        # js.write(sbpmn.decode("UTF-8").replace("ns0", "bioc"))
                        js.write(bioc_regexp.sub(r"bioc\1", sbpmn.decode("UTF-8")))
                        bpmn.write(bioc_regexp.sub(r"bioc\1", sbpmn.decode("UTF-8")))

                        # Copy remainder from .js template
                        for line in tplt:
                            js.write(line)


        elif ptype == "dot":
            # Output graph header, defaults etc.
            print('digraph BPMN_V {')
            print('  forcelabels=true;')
            print('  ranksep="%s"; fontsize="%s"; remincross=%s; '
                  % (DG_RANKSEP, DG_FONTSIZE, DG_REMINCROSS))
            print('margin="%s"; fontname="%s";rankdir="%s";'
                  % (DG_MARGIN, DG_FONTNAME, DG_RANKDIR))

            print('  edge [arrowsize="%s", fontname="%s",fontsize="%s"];' %
                  (E_ARROWSIZE, E_FONTNAME, E_FONTSIZE))

            print('  node [height="%s",width="%s",fontname="%s",'
                  % (N_HEIGHT, N_WIDTH, N_FONTNAME))
            print('fontsize="%s"];' % N_FONTSIZE)

            # Start and End nodes
            for o in [self.s]:
                label = ''
                xlabel = None if o.name in [None, ""] else o.name.replace("\n", "\\n")
                xlabel = wrap(xlabel)
                if len(list(self.vardefs.keys())) >= 1:
                    b_first = True
                    for var in self.vardefs:
                        xlabel += "\\n"
                        if b_first:
                            xlabel += '('
                            b_first = False
                        xlabel += self.vardefs[var]["type"] + ": " + var
                    xlabel += ')'
                if xlabel is None:
                    print('  %s [shape="circle",label="%s",height=%s,width=%s' %
                          (o.oid, label, 0.3, 0.3), end='')
                else:
                    print(
                        '  %s [shape="circle",label="%s",xlabel="%s",height=%s,width=%s' %
                        (o.oid, label, xlabel, 0.3, 0.3), end='')
                print("];")

            for o in [self.e]:
                label = ''
                xlabel = None if o.name in [None, ""] else o.name.replace("\n", "\\n")
                xlabel = wrap(xlabel)
                if xlabel is None:
                    print('  %s [shape="circle",label="%s",height=%s,width=%s' %
                          (o.oid, label, 0.3, 0.3), end='')
                else:
                    print(
                        '  %s [shape="circle",label="%s",xlabel="%s",height=%s,width=%s' %
                        (o.oid, label, xlabel, 0.3, 0.3), end='')
                print(",penwidth=2];")

            # Activities.
            for a in self.Act:
                label = a.name.replace("\n", "\\n")
                xlabel = None
                if a.guard is not None:
                    # label = label + "\\n[" + a.guard + ']'
                    xlabel = '[' + a.guard + ']'
                label = wrap(label)
                if a.data is not None:
                    if xlabel is None:
                        xlabel = ''
                    xlabel += "[" + a.data + ']'
                if a.oid in self.conflictacts:
                    act = self.conflictacts[a.oid]
                    anntext = " Conflict with other " \
                              + ", ".join(set(act["conflictingacts"])) \
                              + " when initial " + ", ".join(act["initvars"]) + ": now " \
                              + ", ".join(act["conflictvars"])
                    xlabel += anntext
                if a.oid in self.constracts:
                    act = self.constracts[a.oid]
                    anntext = " Inconsistent"
                    acts = set(act["conflictingacts"])
                    if acts:
                        anntext += " with this " + ", ".join(acts) \
                                   + " when initial " + ", ".join(act["initvars"]) + ": now " \
                                   + ", ".join(act["conflictvars"])
                    else:
                        anntext += " when initial " + ", ".join(act["initvars"]) + " (now " \
                                   + ", ".join(act["conflictvars"]) + ')'
                    xlabel += anntext
                if xlabel is None:
                    print('  %s [shape="box",label="%s"' %
                          (a.oid, label), end='')
                else:
                    xlabel = wrap(xlabel)
                    print('  %s [shape="box",' % a.oid, end='')
                    print('label="%s",xlabel="%s"' % (label, xlabel), end='')
                if a.oid in self.conflictacts:
                    print(',style=filled,fillcolor=red', end='')
                elif a.oid in self.constracts:
                    print(',style=filled,fillcolor=lightcoral', end='')
                elif label.startswith(ETA):
                    print(",style=filled,fillcolor=black,fontcolor=white,width=0.1", end='')
                print("];")

            # Gateways.
            for g in self.XOR + self.IOR + self.AND:
                label = 'X' if isinstance(g, XOR) else 'O' if isinstance(g, IOR) else '+'
                xlabel = None
                if g.name is not None:
                    # noinspection PyUnresolvedReferences
                    xlabel = wrap(g.name.replace("\n", "\\n"))
                    print(
                        '  %s [shape="diamond",label="%s",xlabel="%s",width=%s,height=%s' %
                        (g.oid, label, xlabel, 0.3, 0.3), end='')
                else:
                    print('  %s [shape="diamond",label="%s",width=%s,height=%s' %
                          (g.oid, label, 0.3, 0.3), end='')
                if xlabel is None:
                    print(",fontsize=16];")
                else:
                    print("];")

            # Sequence Flows (arcs)
            for f in self.F:
                penwidth = 1
                if isinstance(f.fr_node, XOR):
                    if f.fr_node.default == f:
                        penwidth = 3
                if f.name is not None:
                    print('  %s -> %s [label="%s",penwidth=%i];'
                          % (f.fr_node.oid, f.to_node.oid, f.name, penwidth))
                else:
                    print('  %s -> %s [penwidth=%i];' % (f.fr_node.oid, f.to_node.oid, penwidth))

            # Print graph trailer.
            print("}")
        else:
            for v in self.vardefs:
                print("var:", v, ':', self.vardefs[v]["type"], self.vardefs[v]["value"])
            self.s.print()
            self.e.print()
            for a in self.Act:
                a.print()
            for x in self.XOR:
                x.print()
            for i in self.IOR:
                i.print()
            for a in self.AND:
                a.print()
            for f in self.F:
                f.print()

    def parse_text(self, bpmnid):
        """Import BPMN+V model from simple text format.

        Note: cannot specify data and guard annotations in this method.

        :param bpmnid: open file ID
        """
        name_map = {}  # Temporary map object names to objects
        oid = genoid()  # Generator for object IDs
        for line in bpmnid:
            if line[0] != '#':
                # Node records start with particular strings
                fields = line.strip().split()
                recid = fields[0]
                if recid == START_REC:
                    sid = 's' + str(next(oid))
                    self.s = self.add_start(sid, fields[1])  # Only allow one start node
                    name_map[fields[1]] = self.s  # Used to identify flows

                elif recid == END_REC:
                    eid = 'e' + str(next(oid))
                    self.e = self.add_end(eid, fields[1])
                    name_map[fields[1]] = self.e

                elif recid == ACT_REC:
                    for name in fields[1:]:
                        aid = "act" + str(next(oid))
                        obj_act = self.add_act(aid, name)
                        self.Act.append(obj_act)
                        self.obj_map[aid] = obj_act
                        name_map[name] = obj_act

                elif recid == XORDIV_REC:
                    for name in fields[1:]:
                        xid = "xor" + str(next(oid))
                        direction = DIVERGING
                        default = None  # Not specified in text file
                        obj_xor = self.add_xor(xid, name, direction, default)
                        self.XOR.append(obj_xor)
                        name_map[name] = obj_xor

                elif recid == XORCONV_REC:
                    for name in fields[1:]:
                        xid = "xor" + str(next(oid))
                        direction = CONVERGING
                        default = None  # Not specified in text file
                        obj_xor = self.add_xor(xid, name, direction, default)
                        self.XOR.append(obj_xor)
                        name_map[name] = obj_xor

                elif recid == PLLDIV_REC:
                    for name in fields[1:]:
                        aid = "pll" + str(next(oid))
                        direction = DIVERGING
                        default = None  # Not specified in text file
                        obj_and = self.add_pll(aid, name, direction)
                        self.AND.append(obj_and)
                        name_map[name] = obj_and

                elif recid == PLLCONV_REC:
                    for name in fields[1:]:
                        aid = "pll" + str(next(oid))
                        direction = CONVERGING
                        default = None  # Not specified in text file
                        obj_and = self.add_pll(aid, name, direction)
                        self.AND.append(obj_and)
                        name_map[name] = obj_and

                elif recid == IORDIV_REC:
                    for name in fields[1:]:
                        iid = "ior" + str(next(oid))
                        direction = DIVERGING
                        default = None  # Not specified in text file
                        obj_ior = self.add_ior(iid, name, direction)
                        self.IOR.append(obj_ior)
                        name_map[name] = obj_ior

                elif recid == IORCONV_REC:
                    for name in fields[1:]:
                        iid = "ior" + str(next(oid))
                        direction = CONVERGING
                        default = None  # Not specified in text file
                        obj_ior = self.add_ior(iid, name, direction)
                        self.IOR.append(obj_ior)
                        name_map[name] = obj_ior

                else:
                    if len(fields) == 2:
                        fid = "flow" + str(next(oid))
                        name = None
                        fr_node = name_map[fields[0]]
                        to_node = name_map[fields[1]]
                        obj_flo = self.add_flow(fid, name, fr_node, to_node)
                        self.F.append(obj_flo)
                    else:
                        raise Exception("Record", line, "not understood in", modelfile)
        bpmnid.close()

        # Resolve IOR pairings (or error).
        self.IOR_pairs_to_obj()

        return None

    def parse_bpmn(self, bpmnid):
        """Import BPMN: currently assumes BPMN.io format (if it matters).

        Pre-process XML to string and basic removal of namespace information,
        to allow for older versions of LXML.

        :param bpmnid: open file ID

        ASSUMPTION:  Variable:type list as annotation to Start node;
                     type in ["INT", "REAL", "BOOL", "STRING"].
                     Optional text annotations to activity "guard:" or "data:"
                     to specify transition guard or data modification.
        """
        s = ""
        for line in bpmnid:
            # bpmn-js: bpmn prefix, Eclipse BPMN Modeller: bpmn2 prefix
            s += line.replace("bpmn2", "bpmn")

        b = s.encode("utf-8")  # Seems to be required for Python 3
        tree = etree.parse(BytesIO(b))

        root = tree.getroot()
        ns = {"bpmn": root.nsmap["bpmn"]}

        bpmn = root.find("bpmn:process", ns)

        # Start node
        s_event = bpmn.find("bpmn:startEvent", ns)
        sid = s_event.attrib["id"]
        try:
            name = s_event.attrib["name"]
        except KeyError:
            name = ''
        self.s = self.add_start(sid, name)

        # End node
        e_event = bpmn.find("bpmn:endEvent", ns)
        eid = e_event.attrib["id"]
        try:
            name = e_event.attrib["name"]
        except KeyError:
            name = ''
        self.e = self.add_end(eid, name)

        # Activities
        act = bpmn.findall("bpmn:task", ns)
        for a in act:
            aid = a.attrib["id"]
            name = a.attrib["name"]
            obj_act = self.add_act(aid, name)
            self.Act.append(obj_act)

        # XOR splits (exclusive OR)
        xor = bpmn.findall("bpmn:exclusiveGateway", ns)
        for x in xor:
            xid = x.attrib["id"]
            try:
                name = x.attrib["name"]
            except KeyError:
                name = None
            direction = get_direction(x, ns)
            try:
                default = x.attrib["default"]  # (replaced later with SequenceFlow)
            except KeyError:
                default = None
            obj_xor = self.add_xor(xid, name, direction, default)
            self.XOR.append(obj_xor)

        # IOR splits (inclusive OR)
        ior = bpmn.findall("bpmn:inclusiveGateway", ns)
        for i in ior:
            iid = i.attrib["id"]
            try:
                name = i.attrib["name"]
            except KeyError:
                name = None
            direction = get_direction(i, ns)
            # Assuming no default
            obj_ior = self.add_ior(iid, name, direction)
            self.IOR.append(obj_ior)

        # Pair IOR div and conv splits by naming convention.  This is required (at present)
        # for the Z3 solving, to ensure the same number of paths are completed (before conv)
        # as started (following div) -- to avoid parsing the model to find matches.
        ior_divs = [ior for ior in self.IOR if ior.direction == DIVERGING]
        ior_convs = [ior for ior in self.IOR if ior.direction == CONVERGING]
        b_match_iors = True
        if len(ior_divs) != len(ior_convs):
            b_match_iors = False

        if not b_match_iors:
            print("IOR gateways cannot be matched by name.")
            exit(1)

        # AND splits (parallel)
        gw_and = bpmn.findall("bpmn:parallelGateway", ns)
        for a in gw_and:
            aid = a.attrib["id"]
            try:
                name = a.attrib["name"]
            except KeyError:
                name = None
            direction = get_direction(a, ns)
            # Assuming no default
            obj_and = self.add_pll(aid, name, direction)
            self.AND.append(obj_and)

        # Sequence Flows: must be imported after node objects
        flow = bpmn.findall("bpmn:sequenceFlow", ns)
        for f in flow:
            fid = f.attrib["id"]
            try:
                name = f.attrib["name"]
            except KeyError:
                name = None
            fr_node = self.obj_map[f.attrib["sourceRef"]]
            to_node = self.obj_map[f.attrib["targetRef"]]
            obj_flo = self.add_flow(fid, name, fr_node, to_node)
            self.F.append(obj_flo)

        # Replace XOR default flow names with Sequence Flow objects
        for x in self.XOR:
            if x.default is not None:
                x.default = self.obj_map[x.default]

        # Annotations:
        #   text beginning: "guard:" for activity guard.
        #                   "data:" for data modification statement on activity.
        #                   "int|real|string|bool:" for var def (on start event).
        # 1. Find and save annotations.
        act = bpmn.findall("bpmn:textAnnotation", ns)
        for a in act:
            aid = a.attrib["id"]
            text = a.find("bpmn:text", ns).text
            text = text.split(':')
            if len(text) > 1:
                anntype = text[0]  # expect "guard:" etc.
                anntext = ':'.join(text[1:]).strip()  # allow for ':' in remainder
                if anntype == "guard":
                    self.guards[aid] = anntext
                elif anntype == "data":
                    self.data[aid] = anntext
                elif anntype.upper() in self.validVarTypes:
                    ann = anntype + ':' + anntext  # allow for list of vars
                    vardefs = ann.split(',')
                    for vardef in vardefs:
                        [anntype, anntext] = vardef.split(':')
                        self.vardefs[anntext.strip()] = {"type": anntype.strip().upper(),
                                                      "value": None}
                else:
                    print("Ignoring invalid annotation", anntype + ": " + anntext)

        # 2. Find associations and use to allocate annotations to correct objects.
        act = bpmn.findall("bpmn:association", ns)
        for a in act:
            fr_node = a.attrib["sourceRef"]
            to_node = a.attrib["targetRef"]
            if to_node in self.guards.keys():
                if fr_node in self.obj_map and isinstance(self.obj_map[fr_node], Activity):
                    if self.obj_map[fr_node].guard is None:
                        self.obj_map[fr_node].guard = self.guards[to_node]
                    else:
                        print(fr_node, "already has a guard!")
                else:
                    print("Ignoring invalid association!", self.guards[to_node])
            elif to_node in self.data.keys():
                if fr_node in self.obj_map and isinstance(self.obj_map[fr_node], Activity):
                    if self.obj_map[fr_node].data is None:
                        self.obj_map[fr_node].data = self.data[to_node]
                    else:
                        print(fr_node, "already has a data annotation!")
                else:
                    print("Ignoring invalid association!", self.data[to_node])

        # Resolve IOR pairings (or error).
        self.IOR_pairs_to_obj()

        return tree

    def compose(self, bpmnvs):
        """Populate BPMNV through simple parallel composition of bpmnv1 and bpmnv2.

        Copy the components and variables, modifying the names.

        Create new start- and end- nodes and parallel div/join linked to dest of source models' start- arcs,
        source of source models' -end arcs, resp.
        Drop original start and end nodes.
        Merge the variables.

        :param bpmnvs: list of source models (initially expect to be 2)
        """
        oid = genoid()  # Generator for object IDs

        # Create new start and end node
        oid = "s0"
        self.s = self.add_start(oid, "s")
        oid = "e0"
        self.e = self.add_end(oid, "e")

        # Create new parallel gateways
        oid = "c_s"
        pll_s = self.add_pll(oid, oid, DIVERGING)
        self.AND.append(pll_s)
        oid = "c_e"
        pll_e = self.add_pll(oid, oid, CONVERGING)
        self.AND.append(pll_e)

        for ii in range(len(bpmnvs)):
            bpmnv = bpmnvs[ii]
            # self.guards = {}  # Save annotations which are guards before add to Activity
            # self.data = {}  # Save annotations which are data mods b/f add to Activity
            # self.cover_token_set = []  # List of tokens (data valuations) to explore the net

            # Copy structure
            for _act in bpmnv.Act:
                act = copy(_act)
                if not act.name:
                    act.name = ''
                act.name += "_" + str(ii)
                act.oid = str(act.oid) + "_" + str(ii)
                self.Act.append(act)
                self.obj_map[act.oid] = act

            for _xor in bpmnv.XOR:
                xor = copy(_xor)
                if not xor.name:
                    xor.name = ''
                xor.name += "_" + str(ii)
                xor.oid = str(xor.oid) + "_" + str(ii)
                self.XOR.append(xor)
                self.obj_map[xor.oid] = xor

            for _ior in bpmnv.IOR:
                ior = copy(_ior)
                if not ior.name:
                    ior.name = ''
                ior.name += "_" + str(ii)
                ior.oid = str(ior.oid) + "_" + str(ii)
                self.IOR.append(ior)
                self.obj_map[ior.oid] = ior

            for _pll in bpmnv.AND:
                pll = copy(_pll)
                if not pll.name:
                    pll.name = ''
                pll.name += "_" + str(ii)
                pll.oid = str(pll.oid) + "_" + str(ii)
                self.AND.append(pll)
                self.obj_map[pll.oid] = pll

            for _flow in bpmnv.F:
                flow = copy(_flow)
                if flow.name:
                    flow.name += "_" + str(ii)
                flow.oid = str(flow.oid) + "_" + str(ii)
                if flow.fr_node == bpmnv.s:
                    flow.fr_node = pll_s
                else:
                    flow.fr_node = self.obj_map[str(flow.fr_node.oid) + "_" + str(ii)]
                if flow.to_node == bpmnv.e:
                    flow.to_node = pll_e
                else:
                    flow.to_node = self.obj_map[str(flow.to_node.oid) + "_" + str(ii)]

                self.F.append(flow)
                self.obj_map[flow.oid] = flow

                # Merge variables
            for var in bpmnv.vardefs:
                if var not in self.vardefs:
                    self.vardefs[var] = bpmnv.vardefs[var]  # NB value is object
                elif bpmnv.vardefs[var]["type"] != self.vardefs[var]["type"]:
                    raise Exception("Incompatible types (", bpmnv.vardefs[var]["type"] + ','
                                    + self.vardefs[var]["type"] + ") for", var, "in compose.")

        # Join the BPMNV models  with: start: new start event and parallel diverging gateway,
        # end: parallel converging gateway and new end event. connected to the destination nodes of the
        # arcs from the component model start events, and the sources of the component model end events,
        # respectively: to allow the two original nets to run in parallel.

        # Link to start and end nodes
        oid = "s0_pll0"
        flow = self.add_flow(oid, None, self.s, pll_s)
        self.F.append(flow)
        oid = "pll1_e0"
        flow = self.add_flow(oid, None, pll_e, self.e)
        self.F.append(flow)

        for bpmnv in bpmnvs:
            # Remove Start and End node (no action)

            # Relink arcs involving start- and end- nodes to the new parallel gateways.
            # (We already copied the arcs but not the start- and end- nodes.)
            # for arc in bpmnv.s.out_arcs:
            for flow in [ff for ff in self.F if ff.fr_node == bpmnv.s]:
                flow.fr_node = pll_s

            for flow in [ff for ff in self.F if ff.to_node == bpmnv.e]:
                flow.to_node = pll_e

        return

    def cover_tokens(self):
        """Build a set of tokens (data valuations) that covers all the guard decisions.

        I.e. allow exploration of the full behaviour (e.g. via Z3).

        NB: This is quite naive: does it really explore the full net?
        LIMITATION: XXX Perhaps should be a set.
        """
        # For all guards:
        #   parse the guard, extract the variable x, negation, op, val
        #   if 'x' or "not x":
        #     create tokens for x in [True, False]
        #   if "x op val":
        #     if >= or <, need token values (a,b) = (val-1, val) for x
        #     if > or <=, need token values (a,b) = (val, val+1) for x
        #   if no tokens, create 2 tokens with just x, values a and b
        #   if tokens, add x, duplicating all tokens for x = a and b

        # 1. build a dictionary of variables to value pairs.
        gr = BPMNVparse.Parser()

        var_vals = {}  # {var: [v1, v2]}
        self.cover_token_set = []
        for a in self.Act:
            checkvals = gr.aguard.guard_testvals(a.guard, self.vardefs)
            if checkvals:  # No parsed guard(s)
                for ck in checkvals:  # (var, (range pair)) tuple
                    var = ck[0]  # string
                    ck0 = ck[1]  # pair
                    if not ck0:
                        raise Exception("Problem with variable settings", ck)
                    for v in ck0:
                        if var not in var_vals:
                            var_vals[var] = [v]
                        else:
                            if v not in var_vals[var]:
                                var_vals[var].append(v)

        # TBC Add proper checking that all variables in the model are defined.
        if not var_vals:
            # Dummy token
            self.cover_token_set = None
        else:
            # 2. build tokens of unique combinations.
            for var in var_vals:
                vals = var_vals[var]
                if not self.cover_token_set:  # no tokens created yet
                    for v in vals:
                        vtype = self.vardefs[var]["type"]
                        tokendict = {var: {"type": vtype, "value": v}}
                        self.cover_token_set.append(tokendict)  # should be object?
                else:
                    newtokens = []  # Not efficient and perhaps should be recursive.
                    for t in self.cover_token_set:
                        for v in vals:
                            newt = deepcopy(t)
                            vtype = self.vardefs[var]["type"]
                            # tokendict = {var: {"type": vtype, "value": v}}
                            # newt.add(tokendict)
                            newt[var] = {"type": vtype, "value": v}
                            newtokens.append(newt)
                    self.cover_token_set = newtokens
        return self.cover_token_set

    def colour_conflicts(self, conflicts, fn, composed=False):
        """Modify XML to colour XML output BPMN activities for conflict.

        Both internal to a BPMN and between, reported as conflicting or
        conflicted-with.
        :param self:
        :param conflicts: print-format as produced by CPNAnalysis.reach_anal:
          fn1, Activity, var, guard, modvar, data-mod, token, inittoken, type,
          fn2, conflicting Activities, conflicting vars
        :param fn: filename or index (to select from conflicts)
        :param composed: set if we are colouring conflicts from a composed model
        :return: None
        """
        # Indices to conflcts[]
        FN = 0
        ACTIVITY = 1
        TOKEN = 6
        INITTOKEN = 7
        FN2 = 9
        CONFLICTS = 10

        conflicts = [c for c in conflicts if c[FN] == str(fn) or c[FN2] == str(fn)]

        if composed:
            acts = self.conflictacts  # between-model conflicts
            fillcolour = "#ff0000"
        else:
            acts = self.constracts  # model-internal conflicts
            fillcolour = "#ff8888"

        # Create dict of conflicts at the BPMN object level, for adding annotations to the output.
        for conflict in conflicts:
            aid = conflict[ACTIVITY].oid
            conflictingacts = [c.name for c in conflict[CONFLICTS]]

            if conflict[FN] == str(fn):
                if aid in acts:  # If activity already listed
                    acts[aid]["conflictingacts"] += conflictingacts
                    acts[aid]["initvars"] += [conflict[INITTOKEN]]
                    acts[aid]["conflictvars"] += [conflict[TOKEN]]
                else:
                    acts[aid] = {"conflictingacts" : conflictingacts,
                                 "initvars" : [conflict[INITTOKEN]],
                                 "conflictvars" : [conflict[TOKEN]]
                                }

            if conflict[FN2] == str(fn):
                for conflictact in conflict[CONFLICTS]:
                    if conflictact in acts:
                        acts[conflictact.oid]["conflictingacts"] += conflict[ACTIVITY].name
                        acts[conflictact.oid]["initvars"] += [conflict[INITTOKEN]]
                        acts[conflictact.oid]["conflictvars"] += [conflict[TOKEN]]
                    else:
                        acts[conflictact.oid] \
                            = {"conflictingacts": [conflict[ACTIVITY].name],
                               "initvars": [conflict[INITTOKEN]],
                               "conflictvars": [conflict[TOKEN]]
                              }

        # Colouring: set of conflicting activities
        conflictacts = set([conflict[ACTIVITY] for conflict in conflicts] \
                         + [conflict for other in conflicts for conflict in other[CONFLICTS]])
        conflictactsL = list(conflictacts)
        conflictnamesL = [a.oid for a in conflictactsL]

        # Modify the lxml if it exists (e.g. will not if generated random BPMNs)
        if self.tree:
            root = self.tree.getroot()
            ns = {"bpmndi": root.nsmap["bpmndi"]}
            dia = root.find("bpmndi:BPMNDiagram", ns)
            plane = dia.find("bpmndi:BPMNPlane", ns)
            shapes = plane.findall("bpmndi:BPMNShape", ns)

            for shape in shapes:  # XXX Need to ensure inter-model conflict overrides?
                if shape.attrib["bpmnElement"] in conflictnamesL:
                    ind = conflictnamesL.index(shape.attrib["bpmnElement"])

                    if conflictactsL[ind] in self.Act:
                        shape.set("{http://bpmn.io/schema/bpmn/biocolor/1.0}stroke", "#000000")
                        shape.set("{http://bpmn.io/schema/bpmn/biocolor/1.0}fill", fillcolour)

    def IOR_pairs_to_obj(self):
        """Convert string pair names to object IDs to enable IOR div/conv pairint."""

        divs = sorted([ior.name for ior in self.IOR if ior.direction == DIVERGING])
        convs = sorted([ior.name.rstrip('_') for ior in self.IOR if ior.direction == CONVERGING])
        if divs == convs:
            objs = {ior.name: ior for ior in self.IOR}
            for ior in self.IOR:
                ior.pair = objs[ior.pair]
        else:
            raise Exception("Mismatching IOR div and conv")


def get_direction(gw, ns=None):
    """Determine direction of BPMN+V gateway defined in XML import."""
    try:
        direction = gw.attrib["gatewayDirection"]
    except KeyError:
        ins = gw.findall("bpmn:incoming", ns)
        outs = gw.findall("bpmn:outgoing", ns)
        if len(ins) == 1 and len(outs) > 1:
            direction = DIVERGING
        elif len(outs) == 1 and len(ins) > 1:
            direction = CONVERGING
        else:
            direction = None
    return direction


# Class-level functions
def rnd_conflict(bpmns, conflicts, p_conflicts=None):
    """Add random guard/data modification conflict between two BPMN+V models.

    B: list of 2 BPMN+V models.
    conflicts: how many conflicts to add.
    """
    chri = 97  # Variable names from 'a'
    if len(bpmns) < 2:
        bpmns.append(deepcopy(bpmns[0]))  # Temporary bodge to allow test w/o composition.
        # raise Exception("rnd_conflict requires a list of 2 BPMNV+V models.")

    cf1v1m = [0, 0]
    cf2v1m = [0, 0]
    cf1v2m = 0
    cf2v2m = 0

    # Add conflicts that cause the models to block each other, for simplicity of
    # analysis.  We may want to relax this in the future.
    for ii in range(conflicts):
        # 2 types of conflict (may overlap, in one or two models):
        # 1.  Mimic conflicting drug prescriptions:
        #     Model A: a=false => b=true | a<1 => b+1
        #     Model B: b=false => a=true | b<1 => a+1
        # 2.  Mimic over-prescription:
        #     Model A: a=false => a=true | a<1 => a+1
        #     Model B: ------------- ditto ----------

        # Allow: 1. over-prescription conflicts (in one or between models)
        # i.e. two activities have both [a>1][a+1]
        #        2. drug-drug conflicts (in one or between models)
        # i.e. two activities have [a>1][b+1] and [b>1][a+1]

        if p_conflicts:
            p_1v_1m, p_2v_1m, p_1v_2m, p_2v_2m = p_conflicts
        else:
            if len(bpmns[0].Act) > 1 and len(bpmns[1].Act) > 1:
                p_1v_1m = 0.1  # Roulette for adding various conflicts
                p_2v_1m = 0.1
                p_1v_2m = 0.4
                p_2v_2m = 0.4
            else:
                p_1v_1m = 0
                p_2v_1m = 0
                p_1v_2m = 0.5
                p_2v_2m = 0.5
        p = [p_1v_1m, p_2v_1m, p_1v_2m, p_2v_2m]
        [p_1v_1m, p_2v_1m, p_1v_2m, _] = [p[i] + sum(p[0:i]) for i in range(len(p))]
        p = random.random()

        # Single model
        # 1. over-prescription
        if p < p_1v_1m:
            ii = random.randint(0, 1)
            a1i = random.randint(0, len(bpmns[ii].Act) - 1)  # Activity 1
            a2i = copy(a1i)
            while a2i == a1i:
                a2i = random.randint(0, len(bpmns[ii].Act) - 1)  # Activity 2

            a1 = bpmns[ii].Act[a1i]
            a2 = bpmns[ii].Act[a2i]
            if a1.guard is not None:
                a1.guard += ','
            else:
                a1.guard = ''
            if a1.data is not None:
                a1.data += ','
            else:
                a1.data = ''
            if a2.guard is not None:
                a2.guard += ','
            else:
                a2.guard = ''
            if a2.data is not None:
                a2.data += ','
            else:
                a2.data = ''
            a1.guard += chr(chri) + "<1"
            a2.guard += chr(chri) + "<1"
            a1.data += chr(chri) + "+1"
            a2.data += chr(chri) + "+1"
            bpmns[ii].vardefs[chr(chri)] = {"type": "INT", "value": None}
            cf1v1m[ii] += 1

            # 2. drug-drug
        elif p < p_2v_1m:
            ii = random.randint(0, 1)
            a1i = random.randint(0, len(bpmns[0].Act) - 1)  # Guard/data Act 1
            a2i = copy(a1i)
            while a2i == a1i:
                a2i = random.randint(0, len(bpmns[1].Act) - 1)  # Guard/data Act 2

            a1 = bpmns[ii].Act[a1i]
            a2 = bpmns[ii].Act[a2i]
            chrj = copy(chri)  # Second variable
            chri += 1
            if a1.guard is not None:
                a1.guard += ','
            else:
                a1.guard = ''
            if a1.data is not None:
                a1.data += ','
            else:
                a1.data = ''
            if a2.guard is not None:
                a2.guard += ','
            else:
                a2.guard = ''
            if a2.data is not None:
                a2.data += ','
            else:
                a2.data = ''
            a1.guard += chr(chri) + "<1"
            a2.guard += chr(chrj) + "<1"
            a2.data += chr(chri) + "+1"
            a1.data += chr(chrj) + "+1"
            bpmns[ii].vardefs[chr(chri)] = {"type": "INT", "value": None}
            bpmns[ii].vardefs[chr(chrj)] = {"type": "INT", "value": None}
            cf2v1m[ii] += 1

        # Two models
        # Single variable, equivalent guard/mod in each model, on same activity.
        elif p < p_1v_2m:
            a1i = random.randint(0, len(bpmns[0].Act) - 1)  # Guard/Data Act in Model A
            a2i = random.randint(0, len(bpmns[1].Act) - 1)  # Guard/Data Act in Model B
            a1 = bpmns[0].Act[a1i]
            a2 = bpmns[1].Act[a2i]
            if a1.guard is not None:
                a1.guard += ','
            else:
                a1.guard = ''
            if a1.data is not None:
                a1.data += ','
            else:
                a1.data = ''
            if a2.guard is not None:
                a2.guard += ','
            else:
                a2.guard = ''
            if a2.data is not None:
                a2.data += ','
            else:
                a2.data = ''
            a1.guard += chr(chri) + "<1"
            a2.guard += chr(chri) + "<1"
            a1.data += chr(chri) + "+1"
            a2.data += chr(chri) + "+1"
            bpmns[0].vardefs[chr(chri)] = {"type": "INT", "value": None}
            bpmns[1].vardefs[chr(chri)] = {"type": "INT", "value": None}
            cf1v2m += 1

        # More than one variable, guard/data on one act, data/guard on other
        else:
            a1i = random.randint(0, len(bpmns[0].Act) - 1)  # Guard/Data Act in Model A
            a2i = random.randint(0, len(bpmns[1].Act) - 1)  # Data/Guard Act in Model B
            a1 = bpmns[0].Act[a1i]
            a2 = bpmns[1].Act[a2i]
            chrj = copy(chri)  # Second variable
            chri += 1
            if a1.guard is not None:
                a1.guard += ','
            else:
                a1.guard = ''
            if a1.data is not None:
                a1.data += ','
            else:
                a1.data = ''
            if a2.guard is not None:
                a2.guard += ','
            else:
                a2.guard = ''
            if a2.data is not None:
                a2.data += ','
            else:
                a2.data = ''
            a1.guard += chr(chri) + "<1"
            a2.guard += chr(chrj) + "<1"
            a1.data += chr(chrj) + "+1"
            a2.data += chr(chri) + "+1"
            bpmns[0].vardefs[chr(chri)] = {"type": "INT", "value": None}
            bpmns[1].vardefs[chr(chri)] = {"type": "INT", "value": None}
            bpmns[0].vardefs[chr(chrj)] = {"type": "INT", "value": None}
            bpmns[1].vardefs[chr(chrj)] = {"type": "INT", "value": None}
            cf2v2m += 1

        chri += 1

    # Return stats of numbers of conflicts generated
    return cf1v1m[0] + cf2v1m[0], cf1v1m[1] + cf2v1m[1], cf1v2m + cf2v2m

def genoid():
    """IDs for BPMN+V objects read from simple text format."""
    oid = 0
    while True:
        yield oid
        oid += 1

def wrap(s):
    """Naive heuristic wrapping of string to try to improve dot display."""
    minwrap = 10
    if isinstance(s, str) and len(s) > minwrap:
        n = int(sqrt(len(s)) * 2)
        s_ = '\\n'.join([s[ii:ii + n] for ii in range(0, len(s), n)])
        return s_
    return s


def rm_ctrl_chars(s):
    """Attempt to remove control characters from string."""
    return CONTROL_CHAR_RE.sub('', s)


def main():
    """Capture and alert command-line calls."""
    print("BPMNV.py: load as module only.")


if __name__ == "__main__":
    main()
