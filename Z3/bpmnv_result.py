#!/usr/bin/env python3
""" Phil Weber 2018-01-21 Class for storing results of BPMN+V model analysis under Z3.

Used in BPMNVrandom and bpmnv_model directly.
"""

class bpmnv_result(object):
    """Result structure for analysis of a set of BPMN+V models."""
    def __init__(self):
        """Initialise results arrays."""
        self.nodes = []
        self.flows = []
        self.acts = []
        self.seqs = []
        self.plls = []
        self.xors = []
        self.iors = []
        self.limits = []
        self.vals = []
        self.models = []
        self.secs = []
        self.cfs = []

    def extend(self):
        """Append zeros to each array for new experiment."""
        for ss in self.nodes, self.flows, self.acts, self.seqs, self.plls, self.xors, \
                  self.iors, self.limits, self.vals, self.models, self.secs, self.cfs:
            ss.append(0)

    def print(self):
        """Tidy(ish) output."""
        print("nodes: ", self.nodes)
        print("flows: ", self.flows)
        print("acts:  ", self.acts)
        print("seqs:  ", self.seqs)
        print("plls:  ", self.plls)
        print("xors:  ", self.xors)
        print("iors:  ", self.iors)
        print("limits:", self.limits)
        print("vals:  ", self.vals)
        print("models:", self.models)
        print("secs:  ", self.secs)
        print("cfs:   ", self.cfs)
