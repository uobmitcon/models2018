#!/usr/bin/env python3
"""Process variable definitions, transition guards and arc inscriptions in BPMN+V.

Phil Weber 2017-11-17

History: 2016-11-18: original version for CPN.

Summary:
  Limited version of CPN/Tools functionality for guards and arc inscriptions.
  1.  Variable definitions but no colour sets.
        vardef :: "var" var (, var)* : type ;
        var    :: [A-Za-z][A-Za-z0-9]*
        type   :: "BOOL" | "INT" | "REAL" | "STRING"
  2.  Transition guards.
        guard :: ['['] [not | !] var [op (real | int)] [']']
        op    :: = | >[=] | <[=]
        int   :: [0-9]+
        real  :: [0-9]+[.[0-9]*]
        Should allow:   x
                      ! x # ! not
                        x > 1 # > < >= <=
                        x =[=] {false|true}
  3.  (Post-transition) variable modification.
        mod :: [(] mod1+[)]
        mod1 :: var = [var op] num
        mod1 :: var = [var +] string
        mod1 :: [neg] var=bool
        op  :: + | - | * | /
        Should allow: x =   x + 1
                            x + 1 # implicitly modify x (CPN/Tools)
                      x = ! x
                      x =       3 # assignment (CPN/Tools doesn't do this)
                            x     # unchanged

To do:
  o  Formalise as a very limited Domain Specific Language (DSL).

Limitations:
  o  Only implimented explicit options; behaviour outside of these may be
     unpredictable (i.e. limited testing on the DSL): DSL and behaviour need
     to be completely specified.
  o  Some concerns over the scope of variables.
  o  Not allowed string comparisons on guards.
  o  Creation of test values for guards, in guard_testvals, is quite naive.
  o  Bodges around dealing with the grammar - ought to be fixed.

Changes:
  o  2016-11-22 Allow arc inscriptions both setting a variable (e.g. "a=a+2"),
     or as CPN/Tools, e.g. "a+2", "(a + 2)", "(b,c,a-1,d)" (as per CPN/Tools,
     (not extended code, conditional inscriptions).
  o  2016-11-28 Allow transition guards to have CSV-separated list of guards,
     e.g. CS<=1,NSAID<=1.
  o  2016-11-30 Enabled guards such as "b == False".
# o  2016-12-06 Changed updval to ignore missing variables, on the assumption
#    that these are inscription variables which feature on no guards and thus
#    on no tokens.
"""

from sys import path

try:
    import pyparsing
except ImportError:
    try:
        path.append("/usr/lib64/python2.6/site-packages/matplotlib")
        import pyparsing
    except ImportError:
        raise SystemError("Problem with importing modules.")


class Parser(object):  # pylint: disable=too-few-public-methods
    """Top-level object containing grammar objects.

    for interpreting:
      - variable definitions
      - variable assignments
      - variable checks (transition guards)
      - variable modifications (arc inscriptions)
    """
    sbool = pyparsing.CaselessLiteral("BOOL")
    sint = pyparsing.CaselessLiteral("INT")
    sreal = pyparsing.CaselessLiteral("REAL")
    sstring = pyparsing.CaselessLiteral("STRING")
    svar = pyparsing.Suppress(pyparsing.Literal("var"))

    comma = pyparsing.Suppress(pyparsing.Literal(','))
    colon = pyparsing.Suppress(pyparsing.Literal(':'))

    q = pyparsing.Suppress(pyparsing.Optional(pyparsing.Literal("'") | pyparsing.Literal('"')))

    var = pyparsing.Word(pyparsing.alphas + pyparsing.nums + '_').setResultsName("var")
    false = pyparsing.CaselessLiteral("False")
    true = pyparsing.CaselessLiteral("True")
    vbool = (false | true)
    vint = pyparsing.Word(pyparsing.nums)
    vreal = pyparsing.Combine(pyparsing.Word(pyparsing.nums)
                              + '.' + pyparsing.Optional(pyparsing.Word(pyparsing.nums)))
    vstring = q + pyparsing.Word(pyparsing.alphas + '_' + '-' + ' ') + q

    lsq = pyparsing.Suppress(pyparsing.Optional('['))
    rsq = pyparsing.Suppress(pyparsing.Optional(']'))
    lb = pyparsing.Suppress(pyparsing.Optional('('))
    rb = pyparsing.Suppress(pyparsing.Optional(')'))
    eq = (pyparsing.Literal('==')
          | pyparsing.Literal("=")).setParseAction(pyparsing.replaceWith("==")) \
        .setResultsName("eq")
    gt = pyparsing.Literal('>')
    lt = pyparsing.Literal('<')
    ge = pyparsing.Literal(">=")
    le = pyparsing.Literal("<=")
    op = (ge | le | eq | gt | lt).setResultsName("op")  # NB order important.

    pl = pyparsing.Literal('+')
    mi = pyparsing.Literal('-')
    mu = pyparsing.Literal('*')
    di = pyparsing.Literal('/')

    genval = (vreal | vint | vbool | vstring).setResultsName("genval")
    numval = (vreal | vint).setResultsName("numval")
    numboolval = (vreal | vint | vbool).setResultsName("numboolval")

    neg = (pyparsing.Literal("not")
           | pyparsing.Literal('!')).setParseAction(pyparsing.replaceWith("not")) \
        .setResultsName("neg")

    class VarDefGrammar(object):  # pylint: disable=too-few-public-methods
        """Grammar for variable definitions."""

        def __init__(self):
            """Initialise grammar for variables with name and type."""
            self.vtype = (Parser.sbool | Parser.sint | Parser.sreal | Parser.sstring) \
                .setResultsName("vtype")
            self.vardef = Parser.svar + Parser.var \
                + pyparsing.ZeroOrMore(Parser.comma + Parser.var) \
                + Parser.colon + self.vtype

        def parse(self, exp):
            """Parse variable definitions."""
            results = self.vardef.parseString(exp)
            return results[0:-1], results.vtype

    class VarAssGrammar(object):
        """Grammar for variable assignments (test only)."""

        def __init__(self):
            """Initialise grammar."""
            self.ass = Parser.var + '=' + Parser.genval

        def parse(self, exp):
            """Parse variable assignments."""
            results = self.ass.parseString(exp)
            return results.var, results.genval

        @staticmethod
        def assign(vdict, var, val):
            """Create assignment in dictionary of variables."""
            try:
                t = vdict[var]["type"]
            except KeyError:
                raise Exception("Variable", var, "not in dict")
            # Check type: perhaps the parsing should have already done this?
            try:
                if t == "INT":
                    vdict[var]["value"] = int(val)
                elif t == "REAL":
                    vdict[var]["value"] = float(val)
                elif t == "BOOL":
                    vdict[var]["value"] = (val.lower() == "true")
                elif t == "STRING":
                    vdict[var]["value"] = val
                else:
                    raise Exception("Variable", var, "has undefined type", t, "in dict")
            except KeyError:
                raise Exception("Assignment", val, "to var", var, "failed type check")
            return vdict

    class AGuardGrammar(object):
        """Grammar for data guard statements on activities."""

        def __init__(self):
            """Initialise grammar."""
            self.guard = Parser.lsq + pyparsing.Optional(Parser.neg) + Parser.var \
                + pyparsing.Optional(Parser.op + Parser.numboolval) + Parser.rsq

        def parse(self, exp, vdict, check_none=False):
            """Parse transition guards."""
            results = self.guard.parseString(exp)

            var, op, val, neg = results.var, results.op, results.numboolval, results.neg
            try:
                t = vdict[var]["type"]
            except KeyError:
                raise Exception("Variable", var, "not in dict")
            if check_none and vdict[var]["value"] is None:
                raise Exception("Variable", var, "unset (None) in dict")
            newval = val
            try:
                if t in ["INT", "REAL"]:
                    newval = float(val)
                elif t == "BOOL":
                    newval = (val.lower() == "true")
                elif t == "STRING":
                    raise Exception("Variable", var, "has disallowed type", t, "in dict")
                else:
                    raise Exception("Variable", var, "has undefined type", t, "in dict")
            except KeyError:
                raise Exception("Assignment", newval, "to var",
                                var, "type", t, "failed type check")

            return var, op, newval, neg

        def guard_testvals(self, exps, vdict):
            """Parse guard and return a list of (var name, (pair of values)) tuples for testing."""
            returnlist, checkvals = [], None
            if exps not in [None, ""]:
                guards = exps.split(',')
                for exp in guards:
                    results = self.parse(exp, vdict, check_none=False)
                    if results is not None:
                        var, op, val, neg = results
                        neg = None if neg in [None, ''] else neg
                        var = None if var in [None, ''] else var
                        op = None if op in [None, ''] else op
                        val = None if val in [None, ''] else val
                        # Bodge ? needs fixing.
                        if op is val is None \
                                or op is None and val is False \
                                or op == "==" and val in ["True", "False"]:
                            # Boolean 'x' or "not x": check true & false cases
                            if vdict[var]["type"] == "BOOL":
                                checkvals = (True, False)
                            else:
                                raise Exception("Guard boolean test for non-boolean var", var)

                        elif neg is None \
                                and var is not None and op is not None and val is not None:
                            # Bad: enforcing parsing after the event.
                            # >, >=, <, <=: check (close) boundary cases
                            if vdict[var]["type"] in ["INT", "REAL"]:
                                val = float(val)
                                if op in [">=", '<']:
                                    checkvals = (val - 1, val)
                                elif op in ['>', "<=", '=', "=="]:
                                    checkvals = (val, val + 1)
                                else:
                                    raise Exception("Prob. with guard cond. for token creation.")
                        else:
                            raise Exception("Problem parsing guard for token creation.")
                        returnlist.append((var, checkvals))
            return returnlist

    class AModGrammar(object):
        """Grammar for data modification in activities."""

        def __init__(self):
            """Initialise grammar."""
            self.modop = (Parser.pl | Parser.mi | Parser.mu | Parser.di) \
                .setResultsName("modop")
            self.modval = (Parser.vstring | Parser.numval).setResultsName("modval")
            self.mod = \
                pyparsing.Optional(Parser.var.setResultsName("assvar") + '=') \
                + pyparsing.Optional(Parser.neg.setResultsName("modneg")) \
                + pyparsing.Optional(Parser.var.setResultsName("modvar")
                                     + self.modop.setResultsName("modop")) \
                + self.modval.setResultsName("modval")

        @staticmethod
        def parselist(exp):
            """ Non-pyparsing removal of surrounding brackets, and split into CSV."""
            if exp and exp[0] == '(':  # changed from len(x) > 0
                exp = exp[1:]
            if exp and exp[-1] == ')':
                exp = exp[:-1]
            results = exp.split(',')
            return results

        def parse(self, vdict, mod):
            """Parse a single variable modification.

            As split by parse().
            NB: Checks variable in dictionary but no type checking.
            """
            results = self.mod.parseString(mod)

            modneg = None if results.modneg in [None, ""] else results.modneg
            modvar = None if results.modvar in [None, ""] else results.modvar
            assvar = None if results.assvar in [None, ""] else results.assvar
            modop = None if results.modop in [None, ""] else results.modop
            modval = None if results.modval in [None, ""] else results.modval

            # XXX These are bodges; grammar needs some work.
            if modvar is None and assvar is not None:
                modvar = assvar
            if modneg == "not" and modvar is None and modval is not None:
                modvar = modval
                modvarl = None
            try:
                _ = vdict[modvar]["type"]
            except KeyError:
                print(modneg, modvar, assvar, modop, modval)  # XXX
                raise Exception("Variable", modvar, "not in dict")

            return modneg, modvar, assvar, modop, modval

        def updval(self, vdict, modlist):
            """Update a variable.

            (With the results of parsing each in a list of CSV modification stmts.)
            """
            for exp in modlist:
                results = self.mod.parseString(exp)

                modneg = None if results.modneg in [None, ""] else results.modneg
                modvar = None if results.modvar in [None, ""] else results.modvar
                assvar = None if results.assvar in [None, ""] else results.assvar
                modop = None if results.modop in [None, ""] else results.modop
                modval = None if results.modval in [None, ""] else results.modval

                # Bodge: if single var, it should be modvar (e.g. 'x' : unchanged).
                # Unsure how to create the grammar to set this as default.
                if assvar is modneg is modvar is modop is None and modval is not None:
                    modvar, modval = modval, None

                if assvar is None:
                    assvar = modvar

                # Ignore statements of the type "x = x" or sole 'x' (CPN/Tools).
                if not ((assvar == modvar) and (modneg is modop is modval is None)):
                    newval = None
                    if modval is not None:
                        try:
                            t = vdict[assvar]["type"]
                        except KeyError:
                            # Silently ignore assuming this means that the variable is not
                            # part of the tokenset, i.e. not on any guard.
                            return vdict
                            # raise Exception("Variable", assvar, "not in dict")
                        if vdict[assvar]["value"] is None:
                            raise Exception("Variable", assvar, "unset (None) in dict")
                        try:
                            if t == "INT":
                                newval = int(modval)
                            elif t == "REAL":
                                newval = float(modval)
                            elif t == "BOOL":
                                newval = (modval.lower() == "true")
                            elif t == "STRING":
                                newval = '"' + modval + '"'
                            else:
                                raise Exception("Variable", assvar, "has undefined type", t,
                                                "in dict")
                        except KeyError:
                            raise Exception("Assignment", modval, "to var",
                                            assvar, "type", t, "failed type check")

                    upd_str = "vdict['" + assvar + "']['value'] = "
                    if modneg not in [None, ""]:
                        upd_str += "not "
                    if modvar not in [None, ""]:
                        upd_str += "vdict['" + modvar + "']['value'] "
                    if modop not in [None, ""]:
                        upd_str += modop + ' '
                    if newval not in [None, ""]:
                        upd_str += str(newval)
                    exec(upd_str)  # pylint: disable=exec-used

            return vdict

    def __init__(self):
        """ Define grammar. """
        self.vardef = self.VarDefGrammar()
        self.varass = self.VarAssGrammar()
        self.aguard = self.AGuardGrammar()
        self.adatamod = self.AModGrammar()


def main():
    """Capture and alert command-line calls."""
    print("BPMNVParse.py: load as module only.")

if __name__ == "__main__":
    main()
