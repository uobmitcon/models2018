#!/usr/bin/env python

# 2018-05-04 Z3 constraint model for a basic parallel construct.
#            a->{b|c}.  Solution paths list nodes only (not arcs (flows)).
#            Markings on start node (p0) then flows.

#            This version records two separate paths: fullpth records all nodes
#            visited; path records activities only.  jj stores the difference
#            between the indices to both.

from z3 import *

if len(sys.argv) > 1:  # z3 imports sys
  lim = sys.argv[1]
else:
  lim = 10  # upper bound search (but it has little time effect if large)

### Syntax
# Metamodel
node = DeclareSort('node')  # Eventually cover Activity, splits, joins
### flow = Function('flow', node, node, BoolSort())

# Declarations for the axioms
# x, y, T = Consts('x y T', node)  # T: terminator
T = Const('T', node)  # T: terminator

# Model
p0, l0, a, b, l0_, pe = Consts('p0 l0 a b l0_ pe', node)

# Syntax is implicit in the semantics - but Distinct is necessary
syntax = [
###   ForAll([x, y],
###     Not(
###       Or(And(x == p0, y == l0),
###          And(x == l0, y == a),
###          And(x == l0, y == b),
###          And(x == a, y == l0_),
###          And(x == b, y == l0_),
###          And(x == l0_, y == pe),
###       )
###     ) == Not(flow(x, y))
###   ),
###   flow(p0, l0),
###   flow(l0, a),
###   flow(l0, b),
###   flow(a, l0_),
###   flow(b, l0_),
###   flow(l0_, pe),
  Distinct(p0, l0, a, b, l0_, pe, T)
]

### Semantics
semantics = []
ii, limit = Ints('ii limit')  # main index, limit

fullpth = Array('fullpth', IntSort(), node)  # list of nodes
path = Array('path',       IntSort(), node)  # list of activities

# jj is a difference between the current indices to fullpth and path, i.e. is
# incremented whenever a non-activity node is visited which is not recorded in
# the path array.
jj = Array('jj',           IntSort(), IntSort())
p0_M = Array('p0_M',       IntSort(), BoolSort())

p0_l0_M = Array('p0_l0_M',   IntSort(), BoolSort())  # Flow markings
l0_a_M = Array('l0_a_M',     IntSort(), BoolSort())
l0_b_M = Array('l0_b_M',     IntSort(), BoolSort())
a_l0__M = Array('a_l0__M',   IntSort(), BoolSort())
b_l0__M = Array('b_l0__M',   IntSort(), BoolSort())
l0__pe_M = Array('l0__pe_M', IntSort(), BoolSort())

# Data
med = Array('med', IntSort(), BoolSort())  # variable for guard
failonentry = Array('failonentry', IntSort(), BoolSort())

# Initial marking
mark0 = And(p0_M[0],
            Not(p0_l0_M[0]), Not(l0_a_M[0]), Not(l0_b_M[0]), Not(a_l0__M[0]), 
            Not(b_l0__M[0]), Not(l0__pe_M[0]),
            jj[0] == 0)  # Start in p0
semantics.append(mark0)
data0 = And(Not(failonentry[0]), med[0])
semantics.append(data0)

# Subsequent markings depend on initial and flow
mark = [
  limit == lim,
  ForAll(ii,
    # Large speed up changing from If to Implies here (in conjunction w/ fired).
    # (Now have junk in fullpth outside [t0, limit], but see fired.)
    # If(And(ii >= 0, ii <= limit),
#   Implies(And(ii >= 0, ii <= limit),  # No longer required
       And(p0_M[ii] == (ii == 0),  # No flows into p0
          Implies(p0_l0_M[ii],
            Or(And(p0_l0_M[ii-1], fullpth[ii-1] != l0),
               And(p0_M[ii-1], Not(p0_M[ii]), fullpth[ii-1] == p0)),
          ),
          Implies(l0_a_M[ii],
            Or(And(l0_a_M[ii-1], fullpth[ii-1] != a),
               And(p0_l0_M[ii-1], Not(p0_l0_M[ii]), fullpth[ii-1] == l0)),  # Not enforce both
          ),
          Implies(l0_b_M[ii],
            Or(And(l0_b_M[ii-1], fullpth[ii-1] != b),
               And(p0_l0_M[ii-1], Not(p0_l0_M[ii]), fullpth[ii-1] == l0)),  # ditto
          ),
          Implies(a_l0__M[ii],
            Or(And(a_l0__M[ii-1], fullpth[ii-1] != l0_),
               And(l0_a_M[ii-1], Not(l0_a_M[ii]), fullpth[ii-1] == a)),
          ),
          Implies(b_l0__M[ii],
            Or(And(b_l0__M[ii-1], fullpth[ii-1] != l0_),
               And(l0_b_M[ii-1], Not(l0_b_M[ii]), fullpth[ii-1] == b)),
          ),
          Implies(l0__pe_M[ii],
            Or(And(l0__pe_M[ii-1], fullpth[ii-1] != pe),
               And(a_l0__M[ii-1], Not(a_l0__M[ii]),
                   b_l0__M[ii-1], Not(b_l0__M[ii]), fullpth[ii-1] == l0_)),
          ),
       ),
#      fullpth[ii] == T,  # Else
#   )  # End If
  )
]
semantics += mark

# List of fired nodes - node fired implies it was marked (no enablings via
# predecessor tokens here: Don't need to imply false otherwise, as other
# markings could have been posible.
fired = [
  ForAll(ii,
    Implies(And(ii >= 0, ii <= limit),
       And(Or(fullpth[ii] == p0, fullpth[ii] == l0, fullpth[ii] == a, fullpth[ii] == b,
              fullpth[ii] == l0_, fullpth[ii] == pe, fullpth[ii] == T),
           (fullpth[ii] == p0) ==
               # Data: check limit before next marking to allow short fullpth
               And(p0_M[ii], Not(p0_M[ii+1]),
                   Or(p0_l0_M[ii+1], ii >= limit-1),
                   med[ii+1] == med[ii],  # No data modification
                   failonentry[ii+1] == failonentry[ii],
                   jj[ii+1] == jj[ii]+1   # ignore in path
               ),
           (fullpth[ii] == l0) ==
               And(p0_l0_M[ii], Not(p0_l0_M[ii+1]),
                   Or(And(l0_a_M[ii+1], l0_b_M[ii+1]), ii >= limit-1),
                   med[ii+1] == med[ii],  # No data modification
                   failonentry[ii+1] == failonentry[ii],
                   jj[ii+1] == jj[ii]+1   # ignore in path
               ),
           (fullpth[ii] == a) ==
               And(l0_a_M[ii], Not(l0_a_M[ii+1]),
                   Or(a_l0__M[ii+1], ii >= limit-1),
                   If(med[ii],
                      failonentry[ii+1],
                      failonentry[ii+1] == failonentry[ii]),  # Guard
                   med[ii+1] == med[ii],
                   jj[ii+1] == jj[ii],  # include in path
                   path[ii-jj[ii]] == fullpth[ii]
               ),
           (fullpth[ii] == b) ==
               And(l0_b_M[ii], Not(l0_b_M[ii+1]),
                   Or(b_l0__M[ii+1], ii >= limit-1),
                   med[ii+1] == True,  # data modification (won't effect)
                   failonentry[ii+1] == failonentry[ii],
                   jj[ii+1] == jj[ii],  # include in path
                   path[ii-jj[ii]] == fullpth[ii]
               ),
           (fullpth[ii] == l0_) ==
               And(a_l0__M[ii], Not(a_l0__M[ii+1]),
                   b_l0__M[ii], Not(b_l0__M[ii+1]),
                   Or(l0__pe_M[ii+1], ii >= limit-1),
                   med[ii+1] == med[ii],
                   failonentry[ii+1] == failonentry[ii],
                   jj[ii+1] == jj[ii]+1   # ignore in path
               ),
           (fullpth[ii] == pe) ==
               And(l0__pe_M[ii], Not(l0__pe_M[ii+1]),
                   med[ii+1] == med[ii],
                   failonentry[ii+1] == failonentry[ii],
                   jj[ii+1] == jj[ii]+1   # ignore in path
               ),
           (fullpth[ii] == T) ==
               And(path[ii-jj[ii]] == T,
                   Or(failonentry[ii],  # we got here by failure
                      And(Not(Or(p0_M[ii],
                                 p0_l0_M[ii], l0_a_M[ii], l0_b_M[ii],
                                 a_l0__M[ii], b_l0__M[ii], l0__pe_M[ii])),
                          Or(fullpth[ii-1] == T,  # End cases
                             fullpth[ii-1] == pe,
                            ),
                          failonentry[ii]  # with or without failure
                      ),
                      And(Not(Or(p0_M[ii],
                                 p0_l0_M[ii], l0_a_M[ii], l0_b_M[ii],
                                 a_l0__M[ii], b_l0__M[ii], l0__pe_M[ii])),
                          Or(fullpth[ii-1] == T,  # End cases
                             fullpth[ii-1] == pe,
                            ),
                          Not(failonentry[ii])
                      ),
                   ),
                   fullpth[ii+1] == T
               ),
#      And(fullpth[ii] == T, Not(p0_M[ii]), Not(p0_l0_M[ii]), Not(l0_a_M[ii]),
#          Not(l0_b_M[ii]), Not(a_l0__M[ii]), Not(b_l0__M[ii]),
#          Not(l0__pe_M[ii]))
     )
  )
]
semantics += fired

# s = Solver()  # QF_UFLIA restriction is faster
s = SolverFor("QF_UFLIA")  # LRA --> LIA for jj index
s.add(syntax)
s.add(semantics)
r = s.check()
# for aa in s.assertions(): print(aa)
# exit()

print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
  # s.add([fullpth[2] != b, fullpth[3] != a])
  s.add([Not(And(fullpth[2] == b, fullpth[3] == a))])

  # Test constraint to return no duplicate fail mode
  s.add([Not(Exists(iii, And(iii > 0, iii <= 10,  # Test no duplicates
                             failonentry[iii],
                             path[iii-1] == a,
                             med[iii-1] == med[0]
         )))])

  r = s.check()
  print("RESULT", r)
  if r == sat:
    for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
    s.add([Not(And(fullpth[2] == a, fullpth[3] == b))])
    r = s.check()
    print("RESULT", r)
    if r == sat:
      for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
