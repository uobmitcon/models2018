#!/usr/bin/env python

# 2018-05-04 Z3 constraint model for a basic XOR (Exclusive OR) construct.
#            a->{bXc}.  Solution paths list nodes only (not arcs (flows)).
#            Markings on start node (p0) then flows.

from z3 import *

if len(sys.argv) > 1:  # z3 imports sys
  lim = sys.argv[1]
else:
  lim = 10  # upper bound search (but it has little time effect if large)

### Syntax
# Metamodel
node = DeclareSort('node')  # Eventually cover Activity, splits, joins
### flow = Function('flow', node, node, BoolSort())

# Declarations for the axioms
# x, y, T = Consts('x y T', node)  # T: terminator
T = Const('T', node)  # T: terminator

# Model
p0, x0, a, b, x0_, pe = Consts('p0 x0 a b x0_ pe', node)

# Flows
# Syntax is implicit in the semantics - but Distinct is necessary
### syntax = [
###   ForAll([x, y],
###     Not(
###       Or(And(x == p0, y == x0),
###          And(x == x0, y == a),
###          And(x == x0, y == b),
###          And(x == a, y == x0_),
###          And(x == b, y == x0_),
###          And(x == x0_, y == pe),
###         )
###     ) == Not(flow(x, y))
###   ),
###   flow(p0, x0),
###   flow(x0, a),
###   flow(x0, b),
###   flow(a, x0_),
###   flow(b, x0_),
###   flow(x0_, pe),
Distinct(p0, x0, a, b, x0_, pe, T)
### ]

### Semantics
semantics = []
ii, limit = Ints('ii limit')

path = Array('path',   IntSort(), node)  # list of nodes
p0_M = Array('p0_M',   IntSort(), BoolSort())

p0_x0_M = Array('p0_x0_M',   IntSort(), BoolSort())  # Flow markings
x0_a_M = Array('x0_a_M',     IntSort(), BoolSort())
x0_b_M = Array('x0_b_M',     IntSort(), BoolSort())
a_x0__M = Array('a_x0__M',   IntSort(), BoolSort())
b_x0__M = Array('b_x0__M',   IntSort(), BoolSort())
x0__pe_M = Array('x0__pe_M', IntSort(), BoolSort())

# Data
med = Array('med', IntSort(), BoolSort())  # variable for guard
failonentry = Array('failonentry', IntSort(), BoolSort())

# Initial marking
mark0 = And(p0_M[0],
            Not(p0_x0_M[0]), Not(x0_a_M[0]), Not(x0_b_M[0]), Not(a_x0__M[0]),
            Not(b_x0__M[0]), Not(x0__pe_M[0]))  # Start in p0
semantics.append(mark0)
data0 = And(Not(failonentry[0]), med[0])
semantics.append(data0)

# Subsequent markings depend on initial and flow
mark = [
  limit == lim,
  ForAll(ii,
    # Large speed up changing from If to Implies here (in conjunction w/ fired).
    # (Now have junk in path outside [t0, limit], but see fired.)
    # If(And(ii >= 0, ii <= limit),
#   Implies(And(ii >= 0, ii <= limit),  # No longer required
       And(p0_M[ii] == (ii == 0),  # No flows into p0
           Implies(p0_x0_M[ii],
             Or(And(p0_x0_M[ii-1], path[ii-1] != x0),
                And(p0_M[ii-1], Not(p0_M[ii]))),
           ),
           Implies(x0_a_M[ii],
             Or(And(x0_a_M[ii-1], path[ii-1] != a),
                And(p0_x0_M[ii-1], Not(p0_x0_M[ii]),
                    Not(x0_b_M[ii]))),  # Other(s) not enabled
           ),
           Implies(x0_b_M[ii],
             Or(And(x0_b_M[ii-1], path[ii-1] != b),
                And(p0_x0_M[ii-1], Not(p0_x0_M[ii]),
                    Not(x0_a_M[ii]))),  # Other(s) not enabled
           ),
           Implies(a_x0__M[ii],
             Or(And(a_x0__M[ii-1], path[ii-1] != x0_),
                And(x0_a_M[ii-1], Not(x0_a_M[ii]))),
           ),
           Implies(b_x0__M[ii],
             Or(And(b_x0__M[ii-1], path[ii-1] != x0_),
                And(x0_b_M[ii-1], Not(x0_b_M[ii]))),
           ),
           Implies(x0__pe_M[ii],
             Or(And(x0__pe_M[ii-1], path[ii-1] != pe),
                And(a_x0__M[ii-1], Not(a_x0__M[ii])),
                And(b_x0__M[ii-1], Not(b_x0__M[ii]))),
           ),
       ),
       # path[ii] == T,
#   )
  )
]
semantics += mark

# List of fired nodes - node fired implies it was marked (no enablings via
# predecessor tokens here: Don't need to imply false otherwise, as other
# markings could have been posible.
fired = [
  ForAll(ii,
    # If(And(ii >= 0, ii < limit),
    Implies(And(ii >= 0, ii < limit),  # Req. due to not checking cont. final T?
       And(Or(path[ii] == p0, path[ii] == x0, path[ii] == a, path[ii] == b,
              path[ii] == x0_, path[ii] == pe, path[ii] == T),
           (path[ii] == p0) ==
               # Data: chk limit before next marking to allow short path
               And(p0_M[ii], Not(p0_M[ii+1]),
                   Or(p0_x0_M[ii+1], ii >= limit-1),
                   med[ii+1] == med[ii],  # No data modification
                   failonentry[ii+1] == failonentry[ii]),  # Copy data
           (path[ii] == x0) ==
               And(p0_x0_M[ii], Not(p0_x0_M[ii+1]),
                   Or(x0_a_M[ii+1], x0_b_M[ii+1], ii >= limit-1),
                   med[ii+1] == med[ii],  # No data modification
                   failonentry[ii+1] == failonentry[ii]),
           (path[ii] == a) ==
               And(x0_a_M[ii], Not(x0_a_M[ii+1]),
                   Or(a_x0__M[ii+1], ii >= limit-1),
                   If(med[ii],
                      failonentry[ii+1],
                      failonentry[ii+1] == failonentry[ii]),  # Guard
                   med[ii+1] == med[ii]),
           (path[ii] == b) ==
               And(x0_b_M[ii], Not(x0_b_M[ii+1]),
                   Or(b_x0__M[ii+1], ii >= limit-1),
                   med[ii+1] == True,  # data modification (won't effect)
                   failonentry[ii+1] == failonentry[ii]),
           (path[ii] == x0_) ==
               And(Or(And(a_x0__M[ii], Not(a_x0__M[ii+1])),
                      And(b_x0__M[ii], Not(b_x0__M[ii+1]))),
                   Or(x0__pe_M[ii+1], ii >= limit-1),
                   med[ii+1] == med[ii],
                   failonentry[ii+1] == failonentry[ii]),
           (path[ii] == pe) ==
               And(x0__pe_M[ii], Not(x0__pe_M[ii+1]),
                   med[ii+1] == med[ii]),
           (path[ii] == T) ==
               And(Or(failonentry[ii],  # we got here by failure
                      And(Not(Or(p0_M[ii],
                                 p0_x0_M[ii], x0_a_M[ii], x0_b_M[ii],
                                 a_x0__M[ii], b_x0__M[ii], x0__pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          failonentry[ii]  # with or without failure
                      ),
                      And(Not(Or(p0_M[ii],
                                 p0_x0_M[ii], x0_a_M[ii], x0_b_M[ii],
                                 a_x0__M[ii], b_x0__M[ii], x0__pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          Not(failonentry[ii])
                      ),
                   ),
                   path[ii+1] == T
               ),
         ),  # End And
       # And(path[ii] == T, Not(p0_M[ii]), Not(p0_x0_M[ii]), Not(x0_a_M[ii]),
       #     Not(x0_b_M[ii]), Not(a_x0__M[ii]), Not(b_x0__M[ii]),
       #     Not(x0__pe_M[ii]))
     )  # End Implies
  )  # End ForAll
]
semantics += fired

# s = Solver()
s = SolverFor("QF_UFLRA")  # Fater (but unclear if it should be valid)
### s.add(syntax)
s.add(semantics)
r = s.check()
# for aa in s.assertions(): print(aa)
# print(s.sexpr())
# exit()

print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
  s.add(path[2] != b)
  r = s.check()
  print("RESULT", r)
  if r == sat:
    for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
  s.add(path[2] != a)
  r = s.check()
  print("RESULT", r)
  if r == sat:
    for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
