#!/usr/bin/env python

# 2018-05-04 Z3 constraint model for a basic IOR (inclusive OR) construct.
#            a->{b|c}.  Solution paths list nodes only (not arcs (flows)).
#            Markings on start node (p0) then flows.

from z3 import *

if len(sys.argv) > 1:  # z3 imports sys
  lim = sys.argv[1]
else:
  lim = 10  # upper bound search (but it has little time effect if large)

### Syntax
# Metamodel
node = DeclareSort('node')  # Eventually cover Activity, splits, joins
### flow = Function('flow', node, node, BoolSort())

# Declarations for the axioms
# x, y, T = Consts('x y T', node)  # T: terminator
T = Const('T', node)  # T: terminator

# Model
p0, i0, a, b, i0_, pe = Consts('p0 i0 a b i0_ pe', node)

# Syntax is implicit in the semantics - but Distinct is necessary
syntax = [
###   ForAll([x, y],
###     Not(
###       Or(And(x == p0, y == i0),
###          And(x == i0, y == a),
###          And(x == i0, y == b),
###          And(x == a, y == i0_),
###          And(x == b, y == i0_),
###          And(x == i0_, y == pe),
###       )
###     ) == Not(flow(x, y))
###   ),
###   flow(p0, i0),
###   flow(i0, a),
###   flow(i0, b),
###   flow(a, i0_),
###   flow(b, i0_),
###   flow(i0_, pe),
  Distinct(p0, i0, a, b, i0_, pe, T)
]

### Semantics
semantics = []
ii, limit = Ints('ii limit')
ior = Int('ior')  # number of paths selected on entry to IOR

path = Array('path', IntSort(), node)  # list of nodes
p0_M = Array('p0_M', IntSort(), BoolSort())

p0_i0_M = Array('p0_i0_M', IntSort(), BoolSort())  # Flow markings
i0_a_M = Array('i0_a_M', IntSort(), BoolSort())
i0_b_M = Array('i0_b_M', IntSort(), BoolSort())
a_i0__M = Array('a_i0__M', IntSort(), BoolSort())
b_i0__M = Array('b_i0__M', IntSort(), BoolSort())
i0__pe_M = Array('i0__pe_M', IntSort(), BoolSort())

# Data (none)
failonentry = Array('failonentry', IntSort(), BoolSort())

# Initial marking
mark0 = And(p0_M[0],
            Not(p0_i0_M[0]), Not(i0_a_M[0]), Not(i0_b_M[0]), Not(a_i0__M[0]), 
            Not(b_i0__M[0]), Not(i0__pe_M[0]))  # Start in p0
semantics.append(mark0)
data0 = And(Not(failonentry[0]))
semantics.append(data0)

# Subsequent markings depend on initial and flow
mark = [
  limit == lim,
  ForAll(ii,
    # Large speed up changing from If to Implies here (in conjunction w/ fired).
    # (Now have junk in path outside [t0, limit], but see fired.)
    # If(And(ii >= 0, ii <= limit),  # unsat if > final marking.
#   Implies(And(ii >= 0, ii <= limit),  # No longer required
       And(p0_M[ii] == (ii == 0),  # No flows into p0
           Implies(p0_i0_M[ii],
             Or(And(p0_i0_M[ii-1], path[ii-1] != i0),
                And(p0_M[ii-1], Not(p0_M[ii]), path[ii-1] == p0)),
           ),
           Implies(i0_a_M[ii],
             Or(And(i0_a_M[ii-1], path[ii-1] != a),
                And(p0_i0_M[ii-1], Not(p0_i0_M[ii]),
                    path[ii-1] == i0,  # Record how many enabled
                    i0_b_M[ii] == (ior == 2),  # 2 if other enabled
                    Not(i0_b_M[ii]) == (ior == 1),  # 1 if only this
                   )),
           ),
           Implies(i0_b_M[ii],
             Or(And(i0_b_M[ii-1], path[ii-1] != b),
                And(p0_i0_M[ii-1], Not(p0_i0_M[ii]),
                    path[ii-1] == i0,  # Record how many enabled
                    i0_a_M[ii] == (ior == 2),
                    Not(i0_a_M[ii]) == (ior == 1),
                   )),
           ),
           Implies(a_i0__M[ii],
             Or(And(a_i0__M[ii-1], path[ii-1] != i0_),
                And(i0_a_M[ii-1], Not(i0_a_M[ii]), path[ii-1] == a)),
           ),
           Implies(b_i0__M[ii],
             Or(And(b_i0__M[ii-1], path[ii-1] != i0_),
                And(i0_b_M[ii-1], Not(i0_b_M[ii]), path[ii-1] == b)),
           ),
           Implies(i0__pe_M[ii],
             Or(And(i0__pe_M[ii-1], path[ii-1] != pe),
                And(If(ior == 2,
                       And(a_i0__M[ii-1], Not(a_i0__M[ii]),
                           b_i0__M[ii-1], Not(b_i0__M[ii])),
                       # ior == 1
                       Or(And(a_i0__M[ii-1], Not(a_i0__M[ii])),
                          And(b_i0__M[ii-1], Not(b_i0__M[ii]))))),
                    path[ii-1] == i0_),
           ),
       ),
#      path[ii] == T,  # Else
#   )  # End If
  )
]
semantics += mark

# List of fired nodes - node fired implies it was marked (no enablings via
# predecessor tokens here: Don't need to imply false otherwise, as other
# markings could have been posible.
fired = [
  ForAll(ii,
    # If(And(ii >= 0, ii <= limit),
    Implies(And(ii >= 0, ii <= limit),
       And(Or(path[ii] == p0, path[ii] == i0, path[ii] == a, path[ii] == b,
              path[ii] == i0_, path[ii] == pe, path[ii] == T),
           (path[ii] == p0) ==
               And(p0_M[ii], Not(p0_M[ii+1]),
                   Or(p0_i0_M[ii+1], ii >= limit-1),
                   failonentry[ii+1] == failonentry[ii]
               ),
           (path[ii] == i0) ==
               And(p0_i0_M[ii], Not(p0_i0_M[ii+1]),
                   If(ior == 2,
                      And(i0_a_M[ii+1], i0_b_M[ii+1]),
                      Or(i0_a_M[ii+1], i0_b_M[ii+1])),
                   failonentry[ii+1] == failonentry[ii]
               ),
           (path[ii] == a) ==
               And(i0_a_M[ii], Not(i0_a_M[ii+1]),
                   Or(a_i0__M[ii+1], ii >= limit-1),
                   failonentry[ii+1] == failonentry[ii]
               ),
           (path[ii] == b) ==
               And(i0_b_M[ii], Not(i0_b_M[ii+1]),
                   Or(b_i0__M[ii+1], ii >= limit-1),
                   failonentry[ii+1] == failonentry[ii]
               ),
           (path[ii] == i0_) ==
               And(If(ior == 2,
                      And(a_i0__M[ii], Not(a_i0__M[ii+1]),
                          b_i0__M[ii], Not(b_i0__M[ii+1])),
                      Or(And(a_i0__M[ii], Not(a_i0__M[ii+1])),
                         And(b_i0__M[ii], Not(b_i0__M[ii+1])))),
                   Or(i0__pe_M[ii+1], ii >= limit-1),
                   failonentry[ii+1] == failonentry[ii]
               ),
           (path[ii] == pe) ==
               And(i0__pe_M[ii], Not(i0__pe_M[ii+1])),
           (path[ii] == T) ==
               And(Or(failonentry[ii],  # we got here by failure
                      And(Not(Or(p0_M[ii],
                                 p0_i0_M[ii], i0_a_M[ii], i0_b_M[ii],
                                 a_i0__M[ii], b_i0__M[ii], i0__pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          failonentry[ii]  # with or without failure
                      ),
                      And(Not(Or(p0_M[ii],
                                 p0_i0_M[ii], i0_a_M[ii], i0_b_M[ii],
                                 a_i0__M[ii], b_i0__M[ii], i0__pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          Not(failonentry[ii])
                      ),
                   ),
                   path[ii+1] == T
               ),
           ),
#      And(path[ii] == T, Not(p0_M[ii]), Not(p0_i0_M[ii]), Not(i0_a_M[ii]),
#          Not(i0_b_M[ii]), Not(a_i0__M[ii]), Not(b_i0__M[ii]),
#          Not(i0__pe_M[ii]))
     )
  )
]
semantics += fired

# s = Solver()
s = SolverFor("QF_UFLRA")  # Faster
s.add(syntax)
s.add(semantics)
s.push()
s.add(ior == 1)  # Get the 1-path results
r = s.check()
# for aa in s.assertions(): print(aa)
# exit()

print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))

s.add(path[2] != a)
r = s.check()
print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))

s.add(path[2] != b)
r = s.check()
print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))

s.pop()
s.add(ior == 2)  # Get the 2-path results
r = s.check()
print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))

s.add([path[2] != b, path[3] != a])
r = s.check()
print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))

s.add([path[2] != a, path[3] != b])
r = s.check()
print("RESULT", r)
if r == sat:
  for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
