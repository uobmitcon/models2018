#!/usr/bin/env python
# pylint: disable=too-many-lines
""" Phil Weber 2017-08-02 Meta-model for BPMN+V in Z3 (Python).

To do, including:
o  Grammar parsing -- improve robustness interpreting guards and data modifications.
o  IOR - match conv and div (enforce naming convention, or parse model as for CPN)
o  IOR - enable more than 3 paths
"""

import itertools
import sys
import z3
import BPMNVparse

NULLNODE = 'T'
DOTFONT = "NumbusSans"
Z3_TIMEOUT = 30000  # Unresolved issues: seems that a timeout prevents subsequent calls to Z3 from working correctly


# pylint: disable=too-many-instance-attributes
class Z3BPMNV(object):
    """Z3 BPMNV Meta-model."""

    def __init__(self):
        # BPMN+V META-MODEL (removed all syntax, implicit in the semantics)
        self.node = z3.DeclareSort('node')
        # self.flow = z3.Function('flow', self.node, self.node, z3.BoolSort())

        # Create a solver with the meta-model syntax
        self.solver = z3.SolverFor("QF_UFLIA")
        if Z3_TIMEOUT:
            print("WARNING: Setting timeout")
            self.solver.set("timeout", Z3_TIMEOUT)

        return

    def get_path_non_qfuflra(self, t_limit, nodes):
        """Get a solution path (series of nodes and arcs).

        The original version using Solver.  SolverFor("QF_UFLRA") requires different code.
        See get_path().

        :param t_limit: max length of path specified
        :param nodes: string forms of node names.
        Get path for current model solution.
        """
        model = self.solver.model()

        # Map internal node names to user names
        nodes = {model[dd]:dd for dd in model.decls() if dd.arity() == 0
                 and str(dd) != "path" and str(dd) in nodes and not str(dd).endswith("_M")}

        # Extract the internal subfunction name for the path.
        # May be specified directly, or via sub-function.
        # Look at the number of entries.  In the latter case, there should be only an else-value.
        num_entries = [model[dd] for dd in model.decls() if str(dd) == "path"][0].num_entries()
        if num_entries == 0:
            # Extract the definition name from the else-value
            z3pathdecl = [model[dd].else_value().decl()
                          for dd in model.decls() if str(dd) == "path"][0]
        else:
            # Use "path" directly
            z3pathdecl = [dd for dd in model.decls() if str(dd) == "path"][0]

        # Extract the Z3 representation of the path
        z3path = [model[dd] for dd in model.decls() if dd == z3pathdecl][0]

        # Build output path
        path = [None] * (z3path.num_entries() + 1)  # For else_value()
        iii = 0
        while iii < z3path.num_entries():  # Not iterable (may be longer than true path?)
            path_entry_list = z3path.entry(iii).as_list()
            if path_entry_list[0].is_int():
                ee0 = int(path_entry_list[0].as_long())
                ee1 = path_entry_list[1]
                if ee0 >= 0 and ee0 < len(path):  # Ignore spurious path entries
                    path[ee0] = ee1  # Maybe need further checking?
            else:
                print("Unable to interpret path!")
                exit(1)
            iii += 1

        # Add in the else_value() element
        path_entry_list = z3path.else_value()
        try:
            iii = path.index(None)
            path[iii] = path_entry_list
        except:
            print("Unable to place path else_value()")
            raise

        # Translate path to user node names (now to avoid probs cf AST and strings)
        # Also remove extra None entries at end, and clip to t_limit
        path = [str(nodes[nn]) for nn in path if nn != None and nn in nodes]  # Ignore None
        path = path[0:t_limit]

        # Remove trailing sequence of Terminators
        # pylint: disable=invalid-name
        non_T = [n for n in range(len(path))
                 if [str(a) for a in path[n:]] == [NULLNODE]*(len(path)-n)]
        if non_T:
            path = path[:min(non_T)]

        return path

    def get_path_qfuflra(self, t_limit, nodes, pathvar):
        """Get a solution path (series of nodes and arcs).

        This version for SolverFor("QF_UFL[RI]A") (used for performance reasons, although
        questions remain over why it works: directly in Z3 it complains (rightly) about
        needing Quantifiers (QF) and Arrays (ArrayEx).  So Z3Py may be doing something.

        Could probably merge this with get_var_array_qfuflra.

        :param t_limit: max length of path specified
        :param nodes: string forms of node names.
        :param pathvar: path or apath
        Get path for current model solution.
        """
        model = self.solver.model()

        # Map internal node names to user names
        nodes = {model[dd]: dd for dd in model.decls() if dd.arity() == 0
                 and str(dd) != pathvar and str(dd) in nodes and not str(dd).endswith("_M")}

        # Extract the internal subfunction name for the path.
        # May be specified directly, or via sub-function.
        # Look at the number of entries.  In the latter case, there should be only an else-value.
        pathNode = [model[dd] for dd in model.decls() if str(dd) == pathvar][0]
        if not isinstance(pathNode, z3.ArrayRef):
            raise Exception("Cannot parse non-Array Z3 output")

        # There are two Select terms, one mapping Array values to True, the second (for the path)
        # mapping Array values to nodes.
        z3pathdecl = [dd for dd in model.decls()
                      if str(dd) == "Select" and type(model[dd].entry(0).value()) == z3.z3.ExprRef
                      and str(model[dd].entry(0).value()).startswith("node")][0]

        # Extract the Z3 representation of the path
        z3path = [model[dd] for dd in model.decls() if dd == z3pathdecl][0]
        lenpath = len([z3path.entry(ii) for ii in range(z3path.num_entries())
                       if z3path.entry(ii).arg_value(0) == pathNode])

        # Build output path
        path = [None] * (lenpath + 1)  # For else_value()
        iii = 0
        while iii < z3path.num_entries():  # Not iterable (may be longer than true path?)
            # Extract only the relevant entries
            if z3path.entry(iii).arg_value(0) == pathNode:
                path_entry_list = z3path.entry(iii).as_list()
                # (Array!val!0, 0) -> node!val!0
                if path_entry_list[1].is_int():
                    ee0 = int(path_entry_list[1].as_long())
                    ee1 = path_entry_list[-1]
                    if ee0 >= 0 and ee0 < len(path):  # Ignore spurious path entries
                        path[ee0] = ee1  # Maybe need further checking?  XXX
                else:
                    print("Unable to interpret path!")
                    exit(1)
            iii += 1

        # Add in the else_value() element
        path_entry_list = z3path.else_value()
        try:
            iii = path.index(None)
            path[iii] = path_entry_list
        except:
            print("Unable to place path else_value()")
            raise

        # Translate path to user node names (now to avoid probs cf AST and strings)
        # Also remove extra None entries at end, and clip to t_limit
        path = [str(nodes[nn]) for nn in path if nn != None and nn in nodes]  # Ignore None
        path = path[0:t_limit]

        # Remove trailing sequence of Terminators
        # pylint: disable=invalid-name
        non_T = [n for n in range(len(path))
                 if [str(a) for a in path[n:]] == [NULLNODE] * (len(path) - n)]
        if non_T:
            path = path[:min(non_T)]

        return path

    def get_enablings_non_qfuflra(self, lenpath):
        """Get sequence of enablings (markings) for each node and arc.

        TBC: could ignore T entry at end of path.
        This version for Solver() rather than SolverFor("QF_UFL[RI]A")

        :param lenpath: length of path (required)
        """
        model = self.solver.model()

        # Map internal node marking names to user names (via their else_values).
        nodes = {model[dd].else_value().decl(): dd
                 for dd in model.decls() if str(dd).endswith("_M")}

        # Extract the enablings for each node
        enablings = {}
        for node, name in nodes.items():
            name = str(name)
            enablings[name] = [False] * lenpath  # Initialise no marking

            enabling = [model[dd] for dd in model.decls() if dd == node]
            if enabling:
                enabling = enabling[0]

                # If default is True rather than False either something has gone wrong or
                # the path we allowed was too short (current understanding)
                if bool(enabling.else_value()):
                    # print("!!! Apparent enabling error for", node, name, enabling)
                    enablings[name] = [True] * lenpath  # Initialise no marking

                for iii in range(enabling.num_entries()):
                    jjj, en_tf = enabling.entry(iii).as_list()
                    jjj = int(jjj.as_long())

                    if jjj >= 0 and jjj < len(enablings[name]):  # Ignore spurious path entries
                        enablings[name][jjj] = bool(en_tf)  # Is this really false?

        return enablings


    def get_enablings_qfuflra(self, lenpath, valuations):
        """Get sequence of enablings (markings) for each node and arc.

        TBC: could ignore T entry at end of path.
        This version for SolverFor("QF_UFL[RI]A").

        :param lenpath: length of path (required)
        :param valuations: for determining type of variable (in get_var_array: messy).
        """
        model = self.solver.model()

        enablings = {}
        for node in [str(dd) for dd in model.decls() if str(dd).endswith("_M")]:
            enablings[node] = self.get_var_array_qfuflra(lenpath-1, node, valuations)

        return enablings

    def get_var_array_non_qfuflra(self, lenpath, varname):
        """Get failonentry sequence.

        Tricky to interpret whether there is a subfunction, entries, or just an else value.
        This is for the version with Solver rather than SolverFor("QF_UFL[RI]A").

        :param lenpath: length of path (required)
        :param varname: variable name to report
        """
        model = self.solver.model()

        # Map internal name to user name.
        var = [dd for dd in model.decls() if str(dd) == varname][0]
        vv = model[var]
        ev = vv.else_value()
        if len(ev.children()) == 0:  # Simple value
            if z3.is_bool(ev):
                valuation = [bool(ev)] * (lenpath+1)
            elif z3.is_int(ev):
                valuation = [int(ev.as_long())] * (lenpath + 1)
            else:
                valuation = [ev.as_long()] * (lenpath + 1)
        else:
            decl = [model[dd].else_value().decl() for dd in model.decls() if str(dd) == varname]
            node = decl[0]  # Only one var

            # Extract the values at each timestep
            valuation = [False] * (lenpath+1)  # Initialise no marking

            modelval = [model[dd] for dd in model.decls() if dd == node]
            if modelval:
                modelval = modelval[0]

                bb = modelval.else_value()
                if z3.is_bool(bb):
                    if bool(modelval.else_value()):
                        valuation = [True] * (lenpath+1)  # Initialise no marking
                else:
                    valuation = [bb.as_long()] * (lenpath+1)

                for iii in range(modelval.num_entries()):
                    jjj, en_tf = modelval.entry(iii).as_list()
                    jjj = int(jjj.as_long())

                    if jjj >= 0 and jjj < len(valuation):  # Ignore spurious path entries
                        if z3.is_bool(en_tf):
                            valuation[jjj] = bool(en_tf)  # Is this really false?
                        else:
                            valuation[jjj] = en_tf.as_long()  # Is this really false?

        return valuation


    def get_var_array_qfuflra(self, lenpath, varname, valuations):
        """Get failonentry sequence.

        Tricky to interpret whether there is a subfunction, entries, or just an else value.
        This is the version for Solver("QF_UFL[RI]A").

        :param lenpath: length of path (required)
        :param varname: variable name to report
        :param valuations: to obtain type of variable -- perhaps could obtain from Z3?
        """
        model = self.solver.model()

        # Map internal name to user name.
        var = [dd for dd in model.decls() if str(dd) == varname][0]
        vv = model[var]

        # Rough determination of variable type
        if valuations[0] and varname in valuations[0].keys():
            if valuations[0][varname]["type"] == "INT":
                z3type = z3.IntNumRef
            else:  # Only tested INT and BOOL
                z3type = z3.BoolRef
        else:
            z3type = z3.BoolRef
        entries = [model[dd] for dd in model.decls()
                   if str(dd) == "Select" and 
                       type(model[dd].entry(0).value()) == z3type
                   ][0]

        # Attempt to convert elseval: TBC
        elseval = entries.else_value()
        if isinstance(elseval, z3.BoolRef):
            elseval = bool(elseval)
        elif isinstance(elseval, z3.IntNumRef):
            elseval = int(elseval.as_long())

        # Str due to apparent z3 type mismatch (unexplained/not apparent).
        entries = [(entries.entry(ii).arg_value(1), entries.entry(ii).value())
                   for ii in range(entries.num_entries())
                   if str(entries.entry(ii).arg_value(0)) == str(vv)]

        # Template for array
        valuation = [elseval] * (lenpath + 1)

        # Assume simple value rather than else_value.  May need to be extended.
        for index, value in entries:
            index = int(index.as_long())

            # ignore out-of range (within limit but not of interest)
            if index <= lenpath:
                if z3.is_bool(value):
                    valuation[index] = bool(value)
                elif z3.is_int(value):
                    valuation[index] = int(value.as_long())
                else:
                    valuation[index] = value.as_long()

        return valuation


    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-branches
    # pylint: disable=too-many-statements
    # pylint: disable=no-self-use
    def output_dot(self, model, strnodes,
                   act_nodes, start_nodes, end_nodes,
                   xordiv_nodes, xorconv_nodes,
                   plldiv_nodes, pllconv_nodes,
                   iordiv_nodes, iorconv_nodes,
                   arcs, bpmn, valuation,
                   dotfile=None, path=None, enablings=None):
        """Output the returned Z3 model in GraphViz dot format.

        :param model: z3 model
        :param strnodes
        :param act_nodes..arcs: lists of the various node types
        :param bpmn
        :param valuation: variable settings resulting in this model
        :param dotfile: output file name
        :param path: array of nodes and arcs on execution path (internal names)
        :param enablings: dictionary of enablings for each node and arc

        Note: this is a naive translation -- there may be better ways to parse.
        """
        if dotfile:
            fid = open(dotfile + ".dot", 'w')
        else:
            fid = sys.stdout
        print('''
digraph BPMN_V {
  forcelabels=true;
  ranksep="0.3"; fontsize="8"; remincross=true;
  margin="0.0.0.0"; rankdir="LR";
  edge [arrowsize="0.5", fontsize="8"];
  node [height="0.5",width="0.5", fontsize="8"];
''', file=fid)

        # Map internal to external names (e.g. node!val!0 --> a)
        map_acts = {}
        orientation = ""
        color = ""

        # Output variables
        if valuation:
            dotline = "  vars [orientation=0,color=black,shape=none,label=\""
            b_first = True
            for var, vardict in valuation.items():
                if not b_first:
                    dotline += "\n"
                else:
                    b_first = False
                dotline += var + ": " + str(vardict["value"]) + " (" + vardict["type"] + ")\n"
            dotline += "\"];"
            print(dotline, file=fid)

        # Arity 0 should pick out the internal-external name maps only.
        for node in [dd for dd in model.decls() if dd.arity() == 0
                     and str(dd) not in [NULLNODE, "path", "ior"]
                     and str(dd) in strnodes and not str(dd).endswith("_M")]:
            node_name = bpmn.obj_map[str(node)].name or bpmn.obj_map[str(node)].oid
            if node_name in xordiv_nodes + xorconv_nodes + iordiv_nodes + iorconv_nodes + plldiv_nodes + pllconv_nodes:
                node_name = " "
            shape = None

            internal_node = model.get_interp(node)
            map_acts[internal_node] = node
            style = ''
            if str(node) in act_nodes:
                shape, orientation, color, style = \
                    "box", 0, "black", "rounded"
            elif str(node) in xordiv_nodes:
                shape, orientation, color = "triangle", 90, "blue"
            elif str(node) in xorconv_nodes:
                shape, orientation, color = "triangle", 270, "blue"
            elif str(node) in plldiv_nodes + pllconv_nodes:
                shape, orientation, color = "diamond", 0, "red"
            elif str(node) in iordiv_nodes:
                shape, orientation, color = "trapezium", 90, "green"
            elif str(node) in iorconv_nodes:
                shape, orientation, color = "trapezium", 270, "green"
            elif str(node) in start_nodes + end_nodes:
                shape, orientation, color = "circle", 0, "black"
            else:
                True

            if shape:  # Node found
                # Identify if node is on path
                penwidth = '1'
                if path and str(node) in path:
                    fontbold = "bold"
                    fontcolor = "red"
                    xlabelpath = '[' + str(path.index(str(node))) + ']'
                    if str(node) != path[-1]:  # Don't highlight the final (incomplete) node as executed
                        penwidth = '3'
                else:
                    fontbold = ''
                    fontcolor = "black"
                    xlabelpath = ''

                # Highlight if node is enabled on the path.
                n_marked = str(node) + "_M"
                if enablings and n_marked in enablings:
                    if any(enablings[n_marked]):
                        penwidth = '3'

                    # List timesteps when this node/flow is enabled
                    enlist = ', '.join(
                        [str(iii) for iii in range(len(enablings[n_marked]))
                         if enablings[n_marked][iii]]
                    )
                    xlabelenab = '<br/>(' + enlist + ')'
                else:
                    xlabelenab = ''

                xlabeldata, xlabelguard = '', ''
                if str(node) in act_nodes:
                    if bpmn.obj_map[str(node)].guard:
                        xlabelguard = '[' + \
                                      bpmn.obj_map[str(node)].guard.replace('>', "&gt;")\
                                          .replace('<', "&lt;") + ']'
                    if bpmn.obj_map[str(node)].data:
                        xlabeldata = '(' + bpmn.obj_map[str(node)].data + ')'

                dotline = "     " + node.name() + ' ' \
                    + "[orientation=" + str(orientation) \
                    + ",color=" + color \
                    + ",shape=" + shape

                if style:
                    dotline += ',style="' + style + '"'

                dotline += ',label=<<font color="' + fontcolor + '"'
                if fontbold:
                    dotline += ' face="' + DOTFONT + '-Bold"'
                else:
                    dotline += ' face="' + DOTFONT + '-Regular"'
                dotline += '>' + node_name + '</font>>'  # node.name()

                if xlabelpath or xlabelenab or xlabelguard or xlabeldata:
                    dotline += ",fontsize=6,xlabel=<"
                    if xlabelpath:
                        dotline += '<font face="' + DOTFONT + '-Bold">' + xlabelpath \
                                   + "</font> "
                    if xlabelenab:
                        dotline += '<font face="' + DOTFONT + '-Bold" color="blue">' \
                                   + xlabelenab + "</font>"
                    if xlabelguard:
                        dotline += '<font face="' + DOTFONT + '-Bold" color="red">'
                        if xlabelpath or xlabelenab:
                            dotline += "<br/>"
                        dotline += xlabelguard + "</font>"
                    if xlabeldata:
                        dotline += '<font face="' + DOTFONT + '-Bold" color="green">'
                        if xlabelpath or xlabelenab or xlabelguard:
                            dotline += "<br/>"
                        dotline += xlabeldata + "</font>"
                    dotline += '>'

                if shape == "box":
                    dotline += ",height=0.3,width=0.3"
                dotline += ",penwidth=" + penwidth + "];"
                print(dotline, file=fid)

        for arc in arcs:
            m_fr, m_to = arc

            # Pseudo-node for arc, in path
            pseudo_node = m_fr + '_' + m_to
            if path and pseudo_node in path:
                color = "red"
                labelpath = '[' + str(path.index(pseudo_node)) + ']'
            else:
                color = ''
                labelpath = ''

            penwidth = '1'
            ps_marked = pseudo_node + "_M"
            if enablings and ps_marked in enablings:
                if any(enablings[ps_marked]):
                    penwidth = '3'
                # List timesteps when this node/flow is enabled
                enlist = ', '.join([str(iii) for iii in range(len(enablings[ps_marked]))
                                    if enablings[ps_marked][iii]])
                labelenab = '<br/>(' + enlist + ')'
            else:
                labelenab = ''

            dotline = "     " + m_fr + " -> " + m_to + " [penwidth=" + penwidth
            if color:
                dotline += ",color=" + color

            if labelpath or labelenab:
                dotline += ",fontsize=6,label=<"
                if labelpath:
                    dotline += '<font face="' + DOTFONT + '-Bold">' + labelpath \
                               + "</font> "
                if labelenab:
                    dotline += '<font face="' + DOTFONT + '-Bold" color="blue">' \
                               + labelenab + "</font>"
                dotline += '>'

            dotline += "];"

            print(dotline, file=fid)

        print('}', file=fid)

        if dotfile:
            fid.close()

    def get_strnodes(self):
        """Return a map of string node names to Z3 nodes."""
        return {str(dd): dd for dd in self.solver.model().decls() if dd.arity() == 0
                and str(dd) not in [NULLNODE, "path", "ior"]}

    # pylint: disable=no-self-use
    def create_z3_model_nodes(self, syntax_name, nodes, z3fun):
        """Create model nodes as z3 constraints to restrict the meta-model.

        This is mainly boilerplate code, so we can create the model by calling this
        function with a list of node names.

        NB z3bpmnv is hardcoded as the module name used by bpmnv_model.py.  This is
        rather untidy.

        :param syntax_name: name of variable to create
        :param nodes: nodes to create
        :param z3fun: name of function to check node type, e.g. isStart
        """
        str_syntax = syntax_name + """ = [
z3.ForAll(x,"""
        if nodes:  # Leave this section out if no nodes of this type
            str_syntax += """
  z3.Implies(z3.Not(z3.Or(
"""
            for node in nodes:
                str_syntax += "                 "
                str_syntax += "x == " + node + ", "
            str_syntax += """
                )
             ),
"""
        str_syntax += "          "
        str_syntax += "z3.Not(z3bpmnv." + z3fun + "(x))"

        if nodes:
            str_syntax += """
         )
"""
        str_syntax += "),"

        for node in nodes:
            str_syntax += "z3bpmnv." + z3fun + '(' + node + '), '
        str_syntax += ']'

        return str_syntax


    # pylint: disable=no-self-use
    def create_z3_model_arcs(self, syntax_name, arcs, z3fun):
        """Create model arcs as z3 constraints to restrict the meta-model.

        This is mainly boilerplate code, so we can create the model by calling this
        function with a list of arc names.

        :param syntax_name: name of variable to create
        :param arcs: pairs of nodes defining the arcs
        :param z3fun: name of function to check node type, e.g. flow
        """
        str_syntax = syntax_name + """ = [
z3.ForAll([x, y],
  z3.Not(
    z3.Or(
"""
        for arc in arcs:
            str_syntax += "z3.And(x == " + arc[0] + ", y == " + arc[1] + "), "
        str_syntax += """
      )
    ) == z3.Not(z3bpmnv.""" + z3fun + """(x, y))
  ),
"""
        for arc in arcs:
            str_syntax += "z3bpmnv." + z3fun + '(' + arc[0] + ", " + arc[1] + "), "
        str_syntax += ']'

        return str_syntax


    # pylint: disable=no-self-use
    def create_z3_initial_marking(self, syntax_name, zip_nodes, start_nodes):
        """Create z3 code for initial marking (start node only).

        :param syntax_name: name of variable to create
        :param zip_nodes: pairs (node name, corresponding marking array name)
        :param start_nodes: (should be single) start node
        """
        str_syntax = syntax_name + " = [z3.And("
        b_first = True
        for node, marked_node in zip_nodes:
            if b_first:
                b_first = False
            else:
                str_syntax += ", "

            if node in start_nodes:  # Should only be one
                str_syntax += marked_node + "[0]"
            else:
                str_syntax += "z3.Not(" + marked_node + "[0])"

        str_syntax += ', apath_ind_diff[0] == 0)'  # path and activity-only path start with same index
        # Not needed and has a performance degradation
        # str_syntax += "path[0] == " + start_nodes[0]
        str_syntax += ']'

        return str_syntax


    # pylint: disable=no-self-use
    def create_z3_initial_data(self, syntax_name, valuation):
        """Create z3 code for initial data settings (from valuation).

        :param syntax_name: name of variable to create
        :param valuation: dictionary var -> {type, value} of valuations for the data vars
        """
        str_syntax = syntax_name + " = [z3.And(z3.Not(failonentry[0]),\n"  # No failure at start

        if valuation:  # Ignore if None
            for var, defn in valuation.items():
                # Assume type already checked.
                varsort = defn["type"].upper()
                if varsort == "BOOL":
                    if defn["value"] or defn["value"] == "True":
                        str_syntax += "             " + var + "[0], \n"
                    else:
                        str_syntax += "             z3.Not(" + var + "[0]), \n"
                elif varsort in ["INT", "REAL"]:
                    str_syntax += "             " + var + "[0] == " + str(defn["value"]) + ", \n"
                elif varsort == "STRING":
                    # NB: Not yet tested STRING
                    print("Unable yet to process var type", defn["type"])
                else:
                    print("Ignoring invalid var type", defn["type"])

        str_syntax += "             ),"
        str_syntax += ']'

        return str_syntax


    def create_z3_marking_iteration(self,
                                    syntax_name, zip_nodes, start_nodes,
                                    xordiv_nodes, xorconv_nodes,
                                    pllconv_nodes,
                                    iordiv_nodes, iorconv_nodes,
                                    flow_nodes, inflows, outflows,
                                    t_limit, IORS):
        """Create z3 code for marking iteration after start node.

        :param syntax_name: name of variable to_node create
        :param zip_nodes: pairs (node name, corresponding marking array name)
        :param start_nodes: start nodes (should be only one)
        :param xordiv_nodes:
        :param xorconv_nodes:
        :param pllconv_nodes:
        :param iordiv_nodes:
        :param iorconv_nodes:
        :param flow_nodes: dict. mapping flow pseudo nodes to_node their source & dest nodes.
        :param inflows: dict. mapping nodes to_node incoming flows.
        :param outflows: dict. mapping nodes to_node incoming flows.
        :param t_limit: time iteration limit
        :param IORS: BPMN+V IORS array for matching divs and convs
        """
        zip_nodes = list(zip_nodes)  # need to_node re-use

        str_syntax = syntax_name + """ = [
z3.ForAll(iii,
# z3.Implies(z3.And(iii >= 0, iii < """ + str(t_limit) + """),  # not required if QF_UFL[RI]A used
    z3.And(
"""
        # No flows into start node(s), which is (are) only enabled at t == 0
        # NB Marking the start node is an anomaly (simulate marking pre-start-node).
        #    From here on, only flows will be marked.
        for node, marked_node in zip_nodes:
            if node in start_nodes:
                str_syntax += "        "
                str_syntax += marked_node + "[iii] == (iii == 0),\n"

        # All flows: either no change in marking
        # => following node was not executed, or appropriate change
        # Different behaviour for flows post-div, pre-conv.
        # pylint: disable=too-many-nested-blocks
        for node, marked_node in [(a, b) for a, b in zip_nodes
                                  if a not in start_nodes]:  # Flows only by parameter
            str_syntax += "        "
            str_syntax += "z3.Implies(" + marked_node + "[iii],\n"

            # No change in marking => following node not on path (not executed)
            # at the previous time step (detail depends on type of node_
            fr_node, to_node = flow_nodes[node]
            str_syntax += "          "
            str_syntax += "z3.Or(z3.And(" \
                          + marked_node + "[iii-1], path[iii-1] != " + to_node + "),\n"

            # If the previous node was the start node, it would be marked,
            # else we need to_node go back to_node the input arcs to_node that node.
            in_arc1 = None
            if fr_node in start_nodes:
                from_marked = [b for a, b in zip_nodes if fr_node == a][0]
            else:
                in_arc1 = inflows[fr_node][0]  # (assume one)
                from_marked = [b for a, b in zip_nodes if in_arc1 == a][0]

            # In general, inputs to_node source node enabled at last time step, no longer enabled
            # ("tokens moved")/   Duplicate for different types of node for easier reading.
            if fr_node in start_nodes:
                str_syntax += "             "
                str_syntax += "z3.And(" \
                              + from_marked + "[iii-1], z3.Not(" + from_marked + "[iii]), "
                str_syntax += "path[iii-1] == " + fr_node + ",\n"
                str_syntax += "             ))),\n"  # close Or, And and Implies

            elif fr_node in xordiv_nodes:
                str_syntax += "             "
                str_syntax += "z3.And(" \
                              + from_marked + "[iii-1], z3.Not(" + from_marked + "[iii]),\n"

                # No other out arcs enabled -- include in the And
                for outflow in outflows[fr_node]:
                    if outflow != node:
                        outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                        str_syntax += "                 "
                        str_syntax += "z3.Not(" + outflow_m + "[iii]),\n"

                # Add the path check (req for pll)
                str_syntax += "path[iii-1] == " + fr_node + ",\n"
                str_syntax += "             ))),\n"  # close And, Or and Implies

            elif fr_node in xorconv_nodes:
                str_syntax += "             "
                str_syntax += "z3.And(" \
                              + from_marked + "[iii-1], z3.Not(" + from_marked + "[iii])),\n"

                # One incoming arc only, enabled and removed.
                for outflow in inflows[fr_node]:
                    if outflow != in_arc1:  # Already output this one
                        outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                        str_syntax += "             "
                        str_syntax += "z3.And(" \
                                      + outflow_m + "[iii-1], z3.Not(" + outflow_m + "[iii]), "

                        # Add the path check for each
                        str_syntax += "path[iii-1] == " + fr_node + "),\n"

                str_syntax += "             )),\n"  # close Or and Implies

            # Nothing for plldiv_nodes (don't enforce all enabled; not valid when one path moves on)

            elif fr_node in pllconv_nodes:
                # All enabled; remove all.
                str_syntax += "             "
                str_syntax += "z3.And(\n"

                for outflow in inflows[fr_node]:
                    outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                    str_syntax += "                 "
                    str_syntax += outflow_m + "[iii-1], z3.Not(" + outflow_m + "[iii]),\n"

                str_syntax += "path[iii-1] == " + fr_node + ",\n"  # Add the path check
                str_syntax += "             "
                str_syntax += "))),\n"  # Close the And, Or and Implies

            elif fr_node in iordiv_nodes:
                ior_pair = "ior_" + fr_node
                str_syntax += "             "
                str_syntax += "z3.And(" \
                              + from_marked + "[iii-1], z3.Not(" + from_marked + "[iii]),\n"

                # Store in ior (TBC for multiple / nested IOR) XXX
                n_out = len(outflows[fr_node])

                # Number of ior selected implies how many of the other arcs may be enabled.
                # NB: Limited at present to_node 3-way output.
                for n_ior in range(1, n_out+1):
                    str_syntax += "                 "
                    str_syntax += '(' + ior_pair + " == " + str(n_ior) + ") == "

                    # ior=(all outputs): all other outgoing arcs must be enabled.
                    if n_ior == n_out:
                        if n_out > 2:
                            str_syntax += "z3.And("
                        for outflow in outflows[fr_node]:
                            if outflow != node:
                                outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                                str_syntax += outflow_m + "[iii], "
                        if n_out > 2:
                            str_syntax += "),"
                        str_syntax += "\n"

                    # ior=1: none of the other outgoing arcs may be enabled.
                    # (NB this could be consolidated with others).
                    elif n_ior == 1:
                        if n_out > 2:
                            str_syntax += "z3.And("
                        for outflow in outflows[fr_node]:
                            if outflow != node:
                                outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                                str_syntax += "z3.Not(" + outflow_m + "[iii]), "
                        if n_out > 2:
                            str_syntax += "),"
                        str_syntax += "\n"

                    else:  # at least n_out-n_ior of the other arcs are not enabled.
                        # NB: Currently this will only work for 3-way IORs
                        if n_out > 2:
                            str_syntax += "z3.Xor("
                        for outflow in outflows[fr_node]:
                            if outflow != node:
                                outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                                str_syntax += outflow_m + "[iii], "
                        if n_out > 2:
                            str_syntax += "),"
                        str_syntax += "\n"

                str_syntax += "                 "
                str_syntax += "path[iii-1] == " + fr_node + ",\n"  # Add the path check
                str_syntax += "             "
                str_syntax += "))),\n"

            elif fr_node in iorconv_nodes:
                ior_pair = "ior_" + [ior.pair for ior in IORS if ior.oid == fr_node][0].oid

                # Check for the appropriate number of incoming IOR arcs enabled and remove.
                n_in = len(inflows[fr_node])
                str_syntax += "             "
                str_syntax += "z3.And(\n"  # To include the path check

                for n_ior in range(n_in, 0, -1):
                    if n_ior > 1:
                        str_syntax += "                 "
                        str_syntax += "z3.If(" + ior_pair + " == " + str(n_ior) + ",\n"

                    # NB: Hardcoding!
                    if n_ior == n_in:
                        str_syntax += "                    "
                        str_syntax += "z3.And(\n"

                        for outflow in inflows[fr_node]:
                            outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                            str_syntax += "                        "
                            str_syntax += outflow_m + "[iii-1]" + ", z3.Not(" \
                                          + outflow_m + "[iii]),\n"
                        str_syntax += "                       "
                        str_syntax += "),\n"

                    elif n_ior == 1:
                        str_syntax += "                    "
                        str_syntax += "z3.Or(\n"

                        for outflow in inflows[fr_node]:
                            outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                            str_syntax += "                       "
                            str_syntax += "z3.And(" + outflow_m + "[iii-1]" \
                                          + ", z3.Not(" + outflow_m + "[iii])),\n"
                        str_syntax += "                    "
                        str_syntax += "),\n"  # Close the Or(

                    else:
                        # remaining ior: n_ior of the incoming were enabled, now no longer
                        str_syntax += "                   "
                        str_syntax += "z3.Or(\n"
                        for iii in range(2, n_in):
                            for enabled_inflows in itertools.combinations(inflows[fr_node], iii):
                                str_syntax += "                      "
                                str_syntax += "z3.And(\n"

                                for outflow in enabled_inflows:
                                    outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                                    str_syntax += "                          "
                                    str_syntax += outflow_m + "[iii-1], z3.Not(" \
                                                  + outflow_m + "[iii]),\n"
                                str_syntax += "                         "
                                str_syntax += "),\n"
                            str_syntax += "                    "
                            str_syntax += "),\n"

                str_syntax += "        "
                for n_ior in range(n_in, 1, -1):
                    str_syntax += "),"  # close the If(s
                str_syntax += "\n"
                str_syntax += "        "
                str_syntax += "path[iii-1] == " + fr_node + ",\n"  # Add the path check
                str_syntax += "))),\n"  # close the And(, Or( and Implies(

            else:
                str_syntax += "             "
                str_syntax += "z3.And(" + from_marked + "[iii-1], z3.Not(" \
                              + from_marked + "[iii]),\n"
                str_syntax += "                 "
                str_syntax += "path[iii-1] == " + fr_node + ",\n"  # Add the path check
                str_syntax += "             ))),\n"  # close And, Or and Implies

        str_syntax += "       ),\n"
        str_syntax += "#    ),\n"  # Complete the Implies: not required if QF_UFL[RI]A used
        str_syntax += "  )\n"
        str_syntax += "]"

        return str_syntax


    def create_z3_firing(self, syntax_name, nodes, zip_nodes, marked_nodes,
                         start_nodes, end_nodes, act_nodes,
                         xordiv_nodes, xorconv_nodes,
                         plldiv_nodes,
                         iordiv_nodes, iorconv_nodes,
                         flow_pseudo_nodes, inflows, outflows,
                         t_limit,
                         bpmn, nosuccess):
        """Create z3 code for firing semantic (relate path to markings).

        :param syntax_name: name of variable to create
        :param nodes
        :param zip_nodes: pairs (node name, corresponding marking array name)
        :param marked_nodes
        :param start_nodes: (should only be one)
        :param end_nodes: (should only be one)
        :param act_nodes:
        :param xordiv_nodes:
        :param xorconv_nodes:
        :param plldiv_nodes:
        :param iordiv_nodes:
        :param iorconv_nodes:
        :param flow_pseudo_nodes: nodes representing flows
        :param inflows: dict. mapping nodes to incoming flows.
        :param outflows: dict. mapping nodes to incoming flows.
        :param t_limit: time iteration limit
        :param bpmn: BPMN+V object
        :param nosuccess: Do not ask Z3 for successful models.
        """
        zip_nodes = list(zip_nodes)  # need to re-use
        t_limit = str(t_limit)
        vardefs = bpmn.vardefs
        grammar = BPMNVparse.Parser()  # grammars for interpreting guards and data modifications

        str_syntax = syntax_name + """ = [
z3.ForAll(iii,
  z3.Implies(
    z3.And(iii >= 0, iii < """ + t_limit + """),
    z3.And(z3.Or(
"""
        # Path is one of the nodes or T
        for node in nodes + [NULLNODE]:
            if node in bpmn.obj_map:
                if bpmn.obj_map[node].name:
                    nodename = "  # " + bpmn.obj_map[node].name
                else:
                    nodename = "  # " + bpmn.obj_map[node].oid
            else:
                nodename = ""
            str_syntax += "           "
            str_syntax += "path[iii] == " + node + ',' + nodename + "\n"
        str_syntax += "          "
        str_syntax += "),\n"

        # Effect of executing each node.  (May duplicate some constraints in marking?)
        # pylint: disable=too-many-nested-blocks
        for node in nodes + [NULLNODE]:
            if node in bpmn.obj_map:
                if bpmn.obj_map[node].name:
                    nodename = "  # " + bpmn.obj_map[node].name  # Comment for debugging
                else:
                    nodename = "  # " + bpmn.obj_map[node].oid
            else:
                nodename = ""
            str_syntax += "       "
            str_syntax += "(path[iii] == " + node + ") ==" + nodename + "\n"

            if node == NULLNODE:
                # Either reached here by failing, or at end and no longer in any other nodes
                str_syntax += "          z3.And(z3.Or(failonentry[iii],\n"  # we got here by failure

                # Repeat this section for ending with or without failure
                for boolval in [True, False]:
                    str_syntax += "                 z3.And(z3.Not(z3.Or(\n"
                    for marking in start_nodes + flow_pseudo_nodes:
                        marking_m = [b for a, b in zip_nodes if marking == a][0]
                        str_syntax += "                            "
                        str_syntax += marking_m + "[iii],\n"
                    str_syntax += "                        "
                    str_syntax += ")),\n"

                    # Previous either end node or NULLNODE
                    nn_end = end_nodes[0]  # Should only be one
                    str_syntax += "                    "
                    str_syntax += "z3.Or(path[iii-1] == " + NULLNODE + ",\n"
                    str_syntax += "                       "
                    str_syntax += "path[iii-1] == " + nn_end + "),\n"
                    str_syntax += "                    "
                    if boolval:
                        str_syntax += "failonentry[iii]\n"  # end, with failure
                    else:
                        str_syntax += "z3.Not(failonentry[iii])\n"  # without failure
                    str_syntax += "                 ),\n"  # Close the And(
                str_syntax += "             ),\n"  # Close the Or(
                str_syntax += "             path[iii+1] == T"
                str_syntax += "          ),\n"  # Close the And(

            elif node in start_nodes:
                str_syntax += "          "
                str_syntax += "z3.And("

                marked_node = [b for a, b in zip_nodes if node == a][0]
                out_arc = outflows[node][0]  # Should be only one
                out_arc_m = [b for a, b in zip_nodes if out_arc == a][0]
                str_syntax += marked_node + "[iii], z3.Not(" + marked_node + "[iii+1]),\n"

                # Account for enabling at next time step, or being beyond the search limit
                str_syntax += "              "
                str_syntax += "z3.Or(" + out_arc_m + "[iii+1], iii >= " + t_limit + "-1),\n"

            elif node in end_nodes:
                str_syntax += "          "
                str_syntax += "z3.And("

                in_arc = inflows[node][0]
                marked_node = [b for a, b in zip_nodes if in_arc == a][0]
                str_syntax += marked_node + "[iii], z3.Not(" + marked_node + "[iii+1]),\n"

            else:
                # All of the input arcs were enabled at previous time step and are no longer.
                # all output arcs are now enabled.
                # Differs for xordiv (only one output enabled) and iordiv (TBC).
                in_arcs = inflows[node]
                out_arcs = outflows[node]
                str_syntax += "          "

                # Code here duplicated for ease of reading
                n_out = None
                n_in = None
                if node in xordiv_nodes:
                    str_syntax += "z3.And(\n"
                    for in_arc in in_arcs:
                        in_arc_m = [b for a, b in zip_nodes if in_arc == a][0]
                        str_syntax += "              "
                        str_syntax += in_arc_m + "[iii], z3.Not(" + in_arc_m + "[iii+1]),\n"

                    # Only one of the subsequent flows will be enabled.
                    str_syntax += "              z3.Or(\n"

                    for out_arc in out_arcs:
                        out_arc_m = [b for a, b in zip_nodes if out_arc == a][0]
                        str_syntax += "                 "
                        str_syntax += out_arc_m + "[iii+1],\n"

                    str_syntax += "iii >= " + t_limit + "-1"  # Account for search limit
                    str_syntax += "                ),\n"

                elif node in xorconv_nodes:
                    str_syntax += "z3.And(z3.Or(\n"

                    for in_arc in in_arcs:
                        in_arc_m = [b for a, b in zip_nodes if in_arc == a][0]
                        str_syntax += "                 "
                        str_syntax += "z3.And(" + in_arc_m + "[iii], z3.Not(" \
                                      + in_arc_m + "[iii+1])),\n"

                    str_syntax += "                ),\n"

                    # Single outgoing arc is enabled
                    inflow = out_arcs[0]
                    inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                    str_syntax += "              "
                    str_syntax += "z3.Or(" + inflow_m + "[iii+1], iii >= " \
                                  + t_limit + "-1),\n"

                elif node in plldiv_nodes:
                    str_syntax += "z3.And(\n"
                    for in_arc in in_arcs:
                        in_arc_m = [b for a, b in zip_nodes if in_arc == a][0]
                        str_syntax += "              "
                        str_syntax += in_arc_m + "[iii], z3.Not(" + in_arc_m + "[iii+1]),\n"

                    str_syntax += "              "
                    str_syntax += "z3.Or(z3.And(\n"  # to account for the limit check
                    for out_arc in out_arcs:
                        out_arc_m = [b for a, b in zip_nodes if out_arc == a][0]
                        str_syntax += "                     "
                        str_syntax += out_arc_m + "[iii+1],\n"
                    str_syntax += "              ),\n"  # Close the And(
                    str_syntax += "              "
                    str_syntax += "iii >= " + t_limit + "-1),\n"

                elif node in iordiv_nodes:
                    ior_pair = "ior_" + node

                    str_syntax += "z3.And(\n"
                    for in_arc in in_arcs:
                        in_arc_m = [b for a, b in zip_nodes if in_arc == a][0]
                        str_syntax += "              "
                        str_syntax += in_arc_m + "[iii], z3.Not(" + in_arc_m + "[iii+1]),\n"
                        if node in xorconv_nodes:
                            str_syntax += "             ),\n"

                    n_out = len(outflows[node])

                    str_syntax += "              z3.Or(\n"  # Account for the limit check
                    for n_ior in range(n_out, 0, -1):
                        if n_ior > 1:
                            str_syntax += "                 "
                            str_syntax += "z3.If(" + ior_pair + " == " + str(n_ior) + ",\n"

                        # ior=(all outputs): all other outgoing arcs must be enabled.
                        if n_ior == n_out:
                            str_syntax += "                    "
                            str_syntax += "z3.And("
                            for inflow in outflows[node]:
                                inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                str_syntax += inflow_m + "[iii+1], "
                            str_syntax += "),\n"

                        # ior=1: none of the other outgoing arcs may be enabled.
                        elif n_ior == 1:
                            str_syntax += "                    "
                            str_syntax += "z3.Or("
                            for inflow in outflows[node]:
                                inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                str_syntax += inflow_m + "[iii+1], "
                            str_syntax += "),\n"

                        else:  # at least n_out-n_ior of the other arcs are not enabled.
                            # NB: Only valid for IOR=3
                            str_syntax += "                     "
                            str_syntax += "z3.Or(\n"
                            rem_flows = [ff for ff in outflows[node] if ff != node]

                            for disabled_inflows in itertools.combinations(rem_flows, n_ior):
                                str_syntax += "                        "
                                str_syntax += "z3.And("

                                for inflow in disabled_inflows:
                                    inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                    str_syntax += inflow_m + "[iii+1], "
                                str_syntax += "),\n"
                            str_syntax += "),\n"  # Close the Or(

                    str_syntax += "              "
                    for n_ior in range(n_out, 1, -1):
                        str_syntax += "),"  # close the If(s
                    str_syntax += "\n"
                    str_syntax += "              "
                    str_syntax += "iii >= " + t_limit + "-1),\n"
                    str_syntax += "\n"

                elif node in iorconv_nodes:
                    ior_pair = "ior_" + [ior.pair for ior in bpmn.IOR if ior.oid == node][0].oid

                    str_syntax += "z3.And(\n"
                    n_in = len(inflows[node])

                    for n_ior in range(n_in, 0, -1):
                        if n_ior > 1:
                            # NB: Assume ending with _ to match iordiv
                            str_syntax += "              "
                            str_syntax += "z3.If(" + ior_pair + " == " + str(n_ior) + ",\n"

                        # ior=(all outputs): all other incoming arcs must be enabled.
                        if n_ior == n_in:
                            str_syntax += "                 "
                            str_syntax += "z3.And(\n"
                            for inflow in inflows[node]:
                                inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                str_syntax += "                     "
                                str_syntax += inflow_m + "[iii], z3.Not(" \
                                              + inflow_m + "[iii+1]), \n"
                            str_syntax += "                    "
                            str_syntax += "),\n"

                        # ior=1: none of the other incoming arcs may be enabled.
                        elif n_ior == 1:
                            str_syntax += "                 "
                            str_syntax += "z3.Or(\n"
                            for inflow in inflows[node]:
                                inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                str_syntax += "                    "
                                str_syntax += "z3.And(" + inflow_m + "[iii], z3.Not(" \
                                              + inflow_m + "[iii+1])), \n"
                            str_syntax += "                   "
                            str_syntax += "),\n"

                        else:  # at least n_out-n_ior of the other arcs are not enabled.
                            str_syntax += "                 "
                            str_syntax += "z3.Or(\n"
                            rem_flows = [ff for ff in inflows[node] if ff != node]

                            for disabled_inflows in itertools.combinations(rem_flows, n_ior):
                                str_syntax += "                    "
                                str_syntax += "z3.And(\n"

                                for inflow in disabled_inflows:
                                    str_syntax += "                        "
                                    inflow_m = [b for a, b in zip_nodes if inflow == a][0]
                                    str_syntax += inflow_m + "[iii], z3.Not(" \
                                                  + inflow_m + "[iii+1]), \n"
                                str_syntax += "                    "
                                str_syntax += "),\n"
                            str_syntax += "                   "
                            str_syntax += "),\n"  # Close Or(

                    outflow = out_arcs[0]
                    outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                    str_syntax += "              "
                    str_syntax += "z3.Or(" + outflow_m + "[iii+1], iii >= " + t_limit + "-1)\n"

                    str_syntax += "             "
                    for n_ior in range(n_in, 1, -1):
                        str_syntax += "),"  # close the If(s
                    str_syntax += "\n"  # close the And(

                else:
                    str_syntax += "z3.And(\n"
                    for in_arc in in_arcs:
                        in_arc_m = [b for a, b in zip_nodes if in_arc == a][0]
                        str_syntax += "              "
                        str_syntax += in_arc_m + "[iii], z3.Not(" + in_arc_m + "[iii+1]),\n"

                    # Single outgoing arc is enabled
                    outflow = out_arcs[0]
                    outflow_m = [b for a, b in zip_nodes if outflow == a][0]
                    str_syntax += "              "
                    str_syntax += "z3.Or(" + outflow_m + "[iii+1], iii >= " + t_limit + "-1),\n"

            # Check guards and pass on the data
            padding = "             "  # pass to guard_to_z3 for tidy debugging
            if node != NULLNODE:
                # If activity node, we need to check for a guard and data modifications, parse
                # and set constraints as necessary, else defaults (no guard/mod, other node types).
                str_guard = ""
                str_mod = ""
                modvars = list(vardefs.keys())  # List of vars to eliminate if modifcations found

                if node in act_nodes:
                    guard = bpmn.obj_map[node].guard
                    data = bpmn.obj_map[node].data

                    # If there's a guard we need to parse and check it, then set failonentry
                    # constraint.
                    if guard:
                        str_guard = self.guard_to_z3(guard, grammar, bpmn, padding)

                    # If there are data modifications we parse them, then set the Z3 constraints.
                    if data:
                        for datamod in grammar.adatamod.parselist(data):
                            z3str, var = self.datamod_to_z3(datamod, grammar, bpmn, padding)
                            str_mod += z3str
                            modvars.remove(var)

                    # Report in apath: no adjustment to the index
                    str_syntax += padding + "apath_ind_diff[iii+1] == apath_ind_diff[iii],\n"
                    str_syntax += padding + "apath[iii-apath_ind_diff[iii]] == path[iii],\n"
                else:
                    # No report in apath: adjust the index
                    str_syntax += padding + "apath_ind_diff[iii+1] == apath_ind_diff[iii] + 1,\n"

                if str_guard:
                    str_syntax += str_guard
                else:
                    if node not in end_nodes:
                        str_syntax += padding + "failonentry[iii+1] == failonentry[iii],\n"

                if str_mod:
                    str_syntax += str_mod

                for var in modvars:
                    # Z3 constraints for any remaining - copy value to the next time step.
                    str_syntax += "             "
                    str_syntax += var + "[iii+1] == " + var + "[iii],\n"

                str_syntax += "          ),\n"

        # Else section (outside search boundary)
        if nosuccess:
            nn_end = end_nodes[0]  # Should only be one
            str_syntax += "       z3.Not(z3.Exists(iii, z3.And(path[iii] == " + nn_end \
                          + ", path[iii+1] == " + NULLNODE + ")))"
        str_syntax += "       ),\n"
        str_syntax += "))]"

        return str_syntax


    # pylint: disable=no-self-use
    def guard_to_z3(self, guard, grammar, bpmn, padding):
        """Return Z3 syntax string for checking BPMN+V guard.

        XXX This may need some work for robustness.

        :param guard: BPMN+V data (activity) guard string(s)
        :param grammar: BPMNV grammar object
        :param bpmn: BPMNV object
        :param padding: supplied only for nicer formatting for debugging
        """
        # Parse the guard - all vars - try to interpret it, rather than assuming it is
        # valid z3 - then set the negation.
        z3str = padding + "z3.If(z3.Not(z3.And("

        for _guard in guard.split(','):
            _guard = _guard.strip()

            (var, oper, val, neg) = grammar.aguard.parse(_guard, bpmn.vardefs)

            # If-condition: negate the condition
            if neg or neg.lower() == "not":
                z3str += "z3.Not("
            z3str += var + "[iii]"  # Current instantiation of variable
            if oper and val is not None:  # (Exclude duplicated "neg")
                z3str += oper + ' ' # Assume included if not empty ...
                z3str += str(val)  # ... may need further work.
            if neg or neg.lower() == "not":
                z3str += ")"
            z3str += ",\n"
            z3str += padding + "                    "

        z3str += ")),\n"  # Close the Not(And(
        z3str += padding + "   " + "failonentry[iii+1],\n"  # Then
        z3str += padding + "   " + "failonentry[iii+1] == failonentry[iii]),\n"  # Else

        return z3str


    # pylint: disable=no-self-use
    def datamod_to_z3(self, datamod, grammar, bpmn, padding):
        """Return Z3 syntax string for modifying BPMN+V variable.

        :param datamod: BPMN+V data (activity) modification string
        :param grammar: BPMNV grammar object
        :param bpmn: BPMNV object
        :param padding: supplied only for nicer formatting for debugging
        """
        try:
            (modneg, modvar, assvar, modop, modval) = grammar.adatamod.parse(bpmn.vardefs, datamod)
        except:
            print("Problem with grammar")
            raise

        if not assvar:
            assvar = modvar

        # Assvar assignment at next step may depend on modvar (may be same) at current.
        # This may need further work.  XXX
        z3str = padding + assvar + "[iii+1] == "
        if modneg and modneg.lower() == "not":
            z3str += "z3.Not("
        if modvar and modop:
            z3str += modvar + "[iii] "
        if modop:
            z3str += modop + ' '
        if modval:
            z3str += str(modval).capitalize()  # Cater for True/False
        if modneg and modneg.lower() == "not":
            z3str += ")"
        z3str += ",\n"

        return z3str, assvar


def main():
    """Capture and alert command-line calls."""
    print("Z3BPMNV.py: load as module only.")

if __name__ == "__main__":
    main()
