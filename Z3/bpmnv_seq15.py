#!/usr/bin/env python

# 2018-05-04 Z3 constraint model for a basic sequence construct.
#            a->b.  Solution paths list nodes only (not arcs (flows)).
#            Markings on start node (p0) then flows.

from z3 import *

if len(sys.argv) > 1:  # z3 imports sys
  lim = sys.argv[1]
else:
  lim = 10  # upper bound search (but it has little time effect if large)

# Syntax
# Metamodel
node = DeclareSort('node')  # Eventually cover Activity, splits, joins
### flow = Function('flow', node, node, BoolSort())

# Declarations for the axioms
# x, y, T = Consts('x y T', node)  # T: terminator
T = Const('T', node)  # T: terminator

# Model
# Nodes & arcs (as nodes)
p0, a, b, pe = Consts('p0 a b pe', node)

# Flows
# Syntax is implicit in the semantics - but Distinct is necessary
syntax = [
###   ForAll([x, y],  # Adding the Implies back slows this down!
###     Not(Or(And(x == p0, y == a),
###            And(x == a, y == b),
###            And(x == b, y == pe)
###         )
###     ) == Not(flow(x, y))
###   ),
###   flow(p0, a),
###   flow(a, b),
###   flow(b, pe),
  Distinct(p0, pe, a, b, T)
]

# Semantics
semantics = []
ii, limit = Ints('ii limit')

path   = Array('path',   IntSort(), node)  # list of nodes
p0_M   = Array('p0_M',   IntSort(), BoolSort())  # special for init

p0_a_M = Array('p0_a_M', IntSort(), BoolSort())  # Flow markings
a_b_M  = Array('a_b_M',  IntSort(), BoolSort())
b_pe_M = Array('b_pe_M', IntSort(), BoolSort())

# Data
med = Array('med', IntSort(), BoolSort())  # variable for guard
failonentry = Array('failonentry', IntSort(), BoolSort())

# Initial marking:
mark0 = And(p0_M[0],
            Not(p0_a_M[0]), Not(a_b_M[0]), Not(b_pe_M[0]))
semantics.append(mark0)
data0 = And(Not(failonentry[0]), med[0])  # Initially True
# data0 = And(Not(failonentry[0]), Not(med[0]))  # Initially False
semantics.append(data0)

# Subsequent markings depend on initial and flow
mark = [
  limit == lim,  # Req to bound search  OK for short paths or beyond end.
  ForAll(ii,
    # Large speed up changing from If to Implies here (in conjunction w/ fired).
    # (Now have junk in path outside [t0, limit], but see fired.)
#   If(And(ii >= 0, ii <= limit),  # Seems to be necessary
#   Implies(And(ii >= 0, ii <= limit),  # No longer required
       And(p0_M[ii] == (ii == 0),  # No flows into p0
           Implies(p0_a_M[ii],
             Or(And(p0_a_M[ii-1], path[ii-1] != a),
                And(p0_M[ii-1], Not(p0_M[ii]), path[ii-1] == p0)),
           ),
           Implies(a_b_M[ii],
             Or(And(a_b_M[ii-1], path[ii-1] != b),
                And(p0_a_M[ii-1], Not(p0_a_M[ii]), path[ii-1] == a)),
           ),
           Implies(b_pe_M[ii],
             Or(And(b_pe_M[ii-1], path[ii-1] != pe),
                And(a_b_M[ii-1], Not(a_b_M[ii]), path[ii-1] == b)),
           ),
       ),
#      path[ii] == T,  # Else
#   )
  )
]
semantics += mark

# List of fired nodes - node fired implies it was marked (no enablings via
# predecessor tokens here: Don't need to imply false otherwise, as other
# markings could have been posible.
fired = [
  ForAll(ii,
    Implies(And(ii >= 0, ii < limit),
       And(Or(path[ii] == p0, path[ii] == a, path[ii] == b, path[ii] == pe,
              path[ii] == T),
           (path[ii] == p0) ==
              And(p0_M[ii], Not(p0_M[ii+1]), p0_a_M[ii+1],
                  med[ii+1] == med[ii],
                  failonentry[ii+1] == failonentry[ii]
              ),
           (path[ii] == a) ==
              And(p0_a_M[ii], Not(p0_a_M[ii+1]), a_b_M[ii+1],
                  If(med[ii],
                     failonentry[ii+1],
                     failonentry[ii+1] == failonentry[ii]),
                  med[ii+1] == med[ii],
              ),
           (path[ii] == b) ==
              And(a_b_M[ii], Not(a_b_M[ii+1]), b_pe_M[ii+1],
                  med[ii+1] == med[ii],
                  failonentry[ii+1] == failonentry[ii]
              ),
           (path[ii] == pe) ==
              And(b_pe_M[ii], Not(b_pe_M[ii+1], med[ii+1] == med[ii])
              ),
           (path[ii] == T) ==
               And(Or(failonentry[ii],  # we got here by failure
                      And(Not(Or(p0_M[ii],
                                 p0_a_M[ii], a_b_M[ii], b_pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          failonentry[ii]  # with or without failure
                      ),
                      And(Not(Or(p0_M[ii],
                                 p0_a_M[ii], a_b_M[ii], b_pe_M[ii])),
                          Or(path[ii-1] == T,  # End cases
                             path[ii-1] == pe,
                            ),
                          Not(failonentry[ii])
                      ),
                   ),
                   path[ii+1] == T
               ),
           ),
       # And(path[ii] == T, Not(Or(p0_M[ii], p0_a_M[ii], a_b_M[ii], b_pe_M[ii]))),
    )
  )
]
semantics += fired

# s = Solver()
s = SolverFor("QF_UFLRA")  # Faster
s.add(syntax)
s.add(semantics)

# for aa in s.assertions(): print(aa)
# exit()

r = s.check()
print("RESULT", r)
if r == sat:
  print(s.model())
  # s.add([Not(And(path[1] == a, path[2] == b))])
  s.add([Not(path[1] == a)])
  r = s.check()
  print("RESULT", r)
  if r == sat:
    print(s.model())
