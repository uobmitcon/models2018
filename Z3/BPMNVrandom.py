#!/usr/bin/env python3
""" Phil Weber 2017-12-13 Experimental framework for BPMNV + Z3 Analsyis with composition.

Taken from the BPMNV + CPN/Tools version.
"""

import sys
import argparse
from os import path, system
import numpy as np
from scipy import stats as st
from BPMNV import BPMNV, rnd_conflict
from bpmnv_model import bpmnv_model
from bpmnv_result import bpmnv_result

SEP = ','  # for output

def output_header():
    """Stats header."""

    print()
    print(',' * 10, end='')
    print("Mean", ',' * 24, end='')
    print("Stdev", ',' * 24, end='')
    print("Stderr")

    print("Parameters", ',' * 10, end='')
    print("Single Models", ',' * 12, end='')
    print("Composed", ',' * 12, end='')
    print("Single Models", ',' * 12, end='')
    print("Composed", ',' * 12, end='')
    print("Single Models", ',' * 12, end='')
    print("Composed",)

    # Params
    print("Models", "p(seq)", "p(xor)", "p(ior)", "p(and)", "Conflicts", sep=SEP, end=SEP)
    print("p(p1m1v)", "p(p1m2v)", "p(p2m1v)", "p(p2m2v)", sep=SEP, end=SEP)
    # Means
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    # Stdevs
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    # Stderrs
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP)


if __name__ == "__main__":
    B_PRINT = False
    B_LOG = False
    B_TIMINGS = False

    parser = argparse.ArgumentParser(
        description="Create and analyse pairs of random BPMN+V models.")
    parser.add_argument('-m', '--minmodelsize', dest="minmodelsize", type=int, default=2,
                        help="Number of activities in final model")
    parser.add_argument('-s', '--modelsize', dest="maxmodelsize", type=int, default=2,
                        help="Number of activities in final model")
    parser.add_argument('-n', '--models', dest="models", type=int, default=1,
                        help="Number samples of each size model")
    parser.add_argument('-c', '--conflicts', dest="conflicts", type=int, default=1,
                        help="Number guard/data conflicts between each model pair")
    parser.add_argument('-p', '--params', dest="params", default=None,
                        help="Opt. [p_seq,p_xor,p_ior,p_and,p1m1v,p1m2v,p2m1v,p2m2v]")
    parser.add_argument('-B', dest="dot_bpmn", action="store_true",
                        help="Output BPMN to eps")
    parser.add_argument('-X', dest="nocompos", action="store_true",
                        help="Single models only (no composition)")
    parser.add_argument('-d', '--dotfile', dest="dotfile",
                        help="Output model solutions to dot file", default=None)
    args = parser.parse_args()

    DOT_BPMN = args.dot_bpmn

    bCompos = True  # Do the composition; else single models
    if not vars(args):
        raise Exception("Problem with parameters!")
    else:
        if args.maxmodelsize:
            maxmodelsize = args.maxmodelsize
        if args.minmodelsize:
            minmodelsize = args.minmodelsize
        if args.models:
            models = args.models
        if args.conflicts:
            conflicts = args.conflicts
        if args.nocompos:
            bCompos = False
        if args.params:
            # p_seq, p_xor, p_ior, p_and, p1m1v, p1m2v, p2m1v, p2m2v = \
            #     [float(p) for p in
            #      args.params.replace('[', '').replace(']', '').split(',')]
            P = [float(p) for p in
                 args.params.replace('[', '').replace(']', '').split(',')]
            if len(P) == 8:
                p_seq, p_xor, p_ior, p_and, p1m1v, p1m2v, p2m1v, p2m2v = P
                if not (p1m1v + p1m2v + p2m1v + p2m2v) == 1.0:
                    raise Exception("Probabilities must sum to 1.0")
            elif len(P) == 4:
                p_seq, p_xor, p_ior, p_and = P
                p_conflicts = None
                p1m1v = p1m2v = p2m1v = p2m2v = None

            if not (p_seq + p_xor + p_ior + p_and) == 1.0:
                raise Exception("Probabilities must sum to 1.0")
            else:
                p_structs = [p_seq, p_xor, p_ior, p_and]
        else:
            p_structs = p_conflicts = None
            p_seq = p_xor = p_ior = p_and = p1m1v = p1m2v = p2m1v = p2m2v = None

    if args.dotfile:
        dotfile = args.dotfile.replace(".dot", "")
    else:
        dotfile = None

    # Stats header
    output_header()

    # Object wrapper for the BPMN+V analysis and Z3 conflict detection
    bm = bpmnv_model()

    # Work up to max modelsize (may want to supply a set of model sizes)
    for modelsize in range(minmodelsize, maxmodelsize + 1):
        # collection of arrays of result objects for two models (merged) and their composition
        if bCompos:
            results = (bpmnv_result(), bpmnv_result(), bpmnv_result())
        else:
            results = (bpmnv_result(), )

        # Number of samples
        for cc in range(models):
            for exp_result in results:
                exp_result.extend()  # Add zeros for new experiment

            print("RANDOM MODEL", str(cc))
            sys.stdout.flush()
            # Generate random BPMN model(s)
            if bCompos:
                B = [BPMNV(), BPMNV()]
            else:
                B = [BPMNV()]
            for ii in range(len(B)):
                s, x, i, p = B[ii].gen_rnd(modelsize, p_structs)
                results[ii].seqs[-1] = s
                results[ii].xors[-1] = x
                results[ii].iors[-1] = i
                results[ii].plls[-1] = p

            # Randomly add conflicts (return 3 numbers for model 1, 2, and joint
            p = rnd_conflict(B, conflicts, p_conflicts)
            results[0].cfs[-1] = p
            if bCompos:
                results[1].cfs[-1] = p
            else:
                B = [B[0]]  # Temp bodge in rnd_conflicts
            sout = sys.stdout

            # Output the generated BPMNs
            if DOT_BPMN:
                fn = ["rnd1.bpmn", "rnd2.bpmn"]
                for ii in range(len(B)):
                    dotfile = path.basename(fn[ii]).replace(".bpmn", '_bpmn.dot')
                    epsfile = dotfile.replace(".dot", ".eps")
                    f = open(dotfile, 'w')
                    sys.stdout = f
                    B[ii].print(ptype="dot")
                    f.close()
                    sys.stdout = sout
                    cmd = "dot -Teps -o " + epsfile + ' ' + dotfile
                    system(cmd)

            # Now call to bpmnv_model.py
            bm.process_bpmnvs(B, results, dotfile, b_compose=bCompos, nosuccess=True)

            # Now accumulate the stats

        # Output stats
        print(models, p_seq or '', p_and or '', p_xor or '', p_ior or '', conflicts,
              p1m1v or '', p1m2v or '', p2m1v or '', p2m2v or '', sep=SEP, end=SEP)

        # Output results for this model size
        # - means - merge stats for single models
        if bCompos:
            out_res = results[:-1]
        else:
            out_res = results
        print(np.mean(np.hstack([result.nodes for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.flows for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.acts for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.seqs for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.plls for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.xors for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.iors for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.limits for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.vals for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.models for result in out_res])), sep=SEP, end=SEP)
        print(np.mean(np.hstack([result.secs for result in out_res])), sep=SEP, end=SEP)
        print('', end=SEP)

        # Means - composed
        if bCompos:
            print(np.mean(results[-1].nodes), sep=SEP, end=SEP)
            print(np.mean(results[-1].flows), sep=SEP, end=SEP)
            print(np.mean(results[-1].acts), sep=SEP, end=SEP)
            print(np.mean(results[-1].seqs), sep=SEP, end=SEP)
            print(np.mean(results[-1].plls), sep=SEP, end=SEP)
            print(np.mean(results[-1].xors), sep=SEP, end=SEP)
            print(np.mean(results[-1].iors), sep=SEP, end=SEP)
            print(np.mean(results[-1].limits), sep=SEP, end=SEP)
            print(np.mean(results[-1].vals), sep=SEP, end=SEP)
            print(np.mean(results[-1].models), sep=SEP, end=SEP)
            print(np.mean(results[-1].secs), sep=SEP, end=SEP)
            print('', end=SEP)
        else:
            print(',' * 11, end=SEP)

        # Stdevs - for single models
        print(np.std(np.hstack([result.nodes for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.flows for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.acts for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.seqs for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.plls for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.xors for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.iors for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.limits for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.vals for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.models for result in out_res])), sep=SEP, end=SEP)
        print(np.std(np.hstack([result.secs for result in out_res])), sep=SEP, end=SEP)
        print('', end=SEP)

        # Stdevs - composed
        if bCompos:
            print(np.std(results[-1].nodes), sep=SEP, end=SEP)
            print(np.std(results[-1].flows), sep=SEP, end=SEP)
            print(np.std(results[-1].acts), sep=SEP, end=SEP)
            print(np.std(results[-1].seqs), sep=SEP, end=SEP)
            print(np.std(results[-1].plls), sep=SEP, end=SEP)
            print(np.std(results[-1].xors), sep=SEP, end=SEP)
            print(np.std(results[-1].iors), sep=SEP, end=SEP)
            print(np.std(results[-1].limits), sep=SEP, end=SEP)
            print(np.std(results[-1].vals), sep=SEP, end=SEP)
            print(np.std(results[-1].models), sep=SEP, end=SEP)
            print(np.std(results[-1].secs), sep=SEP, end=SEP)
            print('', end=SEP)
        else:
            print(',' * 11, end=SEP)

        # Stderrs - for single models
        print(st.sem(np.hstack([result.nodes for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.flows for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.acts for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.seqs for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.plls for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.xors for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.iors for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.limits for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.vals for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.models for result in out_res])), sep=SEP, end=SEP)
        print(st.sem(np.hstack([result.secs for result in out_res])), sep=SEP, end=SEP)
        print('', end=SEP)

        # Stderrs - composed
        if bCompos:
            print(st.sem(results[-1].nodes), sep=SEP, end=SEP)
            print(st.sem(results[-1].flows), sep=SEP, end=SEP)
            print(st.sem(results[-1].acts), sep=SEP, end=SEP)
            print(st.sem(results[-1].seqs), sep=SEP, end=SEP)
            print(st.sem(results[-1].plls), sep=SEP, end=SEP)
            print(st.sem(results[-1].xors), sep=SEP, end=SEP)
            print(st.sem(results[-1].iors), sep=SEP, end=SEP)
            print(st.sem(results[-1].limits), sep=SEP, end=SEP)
            print(st.sem(results[-1].vals), sep=SEP, end=SEP)
            print(st.sem(results[-1].models), sep=SEP, end=SEP)
            print(st.sem(results[-1].secs), sep=SEP)
        else:
            print()
