#!/usr/bin/env python
"""Phil Weber 2017-08-02 Read and solve BPMN+V model using the Z3 meta-model.

Returns all solutions (paths) -- fail paths only if -n is supplied.
From a simple ".bpmnv" file format (see BPMNV_TEXT) or Camunda/bpmn.io .bpmn files (or Eclipse?).
Node-only syntax and semantics (possible to extend to mark flows as well).

usage: bpmnv_model.py [-h] [-f MODELFILE] [-f1 MODELFILE1] [-f2 MODELFILE2]
                      [-l T_LIMIT] [-d DOTFILE] [-n] [-x]

Z3 BPMN+V

optional arguments:
  -h, --help            show this help message and exit
  -f MODELFILE, --file MODELFILE
                        File specifying BPMN+V model.
  -f1 MODELFILE1, --file1 MODELFILE1
                        File specifying BPMN+V model (1st of pair to compose).
  -f2 MODELFILE2, --file2 MODELFILE2
                        File specifying BPMN+V model (2nd of pair to compose).
  -l T_LIMIT, --limit T_LIMIT
                        Number of time steps in model (TEMP).
  -d DOTFILE, --dotfile DOTFILE
                        Output to dot (PDF via LaTeX files
  -n, --nosuccess       Ask Z3 for failures only
  -x, --debug

Incomplete list of oustanding issues::
o  Z3 timeout sometimes seems to cause all further calls to fail
o  IOR - how to match converging with diverging?  At present naming convention: "iorname" matches "iorname_".
o  IOR - hardcoded for 2- and 3-way due to problems with the generic approach.
o  This code is unnecessarily monolithic -- modularisation hampered by -
   problems with (some) exec statements if we move out of main.
o  This code is unnecessarily messy with respect to locals and globals.
o  Failure node index rather strange: failonentry=3 => failed node @ 2 => last successful node @ 1.
"""

import os
import sys
import argparse
import time
from pathlib import Path
import subprocess
import z3
import BPMNV
import BPMNVparse
from Z3BPMNV import Z3BPMNV
from bpmnv_result import bpmnv_result

START_REC = "(s)"
END_REC = "(e)"
ACT_REC = "[a]"
XORDIV_REC = "-x<"
XORCONV_REC = ">x-"
PLLDIV_REC = "-+<"
PLLCONV_REC = ">+-"
IORDIV_REC = "-o<"
IORCONV_REC = ">o-"

NULLNODE = 'T'
grammar = BPMNVparse.Parser()

SEP = ", "
VALID_TYPES = ["BOOL", "INT", "REAL", "STRING"]


def output_header():
    """Stats header."""
    print()

    print("Single Models", ',' * 12, end='')
    print("Composed", ',' * 12)

    # Stats
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    print("Nodes", "Flows", "Activities", "Seqs", "Plls", "XORs", "IORs", "Limit", sep=SEP, end=SEP)
    print("Valuations", "Models", "Secs", "Conflicts", sep=SEP, end=SEP)
    print()


class bpmnv_model(object):
    def __init__(self):
        return

    # pylint: disable=too-many-locals
    def read_model_file(self, _modelfile, _bpmn=None, _dot=None):
        """Read and apply basic checks to the model file.

        This is a wrapper to determine type of file and pass on appropriately.

        Basic bespoke BPMN+V specification format expected in text file or on command line.
        TBC: Read from bpmn.io or Eclipse format.

        :param _modelfile: file name.
        :param _bpmn: BPMNV object
        """
        if _modelfile:
            try:
                filename = Path(_modelfile)
                _modelfile = filename.resolve()
            except FileNotFoundError:
                print("Problem opening file '" + _modelfile + "'")
            else:
                fid = open(str(_modelfile), 'r')
                # Test type of file
                line = fid.readline()
                fid.seek(0)
                if line.startswith("<?xml"):
                    _bpmn.tree = _bpmn.parse_bpmn(fid)
                else:
                    _bpmn.tree = _bpmn.parse_text(fid)
        else:
            print("Reading from stdin")
            fid = sys.stdin

        fid.close()

        return


    def process_flows(self, _arc_nodes):
        """Pseudo nodes and dictionaries relating flows to_node arcs.

        The semantics requires Pseudo-nodes for the flows

        :param _arc_nodes: (from, to_node) node pairs defining arcs
        """
        _flow_pseudo_nodes = []  # Simple list to_node merge with "true" nodes
        _flow_nodes = {}  # Dictionary for access to_node source and destination nodes
        _inflows = {}  # Need to_node identify node's inflow / match with its colleagues
        _outflows = {}  # Need to_node identify node's outflow / match with its colleagues
        _fpn_map = {}  # Record constituent nodes, to later output original name
        for fr_to in _arc_nodes:
            fr_node, to_node = fr_to
            ps_node = fr_node + '_' + to_node
            _flow_pseudo_nodes.append(ps_node)
            _fpn_map[ps_node] = (fr_node, to_node)
            _flow_nodes[ps_node] = fr_to

            if fr_node in _outflows.keys():
                _outflows[fr_node].append(ps_node)
            else:
                _outflows[fr_node] = [ps_node]

            if to_node in _inflows.keys():
                _inflows[to_node].append(ps_node)
            else:
                _inflows[to_node] = [ps_node]

        return _flow_pseudo_nodes, _fpn_map, _flow_nodes, _inflows, _outflows


    def gen_image_doc(self, basefile, n_models, caption=None, output=None):
        """Append eps files into pdf.

        :param basefile: name template for figures
        :param n_models: how many figures to look for (model x data)
        :param caption: summary of results, for image caption
        :param output: optional array of text (results, failures) to output with image
        """
        texfile = basefile + ".tex"
        with open(texfile, 'w') as fid:
            print(r"\documentclass[a4paper, 10pt]{article}", file=fid)

            # Allows use of png graphics, doc as .pdf, not .ps
            print(r"\usepackage{graphicx}", file=fid)
            print(r"\usepackage{float}", file=fid)  # force float placement
            print(r"\DeclareGraphicsExtensions{.eps}", file=fid)

            # This line gives us wide pages with narrow margins.
            print(r"\usepackage[top=0.5in, bottom=1in, left=0.5in, right=0.5in]{geometry}", file=fid)

            # These lines give non-indented paragraphs, with space between them.
            print(r"\setlength{\parindent}{0pt}", file=fid)
            print(r"\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}", file=fid)

            print(r"\begin{document}", file=fid)

            for fig in range(n_models):
                figbase = basefile + str(fig)
                _epsfile = figbase + ".eps"
                if os.path.isfile(_epsfile):
                    print(r"\begin{figure}[H]", file=fid)
                    print(r"  \centering", file=fid)
                    print(r"  \includegraphics[width=\linewidth]{" + figbase + "}", file=fid)

                    if caption[fig]:
                        print(r"  \caption{", end='', file=fid)
                        print(caption[fig], end='', file=fid)
                        print("}", file=fid)
                    else:
                        print(r"  \caption{semantics: model/data valuation " + str(fig) + "}", file=fid)
                    print(r"\end{figure}", file=fid)
                else:
                    print("Missing", _epsfile)
                if output[fig]:
                    print(r"\begin{verbatim}", file=fid)
                    print(output[fig], file=fid)
                    print(r"\end{verbatim}", file=fid)

            print(r"\end{document}", file=fid)

        try:
            subprocess.run(["latex", basefile], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=10)
        except subprocess.TimeoutExpired:
            print("LaTeX timed out, probably LaTeX code error.")

        subprocess.run(["dvips", "-t", "a4", "-t", "portrait", "-f", basefile + ".dvi", "-o", basefile + ".ps"],
                       stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        subprocess.run(["gs", "-dSAFER", "-dNOPAUSE", "-dBATCH", "-sDEVICE=pdfwrite", "-sPAPERSIZE=a4",
                  "-dPDFSETTINGS=/printer", "-dCompatibilityLevel=1.4", "-dMaxSubsetPct=100",
                  "-dSubsetFonts=true", "-dEmbedAllFonts=true",
                  "-sTitle=" + basefile, "-sOutputFile=" + basefile + ".pdf", basefile + ".ps"],
                       stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


    def get_enablings(self, z3bpmnv, bpmnv, resultpath, fpn_map, start_nodes, outflows, valuations,
                      output=True):
        """Obtain dictionary of all enablings (over time).

        :param z3bpmnv: Z3 BPMNV meta-object
        :param bpmnv: BPMN+V model
        :param resultpath: path returned by Z3
        :param fpn_map: map flows to (start, end) nodes
        :param start_nodes:
        :param outflows:
        :param valuations: to get type of variable (this is rather messy)
        :param output: print the enablings.
        """
        enablings = None
        try:
            enablings = z3bpmnv.get_enablings_qfuflra(len(resultpath), valuations)
        except:
            # for d in s.model().decls(): print(d, "=", s.model().get_interp(d))
            raise

        if enablings:
            # for en, enn in enablings.items(): print(en, ':', enn)
            for node in resultpath:
                if node != NULLNODE:  # Ignore terminator
                    if node in start_nodes:
                        nodeM = node + "_M"
                        nM_mapped = bpmnv.obj_map[node].name + "_M"
                        enabling = enablings[nodeM]
                        if output:
                            print("{:8}: {}".format(nM_mapped, [int(en) for en in enabling]))
                    if node in outflows:
                        for flow in outflows[node]:
                            flow_fr = bpmnv.obj_map[fpn_map[flow][0]].name \
                                      or bpmnv.obj_map[fpn_map[flow][0]].oid
                            flow_to = bpmnv.obj_map[fpn_map[flow][1]].name or \
                                      bpmnv.obj_map[fpn_map[flow][1]].oid
                            flow_mapped = flow_fr + '_' + flow_to
                            flow += "_M"
                            if flow in enablings:
                                enabling = enablings[flow]
                                if output:
                                    print("{:8}: {}".format(flow_mapped,
                                                            [int(en) for en in enabling]))
        return enablings

    def output_failwhy(self, z3bpmnv, bpmnv, valuation, resultpath, rp_names, result_text, fail,
                       valuations):
        """Output information on reason for failure.
           ??? why failed at fail-2?

        :param z3bpmnv: parameters imported from process_bpmnvs
        :param bpmnv
        :param valuation
        :param resultpath
        :param rp_names
        :param result_text
        :param fail
        :param valuations

        :return details of failure for use elsewhere

        Check which activity failed, its guard, variables involved, which caused the failure,
        whether due to initial value or another activity which changed it.
        """
        node = bpmnv.obj_map[resultpath[len(fail) - 2]]
        if hasattr(node, "guard") and node.guard:
            _guard = node.guard.strip()
        else:
            print("Node " + node.name + " has no guard")
            return None

        rs = "failed to execute: " + rp_names[len(fail) - 2] + " (guard: \"" + _guard + "\")"
        print(rs)
        result_text[-1] += "\n" + rs

        # get vars by parsing guards
        # parse nowvar through guard
        guards = _guard.split(',')

        # Identify which variables caused the conflict by reassembling each and parsing
        node_change_raw = ii_varchange = None
        b_firstfail = True
        for guard in guards:
            (failvar, op, val, neg) = \
                grammar.aguard.parse(guard, bpmnv.vardefs)

            # This is a bodge: look at fixing.
            if op in [None, ''] and val in [None, False, '']:
                val = ''

            initval = valuation[failvar]["value"]
            failvarpath = z3bpmnv.get_var_array_qfuflra(len(resultpath), failvar, valuations)
            nowval = failvarpath[len(fail) - 2]

            guard_str = neg + ' ' + str(nowval) + \
                ' ' + op + ' ' + str(val)

            b = eval(guard_str)  # pylint:disable=eval-used

            if not b:  # guard failed
                if b_firstfail:
                    rs = "  due to: "
                    b_firstfail = False
                else:
                    rs = "          "
                if nowval != initval:
                    rs += "var \"" + failvar + "\" (init: " + str(initval) + ", now: " + str(nowval) + ')'
                    result_text[-1] += "\n" + rs

                    # NB entries in failvarpath changed to Python types from Z3 types in get_var_array_qfuflra
                    ii_varchange = [ii for ii in range(1, len(failvarpath))
                                    if failvarpath[ii] != failvarpath[ii - 1]][0] - 1
                    node_change = rp_names[ii_varchange]
                    node_change_raw = resultpath[ii_varchange]

                    by_mod = None  # This should always be changed, unless something went wrong
                    if hasattr(bpmnv.obj_map[resultpath[ii_varchange]], "data"):
                        if bpmnv.obj_map[resultpath[ii_varchange]].data:
                            by_mod = bpmnv.obj_map[resultpath[ii_varchange]].data.strip()

                    rs = "  changed at " + str(ii_varchange) + " by " + node_change + " (\"" \
                         + str(by_mod) + "\")"
                    print(rs)
                    result_text[-1] += "\n" + rs

                else:
                    rs += "init val of var \"" + str(failvar) + "\" (" + str(initval) + ')'
                    print(rs)
                    result_text[-1] += "\n" + rs

        return len(fail) - 2, resultpath[len(fail) - 2], failvar, ii_varchange, node_change_raw

    def process_bpmnvs(self, bpmnvs, results=None, dotfile=None, b_compose=False,
                       modelfiles=None, arg_t_limit=None, nosuccess=False):
        """Process one or more BPMN+V models for inconsistency/conflict.

        :param bpmnvs: Array of BPMN+V models.
        :param results: (opt) tuple of dictionaries to be populated with stats (m1, m2, ..., mC).
        :param modelfiles: Optional array of file names.
        :param b_compose: Optional: >1 model; compose them after individual analysis.
        :param arg_t_limit: Opt: step limit supplied.
        :param nosuccess: do not ask Z3 for successful models (i.e. return conflicts only)
        :return:
        """
        # Analysis for single or both models, plus composition.
        if b_compose:
            bpmnvs.append(BPMNV.BPMNV())

        ii_dot = 0  # counter for dot files
        result_text, result_headline = [], []
        for ii_bpmnv in range(len(bpmnvs)):
            bpmnv = bpmnvs[ii_bpmnv]
            if b_compose and ii_bpmnv == len(bpmnvs)-1:  # Time to do the composition
                bpmnv.compose(bpmnvs[:-1])  # compose from the two supplied BPMNVs

            start_nodes = [bpmnv.s.oid]
            end_nodes = [bpmnv.e.oid]
            act_nodes = [aa.oid for aa in bpmnv.Act]
            xordiv_nodes = [xx.oid for xx in bpmnv.XOR if xx.direction == BPMNV.DIVERGING]
            xorconv_nodes = [xx.oid for xx in bpmnv.XOR if xx.direction == BPMNV.CONVERGING]
            plldiv_nodes = [pp.oid for pp in bpmnv.AND if pp.direction == BPMNV.DIVERGING]
            pllconv_nodes = [pp.oid for pp in bpmnv.AND if pp.direction == BPMNV.CONVERGING]
            iordiv_nodes = [ii.oid for ii in bpmnv.IOR if ii.direction == BPMNV.DIVERGING]
            iorconv_nodes = [ii.oid for ii in bpmnv.IOR if ii.direction == BPMNV.CONVERGING]
            arc_nodes = [(ff.fr_node.oid, ff.to_node.oid) for ff in bpmnv.F]

            nodes = start_nodes + end_nodes \
                + act_nodes \
                + xordiv_nodes + xorconv_nodes \
                + plldiv_nodes + pllconv_nodes \
                + iordiv_nodes + iorconv_nodes

            # Create pseudo nodes and relate flows to nodes (for the semantics)
            flow_pseudo_nodes, fpn_map, flow_nodes, inflows, outflows = self.process_flows(arc_nodes)

            z3bpmnv = Z3BPMNV()  # Create model with the default BPMN+V syntax and semantics.
            solver = z3bpmnv.solver  # Solver

            # CONFIGURE MODEL INSTANCE -- syntax
            m_syntax = []

            # Nodes
            T = z3.Const("T", z3bpmnv.node)  # For terminator node

            # Should be boilerplate from here
            # Create Z3 nodes for BPMN+V nodes only (not for flows)
            for nn in nodes:
                # pylint: disable=exec-used
                exec(nn + " = z3.Const('" + nn + "', z3bpmnv.node)")

            # BPMN+V Nodes (including the terminator/null node) are distinct
            # NB: it seems important (why?) that T is last.
            s_distinct = "m_syntax.append(z3.Distinct(" + ','.join(nodes) + ",T))"
            # pylint: disable=exec-used
            # pylint: disable=undefined-variable
            exec(s_distinct)

            # Add the syntax constraints
            solver.add(m_syntax)

            # Check the model syntax and output if required
            if not b_compose or ii_bpmnv != len(bpmnvs)-1:
                if modelfiles:
                    print(os.path.basename(modelfiles[ii_bpmnv]))
                else:
                    print("INPUT BPMN+V", str(ii_bpmnv))
            else:
                print("Composition:")

            # Rudimentary estimate of t_limit (longest path problem is NP hard, but over-estimate
            # seems to have very little effect):
            if not arg_t_limit:
                t_limit = max(len(nodes) + 1, 10)
                print("t_limit not specified: Setting to", t_limit)
            else:
                t_limit = arg_t_limit

            if len(nodes) > 0: print("  Nodes:", len(nodes))
            if len(flow_nodes) > 0: print("  Flows:", len(flow_nodes))
            if len(act_nodes) > 0: print("  Activities:", len(act_nodes))
            if len(plldiv_nodes) > 0: print("  Parallel Structures:", len(plldiv_nodes))
            if len(xordiv_nodes) > 0: print("  XOR Structures:", len(xordiv_nodes))
            if len(iordiv_nodes) > 0: print("  IOR Structures:", len(iordiv_nodes))

            if results:
                results[ii_bpmnv].limits[-1] = t_limit
                results[ii_bpmnv].nodes[-1] = len(nodes)
                results[ii_bpmnv].flows[-1] = len(flow_nodes)
                results[ii_bpmnv].acts[-1] = len(act_nodes)
                results[ii_bpmnv].plls[-1] = len(plldiv_nodes)
                results[ii_bpmnv].xors[-1] = len(xordiv_nodes)
                results[ii_bpmnv].iors[-1] = len(iordiv_nodes)

            print("Syntax check ... ", end='')

            timei = time.time()
            r = solver.check()  # Checked at start of loop
            timeo = time.time()
            timed = timeo - timei
            results[ii_bpmnv].secs[-1] += timed
            if r == z3.unsat:
                print("failed; aborting (1).")
                for aa in solver.assertions():
                    print(aa)
                    exit(1)
            else:
                print("passed (" + "{:.2f}s".format(timed) + "); solving semantics ... ")

            # CONFIGURE MODEL INSTANCE -- semantics
            m_semantics = []

            # Nodes
            iii = z3.Int('iii')  # For quantifiers
            jjj = z3.Int('jjj')  # For quantifiers - for adding constraints for no duplicate conflicts

            # Create counters for ior structures
            if iordiv_nodes:
                if len(iordiv_nodes) == 1:
                    s_ior = "ior_" + iordiv_nodes[0] + " = z3.Int('ior_" + iordiv_nodes[0] + "')"
                else:
                    s_ior = "ior_" + ", ior_".join(iordiv_nodes) + " = z3.Ints('ior_" \
                            + ' ior_'.join(iordiv_nodes) + "')"
                # pylint: disable=exec-used
                exec(s_ior)

            # Path (sequence of nodes executed)
            path = z3.Array('path', z3.IntSort(), z3bpmnv.node)  # list of nodes
            apath = z3.Array('apath', z3.IntSort(), z3bpmnv.node)  # list of activity nodes only
            apath_ind_diff = z3.Array('apath_ind_diff', z3.IntSort(), z3.IntSort())  # index difference path-->apath

            # Marking array for start node and each flow node
            M_nodes = []  # Marking array names
            for nn in start_nodes + flow_pseudo_nodes:
                arr = nn + "_M"
                M_nodes.append(arr)
                # pylint: disable=exec-used
                exec(arr + " = z3.Array('" + arr + "', z3.IntSort(), z3.BoolSort())")

            # Arrays for tracking data
            for var, defn in bpmnv.vardefs.items():
                if defn["type"].upper() in VALID_TYPES:
                    # NB So far only tested Bool and Int
                    varsort = defn["type"].capitalize()
                    s_data = var + " = z3.Array('" + var + "', z3.IntSort(), z3." + varsort + "Sort())"
                    # pylint: disable=exec-used
                    exec(s_data)
                else:
                    print("Ignoring invalid var type", defn["type"])
            # Array to track failure
            s_data = "failonentry = z3.Array('failonentry', z3.IntSort(), z3.BoolSort())"
            # pylint: disable=exec-used
            exec(s_data)

            # Initial marking
            s_initial = z3bpmnv.create_z3_initial_marking(
                "mark0", zip(nodes + flow_pseudo_nodes, M_nodes), start_nodes
            )
            # pylint: disable=exec-used
            exec(s_initial)
            # pylint: disable=undefined-variable
            m_semantics += locals()["mark0"]

            # Initial data - moved to just before execution so we can explore all values.

            # Marking iteration
            s_marking = z3bpmnv.create_z3_marking_iteration(
                "mark", zip(start_nodes + flow_pseudo_nodes, M_nodes),
                start_nodes, xordiv_nodes, xorconv_nodes, pllconv_nodes,
                iordiv_nodes, iorconv_nodes, flow_nodes,
                inflows, outflows,
                t_limit, bpmnv.IOR
            )
            # print(s_marking)
            try:
                # pylint: disable=exec-used
                exec(s_marking)
            except:
                print(s_marking)
                raise
            m_semantics += locals()["mark"]

            # Firing
            s_firing = z3bpmnv.create_z3_firing(
                "fired", nodes,
                zip(start_nodes + flow_pseudo_nodes, M_nodes), M_nodes,
                start_nodes, end_nodes, act_nodes,
                xordiv_nodes, xorconv_nodes,
                plldiv_nodes,
                iordiv_nodes, iorconv_nodes,
                flow_pseudo_nodes, inflows, outflows,
                t_limit, bpmnv, nosuccess
            )
            # print(s_firing)
            try:
                # pylint: disable=exec-used
                exec(s_firing)
            except:
                print(s_firing)
                raise
            # pylint: disable=undefined-variable
            m_semantics += locals()["fired"]

            solver.add(m_semantics)

            valuations = bpmnv.cover_tokens()
            solver.push()  # Semantics without the data settings

            # Solve the model for each set of data valuations
            ii_data = 0
            if not valuations:
                valuations = [None]

            for valuation in valuations:
                if b_compose and ii_bpmnv != len(bpmnvs) - 1:
                    break
                solver.pop()  # Semantics without the data settings
                solver.push()  # Semantics without the data settings

                # Initial data
                s_data = z3bpmnv.create_z3_initial_data("data0", valuation)
                # pylint: disable=exec-used
                exec(s_data)
                # pylint: disable=undefined-variable
                solver.add(locals()["data0"])

                print("data", str(ii_data) + ':', end=' ')
                if results:
                    results[ii_bpmnv].vals[-1] += 1

                if not valuation:
                    print("None or N/A")
                else:
                    print()
                    for var in valuation:
                        print(var + ':', valuation[var]["value"], '(' + valuation[var]["type"] + ')')
                print()

                # Solve the model for the first result.

                # Sometimes (?) the timeout causes an exception.  Wrapping this for now but it needs further
                # investigation lest something more serious is going on.
                timei = time.time()
                try:
                    r = solver.check()  # Checked at start of loop
                except z3.z3types.Z3Exception as e:
                    print("failed due to ", e.value)
                    if e.value == b"canceled":
                        print("z3 CANCELLATION RAISED EXCEPTION")
                        r = None  # Not z3.sat
                    else:
                        raise
                timeo = time.time()
                timed = timeo - timei
                results[ii_bpmnv].secs[-1] += timed
                ii_model = 0
                if r != z3.sat:
                    if nosuccess:
                        print("(no_solutions_found (assuming no conflicts)", end=' ')
                    else:
                        print("(failed - no solutions found)", end=' ')
                    print("Time: {:.6f}s".format(timed) + " (step limit " + str(t_limit) + ')')

                constr = []

                resultpatha_old = []
                assertions_old = []
                model_old = []
                while r == z3.sat:
                    strnodes = z3bpmnv.get_strnodes()
                    resultpath = z3bpmnv.get_path_qfuflra(t_limit, nodes, "path")
                    actslen = len([rr for rr in resultpath if rr in act_nodes])
                    resultpatha = z3bpmnv.get_path_qfuflra(actslen, nodes, "apath")
                    if resultpatha == resultpatha_old:  # tracking duplicates
                        print("*** DUPLICATE ***")
                        print("ASSERTIONS OLD")
                        for aa in assertions_old: print(aa)
                        print("ASSERTIONS NEW")
                        for aa in solver.assertions(): print(aa)
                        print("MODELS OLD")
                        for mm in model_old: print(mm, '=', model_old[mm])
                        print("MODELS NEW")
                        for mm in solver.model(): print(mm, '=', solver.model()[mm])
                        print("*** DUPLICATE ***")
                        input()
                    else:
                        resultpatha_old = resultpatha.copy()
                        assertions_old = solver.assertions()
                        model_old = solver.model()

                    rp_names = [bpmnv.obj_map[o].name or bpmnv.obj_map[o].oid for o in resultpath]
                    rp_namesa = [bpmnv.obj_map[o].name or bpmnv.obj_map[o].oid for o in resultpatha]
                    rs = "model " + str(ii_model) + ": " + str(r) + ' '
                    print(rs, end='')
                    result_headline.append(rs)
                    if results:
                        results[ii_bpmnv].models[-1] += 1

                    fail = z3bpmnv.get_var_array_qfuflra(len(resultpath), "failonentry", valuations)

                    # fail is useless for interpretation outside of the length of path.
                    bfail = False
                    if resultpath[-1] in end_nodes:
                        rs = "(success) "
                    elif len(resultpath) < t_limit:
                        rs = "(failed) "
                        bfail = True
                    else:
                        rs = "(success - incompletely explored) "
                    print(rs, end='')
                    result_headline[-1] += rs

                    rs = "Time: {:.6f}s".format(timed) + " (step limit " + str(t_limit) + ')'
                    print(rs)
                    result_headline[-1] += rs

                    rs = ' --> '.join(rp_namesa)  # Activities only
                    print(rs)
                    result_text.append(rs)

                    # Extract reason for failure
                    changeat = change_node = None  # Check these for fail due to init or otherwise
                    if resultpath[-1] not in end_nodes:
                        failat, failnode, failvar, changeat, changenode = \
                            self.output_failwhy(z3bpmnv, bpmnv, valuation, resultpath, rp_names,
                                                result_text, fail, valuations)

                    # Construct a constraint to avoid returning duplicate conflict (DRAFT)
                    if bfail:
                        if changeat == changenode == None:
                            constr = """
norepeat = z3.Not(z3.Exists(iii,
                            z3.And(iii > 0, iii <= """ + str(t_limit) + """,
                            failonentry[iii],
                            path[iii-1] == """ + failnode + """,
                            """ + failvar + """[iii-1] == """ + failvar + """[0]
))),"""
                        else:
                            constr = """
norepeat = z3.Not(z3.Exists([iii, jjj],
                            z3.And(iii > 0, iii <= """ + str(t_limit) + """,
                            failonentry[iii],
                            path[iii-1] == """ + failnode + """,
                            path[jjj] == """ + changenode + """,
                            """ + failvar + """[jjj] == """ + failvar + """[jjj-1]
))),"""

                        # print(constr)
                        exec(constr)
                        try:
                            True
                            solver.add(locals()["norepeat"])
                        except:
                            for aa in solver.assertions(): print(aa)
                            raise

                    enablings = self.get_enablings(
                        z3bpmnv, bpmnv, resultpath, fpn_map, start_nodes, outflows, valuations,
                        output=False)

                    if dotfile:
                        # Output with path marked
                        en_dotfile = dotfile + str(ii_dot)
                        z3bpmnv.output_dot(solver.model(), nodes,
                                           act_nodes, start_nodes, end_nodes,
                                           xordiv_nodes, xorconv_nodes,
                                           plldiv_nodes, pllconv_nodes,
                                           iordiv_nodes, iorconv_nodes,
                                           arc_nodes, bpmnv, valuation,
                                           en_dotfile, resultpath, enablings)
                        ii_dot += 1

                        epsfile = en_dotfile + ".eps"
                        cmd = "dot -Teps -o " + epsfile + ' ' + en_dotfile + ".dot"
                        subprocess.run(cmd.split())

                    ii_model += 1

                    # Obtain next result - using activity path only
                    constr = "pathconstr = z3.Not(z3.And("
                    for ii, nn in enumerate(resultpatha):
                        if nn != NULLNODE:
                            node = strnodes[nn]
                            constr += "apath[" + str(ii) + "] == " + str(node) + ", "
                    constr += "))"
                    # print(constr)

                    # pylint: disable=exec-used
                    exec(constr)
                    # pylint: disable=undefined-variable
                    solver.add(locals()["pathconstr"])

                    timei = time.time()
                    try:
                        r = solver.check()  # Checked at start of loop
                    except z3.z3types.Z3Exception as e:
                        print("failed due to ", e.value)
                        if e.value == b"canceled":
                            print("z3 CANCELLATION RAISED EXCEPTION")
                            r = None  # Not z3.sat
                        else:
                            raise
                    timeo = time.time()
                    timed = timeo - timei
                    results[ii_bpmnv].secs[-1] += timed

                    print()
                    sys.stdout.flush()
                ii_data += 1

        # Compile images into PDF
        if dotfile:
            self.gen_image_doc(dotfile, ii_dot, result_headline, result_text)


if __name__ == "__main__":
    # pylint: disable=invalid-name
    # Problems with the way we build exec statements containing Z3 code mean __main__ is
    # large; do not wish to capitalise all these "vars/constants" as pylint would like.
    parser = argparse.ArgumentParser(description="Z3 BPMN+V")
    parser.add_argument('-f', '--file', dest="modelfile", default=None,
                        help="File specifying BPMN+V model.")
    parser.add_argument('-f1', '--file1', dest="modelfile1", default=None,
                        help="File specifying BPMN+V model (1st of pair to compose).")
    parser.add_argument('-f2', '--file2', dest="modelfile2", default=None,
                        help="File specifying BPMN+V model (2nd of pair to compose).")
    parser.add_argument('-l', '--limit', type=int, dest="t_limit", default=None,
                        help="Number of time steps in model (TEMP).")
    parser.add_argument('-d', '--dotfile', dest="dotfile",
                        help="Output to dot file", default=None)
    parser.add_argument('-n', '--nosuccess', dest="nosuccess", action="store_true",
                        help="Ask Z3 for failures only", default=False)
    parser.add_argument('-x', '--debug', dest="debug", action="store_true", default=False)

    args = parser.parse_args()
    DEBUG = False
    DEBUG = args.debug
    nosuccess = False
    nosuccess = args.nosuccess
    arg_t_limit = args.t_limit  # If not specified, roughly estimated later

    # Check files specified for single analysis or composition
    modelfile = args.modelfile
    modelfile1 = args.modelfile1
    modelfile2 = args.modelfile2

    # Check whether we are doing composition, and check file names are appropriate
    if modelfile and not (modelfile1 or modelfile2):
        b_compose = False
        modelfiles = [modelfile]
    else:
        b_compose = True
        modelfiles = [modelfile1, modelfile2]

    if modelfile and (modelfile1 or modelfile2):
        print("Please specify either -f (analyse single file) or both -f1 and -f2 (composition and analysis).")
        exit(1)
    if not modelfile and (not modelfile1 or not modelfile2):
        print("Please specify both -f1 and -f2 for composition and analysis.")
        exit(1)

    if args.dotfile:
        dotfile = args.dotfile.replace(".dot", "")
    else:
        dotfile = None

    # Process the input file(s) (will determine if text file or BPMN XML)
    if b_compose:
        bpmnvs = [BPMNV.BPMNV(), BPMNV.BPMNV()]  # two models and their composition
        results = (bpmnv_result(), bpmnv_result(), bpmnv_result())
    else:
        bpmnvs = [BPMNV.BPMNV()]
        results = (bpmnv_result(),)

    # Add zeros for new experiment
    for exp_result in results:
        exp_result.extend()

    dot = None
    bm = bpmnv_model()

    # Read the model files: last entry in bpmnvs is for the composed model.
    for ii in range(len(bpmnvs)):
        _bpmnv = bpmnvs[ii]
        bm.read_model_file(modelfiles[ii], _bpmnv, _dot=dot)

    bm.process_bpmnvs(bpmnvs, results, dotfile, b_compose, modelfiles, arg_t_limit, nosuccess)
    out_res = results[-1]

    output_header()

    for out_res in results:
        print(out_res.nodes[0], sep=SEP, end=SEP)
        print(out_res.flows[0], sep=SEP, end=SEP)
        print(out_res.acts[0], sep=SEP, end=SEP)
        print(out_res.seqs[0], sep=SEP, end=SEP)
        print(out_res.plls[0], sep=SEP, end=SEP)
        print(out_res.xors[0], sep=SEP, end=SEP)
        print(out_res.iors[0], sep=SEP, end=SEP)
        print(out_res.limits[0], sep=SEP, end=SEP)
        print(out_res.vals[0], sep=SEP, end=SEP)
        print(out_res.models[0], sep=SEP, end=SEP)
        print(out_res.secs[0], sep=SEP, end=SEP)

        print()

