/* Phil Weber 2017-04-25 Implement BPMN+V for a test IOR model.
*/

open util/ordering[State] as states
open BPMNV  // Syntax of BPMN+V models

// Syntax of a specific model - arcs created by default from these
one sig P0 extends Start { }
one sig Pe extends End { }
one sig A, B, C extends Activity { }
one sig i0 extends IorDiv { }
one sig i0_ extends IorConv { }
fact model {
  P0.next = A
  A.next = i0
  i0.next = B + C
  (B + C).next = i0_
  i0_.next = Pe
  no Pe.next + XorDiv + XorConv + PllDiv + PllConv
}

pred test {
  states/first.markedNode.Token = P0 // and states/last.markedNode.Token = Pe
  no states/first.markedFlow // and no states/last.markedFlow  // else exclude models!
}
// need to count the no. or arcs, but any way to avoid large no. of states?
// run test for 9  but 11 State
run test for 7 Node, 7 Flow, 3 Activity, 1 IorDiv, 1 IorConv, 2 Token, 2 List, 1 Elem,
  0 PllDiv, 0 PllConv, 0 XorDiv, 0 XorConv, 13 State
