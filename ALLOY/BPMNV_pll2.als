/* Phil Weber 2017-04-25 Implement BPMN+V for a test parallel model.

Question: do we have to know the minimum #states for scope?

  o  Issue: duplicate models where arcs to B and C are reversed in names.
      Behaviour is the same.  Implies some under-specification.
*/

open BPMNV  // Syntax of BPMN+V models

// Syntax of a specific model - arcs created by default from these
one sig P0 extends Start { }
one sig Pe extends End { }
one sig A, B, C extends Activity { }
one sig f0 extends PllDiv { }  // parallel diverge
one sig j0 extends PllConv { } // parallel join
fact model {
  P0.next = A
  A.next = f0
  f0.next = B + C
  B.next = j0
  C.next = j0
  j0.next = Pe
  no Pe.next
  no XorDiv + XorConv + IorDiv + IorConv
}

pred test {
  BPMNV/states/first.markedNode.Token = P0 // and BPMNV/states/last.markedNode.Token = Pe
}
// need to count the no. or arcs, but any way to avoid large no. of states?
// run test for 9  but 11 State
run test for 7 Node, 7 Flow, 3 Activity, 0 IorDiv, 0 IorConv, 2 Token, 2 List, 1 Elem,
  1 PllDiv, 1 PllConv, 0 XorDiv, 0 XorConv, 13 State
