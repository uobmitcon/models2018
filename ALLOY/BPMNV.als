/* Phil Weber 2017-04-27 BPMN+V syntax and semantics.

Implemented for sequence, XOR, Parallel, IOR constructs.
Initially developed on test fragments, tested for nesting and sequence depth/length 2.
Complicated by allowing State to be in a combination of activities and (on) arcs.
*/

open util/ordering[State] as states
open stack  // linked list (for managing IOR deciding convergence)

// SYNTAX: abstract: to be extended by individual models
abstract sig Node {
  next: set Node,  // specified to instantiate connections in the model
  pre: set Flow,  // pre and post are required for the semantics
  post: set Flow
}
abstract sig
  Start, End, Activity, PllDiv, PllConv, XorDiv, XorConv, IorDiv, IorConv
  extends Node { }

// Node.next fully specifies the syntactic connections between nodes;
// Flows enable the semantics.
sig Flow {
  pre: one Node,
  post: one Node
}

// Syntactic constraints
fact syntax {
  one Start
  one End
  all s: Start | one s.post and no s.pre  // flow specifications
  all e: End | no e.post and one e.pre
  all a: Activity | one a.post and one a.pre
  all d: (PllDiv + XorDiv + IorDiv) | some d.post and one d.pre and #d.post > 1
  all c: (PllConv + XorConv + IorConv) | one c.post and some c.pre and #c.pre > 1

  // flow arcs for each Node.next specification
  all n: Node | all n': n.next | one f: Flow | f.pre = n and f.post = n'

  // Using Node.next, specify pre and post for both nodes and flows, to enable the semantics
  all f: Flow, n: Node {
    f.pre = n <=> f in n.post
    f.post = n <=> f in n.pre
  }
  all n: Node {
    n.post.post in n.next
    n.pre.pre in next.n
  }
}

// Semantics
abstract sig Token {
  iors: one List  // IOR nesting - match with state
}

abstract sig State {  // This is acting as patient
  markedNode: set Node->Token,  // do these work?
  markedFlow: set Flow->Token,  // must be a marking
  visMarkedFlow: set Node->Node,  // for visualisation only
}

// Enter Node (all except XorConv, IorConv) - all in Flows must be enabled
pred stepIn [s, s': State, n: Node] {
  n in (Node - XorConv - IorConv)  // these require special treatment
  let f = n.pre & s.(markedFlow.Token) {
    f = n.pre  // enabled in flows
    // move token - this will need to be extended to change data
    let t = f.(s.markedFlow) { // , i = t.iors {
      s'.markedFlow = s.markedFlow - (f->t)  // remove in-flows from marking
      s'.markedNode = s.markedNode + (n->t)
      s'.visMarkedFlow = s.visMarkedFlow - f.pre->f.post  // remove arc visualisation
    }
  }
}

// Enter XorConv (req: only one input arc marked) or IorConv (some)
pred stepInXorIorConv [s, s': State, n: Node] {
  n in XorConv + IorConv // Only valid for these types of Node
  let f = n.pre & s.(markedFlow.Token), t = f.(s.markedFlow) { // XXX
    n in XorConv => one f else some f  // XOR: only one enabled input arc
    // prevent entry (IorConv) if not the same number of arcs enabled as on exit
    // from corresponding (?) IorDiv - should account for nested IOR.
    n in IorConv => {
      one t': Token | one i': List {
        let i = t.iors {
          #t = 1
          some i.elems  // #i.elems > 0
          #f = i.head.value  // check stored branching value and remove it from the stack
          popElem[i, i']
          t'.iors = i'
          s'.markedNode = s.markedNode + (n->t')  // add Node n to marking
        }
      }
    } else {
      s'.markedNode = s.markedNode + (n->t)  // add Node n to marking
    }
    s'.markedFlow = s.markedFlow - (f->t) // remove in-flows from marking
    s'.visMarkedFlow = s.visMarkedFlow - f.pre->f.post  // remove arc visualisation
  }
}

// End Activity (all except XorDiv, IorDiv) - all out flows must be enabled
pred stepOut [s, s': State, n: Node] {
  n in (Node - XorDiv - IorDiv)  // these require special treatment
  // no n.pre & s.(markedFlow.Token)  // guard against unfinished PllConv (?)
  n in End => {
    s'.markedNode = s.markedNode  // else we need to specify exact run scope
    s'.markedFlow = s.markedFlow  // irrelevant?
    s'.visMarkedFlow = s.visMarkedFlow
  }
  else let f = n.post, t = n.(s.markedNode) {
    // move token - will need to be modified if changing data on exit
    s'.markedFlow = s.markedFlow + (f->t)  // add out-flows
    s'.markedNode = s.markedNode - (n->t)  // remove node
    s'.visMarkedFlow = s.visMarkedFlow + f.pre->f.post  // add arc for visualisation
  }
}

// End XorDiv (requires only one output Flow to be marked)
pred stepOutXorIorDiv [s, s': State, n: Node] {
  n in (XorDiv + IorDiv)
  n in End => {
    s'.markedNode = s.markedNode  // else we need to specify exact run scope
    s'.markedFlow = s.markedFlow  // XXX irrelevant?
    s'.visMarkedFlow = s.visMarkedFlow
  }
  else let f = n.post & s'.(markedFlow.Token), t = n.(s.markedNode), i = t.iors {  // XXX
    // move token - push the number of enabled arcs
    n in XorDiv => one f else some f  // XOR: only one enabled input arc
    n in IorDiv => {
      one t': Token | one i': List | one e: Elem {
        e.value = #f
        pushElem[i, i', e]
        t'.iors = i'
        s'.markedFlow = s.markedFlow + (f->t')  // add out-flow to marking
      }
    } else {
      s'.markedFlow = s.markedFlow + (f->t)  // add out-flow to marking
    }
    s'.markedNode = s.markedNode - (n->t)  // remove Node n from marking
    s'.visMarkedFlow = s.visMarkedFlow + f.pre->f.post  // add arc for visualisation
  }
}

pred init [s: State] {
  no s.markedFlow
  one t: Token {
    #t.iors.elems = 0  // not in any IORs
    s.markedNode = Start->t
  }
  no s.visMarkedFlow
}

// If we're in an activity, we can step out of it (assuming exit conditions met).
fact traces {
  init [states/first]
  // some m because not all markings allow moving to this next state.  Maybe one?
  all s: State - states/last | some m: s.markedNode.Token + s.markedFlow.Token {
      let s' = s.next {
        (m in (Node - XorDiv - IorDiv) and stepOut[s, s', m]) || 
        (m in (XorDiv + IorDiv) and stepOutXorIorDiv[s, s', m]) || 
        (m in Flow and stepIn[s, s', m.post]) ||
        (m in Flow and stepInXorIorConv[s, s', m.post])
      }
  }
  all l: List | some t: Token | t.iors = l  // no floating lists
  all t: Token | some s: State | t in (Node+Flow).(s.(markedNode + markedFlow))
}
