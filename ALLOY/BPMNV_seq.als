/* Phil Weber 2017-04-25 Implement BPMN+V for a test sequence model.

Question: do we have to know the minimum #states for scope?
*/

open BPMNV  // syntax & semantics of BPMN+V models

// Syntax of a specific model - arcs created by default from these
one sig P0 extends Start { }
one sig Pe extends End { }
one sig A, B, C extends Activity { }
fact {
  P0.next = A
  A.next = B
  B.next = C
  C.next = Pe
  no Pe.next
  no PllDiv + PllConv + XorDiv + XorConv + IorDiv + IorConv
}

pred test {
  BPMNV/states/first.markedNode.Token = P0 // and states/last.markedNode.Token = Pe
}
// need to count the no. or arcs, but any way to avoid large no. of states?
// run test for 9  but 2 State
run test for 5 Node, 4 Flow, 3 Activity, 0 IorDiv, 0 IorConv, 1 Token, 1 List, 1 Elem,
  0 PllDiv, 0 PllConv, 0 XorDiv, 0 XorConv, 9 State
