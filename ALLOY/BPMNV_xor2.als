/* Phil Weber 2017-04-25 Implement BPMN+V for a test XOR model.

Question: do we have to know the minimum #states for scope?

  o  Solved Issue (?): duplicate models where more states than needed: 
      arcs B to x'0 and x'0 onwards are reversed in names.  
      Assume similar for C to x'0 (in the alternative execution).
      Behaviour is the same.  Possibly implies some under-specification.
*/

open BPMNV  // Syntax of BPMN+V models

// Syntax of a specific model - arcs created by default from these
one sig P0 extends Start { }
one sig Pe extends End { }
one sig A, B, C extends Activity { }
one sig x0 extends XorDiv { }
one sig x'0 extends XorConv { }
fact model {
  P0.next = A
  A.next = x0
  x0.next = B + C
  (B + C).next = x'0
  x'0.next = Pe
  no Pe.next
  no PllDiv + PllConv + IorDiv + IorConv
}

pred test {
  BPMNV/states/first.markedNode.Token = P0 // and BPMNV/states/last.markedNode.Token = Pe
}
// need to count the no. or arcs, but any way to avoid large no. of states?
// run test for 9  but 11 State
run test for 7 Node, 7 Flow, 3 Activity, 0 IorDiv, 0 IorConv, 2 Token, 2 List, 1 Elem,
  0 PllDiv, 0 PllConv, 1 XorDiv, 1 XorConv, 11 State
